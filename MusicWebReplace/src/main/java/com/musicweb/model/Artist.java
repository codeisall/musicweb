package com.musicweb.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "artist")
public class Artist {
	private Integer id;
	private String name;
	private String img;
	private String description;
	private Integer liked;
	private List<User_Artist_Liked> user_Artist_Likeds;
	private List<Artist_Genre> artist_Genres;
	private List<Song_Artist> song_Artists;
	private List<Album> albums;

	public Artist(Integer id, String name, String img, String description) {
		super();
		this.id = id;
		this.name = name;
		this.img = img;
		this.description = description;
	}

	public Artist() {
		super();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "img")
	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	@Column(name = "description")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Column(name = "liked")
	public Integer getLiked() {
		return liked;
	}

	public void setLiked(Integer liked) {
		this.liked = liked;
	}
	
	@JsonIgnore
	@OneToMany(mappedBy = "id.artist", fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST, CascadeType.MERGE,
			CascadeType.DETACH, CascadeType.REFRESH })
	public List<User_Artist_Liked> getUser_Artist_Likeds() {
		return user_Artist_Likeds;
	}

	public void setUser_Artist_Likeds(List<User_Artist_Liked> user_Artist_Likeds) {
		this.user_Artist_Likeds = user_Artist_Likeds;
	}

	@JsonIgnore	
	@OneToMany(mappedBy = "id.artist", fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST, CascadeType.MERGE,
			CascadeType.DETACH, CascadeType.REFRESH})
	public List<Artist_Genre> getArtist_Genres() {
		return artist_Genres;
	}

	public void setArtist_Genres(List<Artist_Genre> artist_Genres) {
		this.artist_Genres = artist_Genres;
	}

	@JsonIgnore
	@OneToMany(mappedBy = "id.artist", fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST, CascadeType.MERGE,
			CascadeType.DETACH, CascadeType.REFRESH })
	public List<Song_Artist> getSong_Artists() {
		return song_Artists;
	}

	public void setSong_Artists(List<Song_Artist> song_Artists) {
		this.song_Artists = song_Artists;
	}

	@JsonIgnore
	@OneToMany(mappedBy = "artist", fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST, CascadeType.MERGE,
			CascadeType.DETACH, CascadeType.REFRESH })
	public List<Album> getAlbums() {
		return albums;
	}

	public void setAlbums(List<Album> albums) {
		this.albums = albums;
	}
}
