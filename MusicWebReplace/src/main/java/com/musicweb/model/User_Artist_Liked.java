package com.musicweb.model;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "user_artist_liked")
public class User_Artist_Liked {
	@EmbeddedId
	private User_Artist_LikedId id;

	public User_Artist_Liked(User_Artist_LikedId id) {
		super();
		this.id = id;
	}

	public User_Artist_Liked() {
		super();
	}

	public User_Artist_LikedId getId() {
		return id;
	}

	public void setId(User_Artist_LikedId id) {
		this.id = id;
	}
}
