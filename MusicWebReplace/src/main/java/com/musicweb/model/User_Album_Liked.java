package com.musicweb.model;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "user_album_liked")
@Setter
@Getter
public class User_Album_Liked {
	@EmbeddedId
	private User_Album_LikedId id;

	public User_Album_Liked(User_Album_LikedId id) {
		super();
		this.id = id;
	}

	public User_Album_Liked() {
		super();
	}
}
