package com.musicweb.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.ColumnDefault;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "genre")
public class Genre {
	private Integer id;
	private String name;
	private Integer listen;
	private List<Artist_Genre> artist_Genres;
	private List<Song_Genre> song_Genres;

	public Genre(Integer id, String name) {
		super();
		this.id = id;
		this.name = name;
	}

	public Genre() {
		super();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Column(name = "listen")
	public Integer getListen() {
		return listen;
	}

	public void setListen(Integer listen) {
		this.listen = listen;
	}

	@JsonIgnore
	@OneToMany(mappedBy = "id.genre", fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST, CascadeType.MERGE,
			CascadeType.DETACH, CascadeType.REFRESH })
	public List<Artist_Genre> getArtist_Genres() {
		return artist_Genres;
	}

	public void setArtist_Genres(List<Artist_Genre> artist_Genres) {
		this.artist_Genres = artist_Genres;
	}

	@JsonIgnore
	@OneToMany(mappedBy = "id.genre", fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST, CascadeType.MERGE,
			CascadeType.DETACH, CascadeType.REFRESH })
	public List<Song_Genre> getSong_Genres() {
		return song_Genres;
	}

	public void setSong_Genres(List<Song_Genre> song_Genres) {
		this.song_Genres = song_Genres;
	}
}
