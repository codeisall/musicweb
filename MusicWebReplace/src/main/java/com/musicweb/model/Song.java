package com.musicweb.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "song")
public class Song {
	private Integer id;
	private String name;
	private String lyric;
	private Integer liked;
	private Integer listen;
	private String resource;
	private List<Song_Genre> song_Genres;
	private List<Song_Artist> song_Artists;
	private List<Comment> comments;
	private List<Favorite> favorites;
	private List<Album_Song> album_Songs;

	public Song(Integer id, String name, String lyric, Integer liked, Integer listen) {
		super();
		this.id = id;
		this.name = name;
		this.lyric = lyric;
		this.liked = liked;
		this.listen = listen;
	}

	public Song() {
		super();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "lyric")
	public String getLyric() {
		return lyric;
	}

	public void setLyric(String lyric) {
		this.lyric = lyric;
	}

	@Column(name = "liked")
	public Integer getLiked() {
		return liked;
	}

	public void setLiked(Integer liked) {
		this.liked = liked;
	}

	@Column(name = "listen")
	public Integer getListen() {
		return listen;
	}

	public void setListen(Integer listen) {
		this.listen = listen;
	}

	@Column(name = "resource")
	public String getResource() {
		return resource;
	}

	public void setResource(String resource) {
		this.resource = resource;
	}

	@JsonIgnore
	@OneToMany(mappedBy = "id.song", fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST, CascadeType.MERGE,
			CascadeType.DETACH, CascadeType.REFRESH })
	public List<Song_Genre> getSong_Genres() {
		return song_Genres;
	}

	public void setSong_Genres(List<Song_Genre> song_Genres) {
		this.song_Genres = song_Genres;
	}

	@JsonIgnore
	@OneToMany(mappedBy = "id.song", fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST, CascadeType.MERGE,
			CascadeType.DETACH, CascadeType.REFRESH })
	public List<Song_Artist> getSong_Artists() {
		return song_Artists;
	}

	public void setSong_Artists(List<Song_Artist> song_Artists) {
		this.song_Artists = song_Artists;
	}

	@JsonIgnore
	@OneToMany(mappedBy = "song", fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST, CascadeType.MERGE,
			CascadeType.DETACH, CascadeType.REFRESH })
	public List<Comment> getComments() {
		return comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

	@JsonIgnore
	@OneToMany(mappedBy = "id.song", fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST, CascadeType.MERGE,
			CascadeType.DETACH, CascadeType.REFRESH })
	public List<Favorite> getFavorites() {
		return favorites;
	}

	public void setFavorites(List<Favorite> favorites) {
		this.favorites = favorites;
	}

	@JsonIgnore
	@OneToMany(mappedBy = "id.song", fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST, CascadeType.MERGE,
			CascadeType.DETACH, CascadeType.REFRESH })
	public List<Album_Song> getAlbum_Songs() {
		return album_Songs;
	}

	public void setAlbum_Songs(List<Album_Song> album_Songs) {
		this.album_Songs = album_Songs;
	}
}
