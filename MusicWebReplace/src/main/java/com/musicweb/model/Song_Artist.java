package com.musicweb.model;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "song_artist")
public class Song_Artist {
	@EmbeddedId
	private Song_ArtistId id;

	public Song_Artist(Song_ArtistId id) {
		super();
		this.id = id;
	}

	public Song_Artist() {
		super();
	}

	public Song_ArtistId getId() {
		return id;
	}

	public void setId(Song_ArtistId id) {
		this.id = id;
	}
}
