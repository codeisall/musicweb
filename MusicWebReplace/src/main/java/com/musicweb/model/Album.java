package com.musicweb.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "album")
public class Album {
	private Integer id;
	private String name;
	private Integer liked;
	private Artist artist;
	private List<Album_Song> album_Songs;
	private List<User_Album_Liked> user_Album_Likeds;

	public Album(Integer id, String name) {
		super();
		this.id = id;
		this.name = name;
	}

	public Album() {
		super();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "liked")
	public Integer getLiked() {
		return liked;
	}

	public void setLiked(Integer liked) {
		this.liked = liked;
	}

	@ManyToOne(fetch = FetchType.EAGER, cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH,
			CascadeType.REFRESH })
	@JoinColumn(name = "artist_id")
	public Artist getArtist() {
		return artist;
	}

	public void setArtist(Artist artist) {
		this.artist = artist;
	}

	@JsonIgnore	
	@OneToMany(mappedBy = "id.album", fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST, CascadeType.MERGE,
			CascadeType.DETACH, CascadeType.REFRESH })
	public List<Album_Song> getAlbum_Songs() {
		return album_Songs;
	}

	public void setAlbum_Songs(List<Album_Song> album_Songs) {
		this.album_Songs = album_Songs;
	}
	
	@JsonIgnore
	@OneToMany(mappedBy = "id.album", fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST, CascadeType.MERGE,
			CascadeType.DETACH, CascadeType.REFRESH })
	public List<User_Album_Liked> getUser_Album_Likeds() {
		return user_Album_Likeds;
	}

	public void setUser_Album_Likeds(List<User_Album_Liked> user_Album_Likeds) {
		this.user_Album_Likeds = user_Album_Likeds;
	}
}
