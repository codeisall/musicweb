package com.musicweb.model;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "album_song")
public class Album_Song {
	@EmbeddedId
	private Album_SongId id;

	public Album_Song(Album_SongId id) {
		super();
		this.id = id;
	}

	public Album_Song() {
		super();
	}

	public Album_SongId getId() {
		return id;
	}

	public void setId(Album_SongId id) {
		this.id = id;
	}
}
