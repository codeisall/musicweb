package com.musicweb.model;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "song_genre")
public class Song_Genre {
	@EmbeddedId
	private Song_GenreId id;

	public Song_Genre(Song_GenreId id) {
		super();
		this.id = id;
	}

	public Song_Genre() {
		super();
	}

	public Song_GenreId getId() {
		return id;
	}

	public void setId(Song_GenreId id) {
		this.id = id;
	}
}
