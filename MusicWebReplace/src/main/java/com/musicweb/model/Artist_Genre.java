package com.musicweb.model;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "artist_genre")
public class Artist_Genre {
	@EmbeddedId
	private Artist_GenreId id;

	public Artist_Genre(Artist_GenreId id) {
		super();
		this.id = id;
	}

	public Artist_Genre() {
		super();
	}

	public Artist_GenreId getId() {
		return id;
	}

	public void setId(Artist_GenreId id) {
		this.id = id;
	}
}
