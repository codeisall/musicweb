package com.musicweb.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.musicweb.model.Album;
import com.musicweb.model.Artist;
import com.musicweb.model.Genre;
import com.musicweb.model.Song;
import com.musicweb.model.User;
import com.musicweb.service.AlbumService;
import com.musicweb.service.ArtistService;
import com.musicweb.service.FavoriteService;
import com.musicweb.service.GenreService;
import com.musicweb.service.SongService;
import com.musicweb.service.User_Album_LikedService;
import com.musicweb.service.User_Artist_LikedService;

@Controller
@RequestMapping(value = "homeSubController")
public class HomeSubController {
	@Autowired
	private GenreService genreService;
	@Autowired
	private ArtistService artistService;
	@Autowired
	private AlbumService albumService;
	@Autowired
	private SongService songService;
	@Autowired
	private FavoriteService favoriteService;
	@Autowired
	private User_Artist_LikedService user_Artist_LikedService;
	@Autowired
	private User_Album_LikedService user_Album_LikedService;

	private User user = null;

	private int limit = 10;

	@GetMapping(value = "/")
	@ResponseBody
	public ModelAndView display(HttpSession session) {
		ModelAndView view = new ModelAndView();
		List<Song> topSongsLike = songService.getTopSongLike(limit, 0);
		List<Song> topSongsListen = songService.getTopSongListen(limit, 0);
		List<Artist> topArtists = artistService.getTopArtist(limit);
		List<Album> topAlbums = albumService.getTopAlbum(limit);
		List<Integer> songIdLiked = new ArrayList<Integer>();
		List<Integer> songIdLiked2 = new ArrayList<Integer>();
		List<Integer> artistIdLiked = new ArrayList<Integer>();
		List<Integer> albumIdLiked = new ArrayList<Integer>();
		view.setViewName("homesub");

		view.addObject("topGenres", genreService.getTopGenre(limit));
		if (session.getAttribute("user") != null) {
			user = (User) session.getAttribute("user");
			for (Song s : topSongsLike) {
				if (favoriteService.isLiked(s.getId(), user.getId()))
					songIdLiked.add(s.getId());
				else
					songIdLiked.add(-1);
			}

			for (Song s : topSongsListen) {
				if (favoriteService.isLiked(s.getId(), user.getId()))
					songIdLiked2.add(s.getId());
				else
					songIdLiked2.add(-1);
			}

			for (Artist s : topArtists) {
				if (user_Artist_LikedService.isLiked(s.getId(), user.getId()))
					artistIdLiked.add(s.getId());
				else
					artistIdLiked.add(-1);
			}

			for (Album s : topAlbums) {
				if (user_Album_LikedService.isLiked(s.getId(), user.getId()))
					albumIdLiked.add(s.getId());
				else
					albumIdLiked.add(-1);
			}
		}

		view.addObject("songidsliked2", songIdLiked2);
		view.addObject("topSongsListen", topSongsListen);
		view.addObject("songidsliked", songIdLiked);
		view.addObject("topSongs", topSongsLike);
		view.addObject("artistdiliked", artistIdLiked);
		view.addObject("topArtists", topArtists);
		view.addObject("albumidliked", albumIdLiked);
		view.addObject("topAlbums", topAlbums);
		return view;
	}

	@GetMapping(value = "/loadmore")
	@ResponseBody
	public List<Object> loadMore(@RequestParam("type") Integer type, @RequestParam("start") String start) {
		List<Object> objs = new ArrayList<Object>();
		List<Song> list = (type == 1) ? songService.getTopSongLike(limit, Integer.parseInt(start))
				: songService.getTopSongListen(limit, Integer.parseInt(start));
		List<Integer> songIdLiked = new ArrayList<Integer>();
		for (Song s : list) {
			s.setName(s.getName() + "~" + s.getSong_Artists().get(0).getId().getArtist().getName() + "~"
					+ s.getSong_Artists().get(0).getId().getArtist().getImg());
			if (user != null)
				if (favoriteService.isLiked(s.getId(), user.getId()))
					songIdLiked.add(s.getId());
				else
					songIdLiked.add(-1);
			else
				songIdLiked.add(-1);
		}
		objs.add(list);
		objs.add(songIdLiked);
		return objs;
	}
	
	@GetMapping("/search")
	@ResponseBody
	public List<Object> search(@RequestParam("content") String content) {
		List<Object> objs = new ArrayList<Object>();
		List<Song> songs = songService.search(content);
		List<Artist> artists = artistService.search(content);
		List<Album> albums = albumService.search(content);
		List<Genre> genres = genreService.search(content);
		
		
		objs.add(songs);
		objs.add(artists);
		objs.add(albums);
		objs.add(genres);
		return objs;
	}
}
