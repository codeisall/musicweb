package com.musicweb.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.musicweb.model.Album_Song;
import com.musicweb.model.Artist;
import com.musicweb.model.Song_Artist;
import com.musicweb.model.Song_Genre;
import com.musicweb.model.User;
import com.musicweb.service.Album_SongService;
import com.musicweb.service.FavoriteService;

@Controller
@RequestMapping(value = "albumSongController")
public class AlbumSongController {
	@Autowired
	private Album_SongService album_SongService;
	@Autowired
	private FavoriteService favoriteService;

	@PostMapping("/add")
	@ResponseBody
	public void add(@RequestBody List<Album_Song> album_songs, @RequestParam("id") String id) {
		for (Album_Song a : album_songs) {
			album_SongService.insert(a);
		}
	}

	@PostMapping("/update")
	@ResponseBody
	public void update(@RequestBody List<Album_Song> album_Songs, @RequestParam("id") String id) {
		album_SongService.deleteByAlbumId(Integer.parseInt(id));
		for (Album_Song a : album_Songs) {
//			System.out.println(a.getId().getGenre().getId());
			album_SongService.insert(a);
		}
	}

	@GetMapping(value = "/songs")
	@ResponseBody
	public List<Object> getSongsOfAlbum(@RequestParam("id") String id, HttpSession session) {
		List<Object> objs = new ArrayList<Object>();
		List<Album_Song> list = album_SongService.getSongsOfAlbum(Integer.parseInt(id));
		List<Integer> songIdLiked = new ArrayList<Integer>();
		List<Artist> artists = new ArrayList<Artist>();
		List<Object> objs_sub = new ArrayList<Object>();
		User user = null;
		if (session.getAttribute("user") != null)
			user = (User) session.getAttribute("user");
		for (Album_Song s : list) {
			if(user != null)
				if(favoriteService.isLiked(s.getId().getSong().getId(), user.getId()))
					songIdLiked.add(s.getId().getSong().getId());
				else
					songIdLiked.add(-1);
			else
				songIdLiked.add(-1);
			
			artists = new ArrayList<Artist>();
			for(Song_Artist ss : s.getId().getSong().getSong_Artists())
				artists.add(ss.getId().getArtist());
			objs_sub.add(artists);
		}
		objs.add(list);
		objs.add(songIdLiked);
		objs.add(objs_sub);
		return objs;
	}
}
