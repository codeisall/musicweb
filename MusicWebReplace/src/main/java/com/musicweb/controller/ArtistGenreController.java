package com.musicweb.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.musicweb.model.Artist_Genre;
import com.musicweb.service.Artist_GenreService;

@Controller
@RequestMapping(value = "artistGenreController")
public class ArtistGenreController {
	@Autowired
	private Artist_GenreService artist_GenreService;
	
	@PostMapping("/add")
	@ResponseBody
	public void add(@RequestBody List<Artist_Genre> artist_Genres) {
		for(Artist_Genre a : artist_Genres) {
			artist_GenreService.insert(a);
		}
	}
	
	@PostMapping("/update")
	@ResponseBody
	public void update(@RequestBody List<Artist_Genre> artist_Genres, @RequestParam("id") String id) {
		artist_GenreService.deleteByArtistId(Integer.parseInt(id));
		for(Artist_Genre a : artist_Genres) {
			artist_GenreService.insert(a);
		}
	}
}
