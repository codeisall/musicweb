package com.musicweb.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.musicweb.model.Artist;
import com.musicweb.model.Song;
import com.musicweb.model.Song_Artist;
import com.musicweb.model.Song_Genre;
import com.musicweb.model.User;
import com.musicweb.service.FavoriteService;
import com.musicweb.service.Song_ArtistService;

@Controller
@RequestMapping(value = "songArtistController")
public class SongArtistController {
	@Autowired
	private Song_ArtistService song_ArtistService;
	@Autowired
	private FavoriteService favoriteService;

	@PostMapping("/add")
	@ResponseBody
	public void add(@RequestBody List<Song_Artist> song_Artits, @RequestParam("id") String id) {
		for (Song_Artist a : song_Artits) {
			System.out.println(a.getId().getArtist().getId());
			song_ArtistService.insert(a);
		}
	}
	
	@PostMapping("/update")
	@ResponseBody
	public void update(@RequestBody List<Song_Artist> song_Artits, @RequestParam("id") String id) {
		song_ArtistService.deleteBySongId(Integer.parseInt(id));
		for (Song_Artist a : song_Artits) {
			System.out.println(a.getId().getArtist().getId());
			song_ArtistService.insert(a);
		}
	}
	
	@GetMapping(value = "/songs")
	@ResponseBody
	public List<Object> getSongsOfArtist(@RequestParam("id") String id, HttpSession session) {
		String[] ids_string = id.split(",");
		List<Integer> ids_integer = new ArrayList<Integer>();
		for(String s : ids_string)
			ids_integer.add(Integer.parseInt(s));
		List<Object> objs = new ArrayList<Object>();
		List<Song> songs = song_ArtistService.getSongsOfArtist(ids_integer);
		List<Integer> songIdLiked = new ArrayList<Integer>();
		List<Artist> artists = new ArrayList<Artist>();
		List<Object> objs_sub = new ArrayList<Object>();
		User user = null;
		if (session.getAttribute("user") != null)
			user = (User) session.getAttribute("user");
		
		for (Song s : songs) {
			if(user != null)
				if(favoriteService.isLiked(s.getId(), user.getId()))
					songIdLiked.add(s.getId());
				else
					songIdLiked.add(-1);
			else
				songIdLiked.add(-1);
			
			artists = new ArrayList<Artist>();
			for(Song_Artist ss : s.getSong_Artists())
				artists.add(ss.getId().getArtist());
			objs_sub.add(artists);
		}
		
		objs.add(songs);
		objs.add(songIdLiked);
//		if(songs.size() > 0)
//			objs.add(songs.get(0).getSong_Artists().get(0).getId().getArtist());
//		else
//			objs.add(new Artist());
		objs.add(objs_sub);
		return objs;
	}
}
