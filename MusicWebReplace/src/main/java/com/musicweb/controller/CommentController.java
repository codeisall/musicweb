package com.musicweb.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.musicweb.model.Comment;
import com.musicweb.model.User;
import com.musicweb.service.CommentService;

@Controller
@RequestMapping(value = "commentController")
@SessionAttributes({"user"})
public class CommentController {
	@Autowired
	private CommentService commentService;
	
	@PostMapping(value = "/comment")
	@ResponseBody
	public void comment(@RequestBody Comment comment, @ModelAttribute User user) {
		comment.setUser(user);
		comment.setLiked(0);
		commentService.insert(comment);
	}
	
	@GetMapping(value = "/comments")
	@ResponseBody
	public List<Comment> getCommentsOfASong(@RequestParam("id") String id) {
		return commentService.getCommentsOfASong(Integer.parseInt(id));
	}
	
	@GetMapping(value = "/last")
	@ResponseBody
	public Comment getLastComment() {
		return commentService.getLastComment();
	}
}
