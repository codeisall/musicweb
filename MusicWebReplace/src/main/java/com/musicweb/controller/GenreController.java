package com.musicweb.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.musicweb.model.Genre;
import com.musicweb.service.GenreService;

@Controller
@RequestMapping(value = "genreController")
public class GenreController {
	@Autowired
	private GenreService genreService;

	@GetMapping(value = "/")
	@ResponseBody
	public ModelAndView display(@RequestParam(value = "attribute", required = false) String attribute,
			@RequestParam(value = "type", required = false) String type) {
		ModelAndView view = new ModelAndView();
		view.setViewName("genre");
		
		String orderBy = "";
		if(attribute == null || type == null) {
			attribute = "id";
			type = "1";
			orderBy = "asc";
		} else {
			if (Integer.parseInt(type) == 1) {
				orderBy = "asc";
			} else {
				orderBy = "desc";
			}
		}
		view.addObject("attribute", attribute);// set attribute after sort
		view.addObject("type", type);// set type sort after sort
		view.addObject("genres", genreService.sort(attribute, orderBy));
		return view;
	}

	@PostMapping(value = "/add")
	@ResponseBody
	public void add(@RequestBody Genre genre) {
		genre.setListen(0);
		genreService.insert(genre);
	}

	@PostMapping(value = "/update")
	@ResponseBody
	public void update(@RequestBody Genre genre) {
		genreService.update(genre);
	}

	@DeleteMapping(value = "/delete")
	@ResponseBody
	public void delete(@RequestBody Genre genre) {
//		System.out.println(genre.getName());
		genreService.delete(genre);
	}

	@GetMapping("/genre")
	@ResponseBody
	public Genre getGenre(@RequestParam("id") String id) {
		return genreService.getById(Integer.parseInt(id));
	}
}
