package com.musicweb.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.musicweb.model.Album;
import com.musicweb.model.Artist;
import com.musicweb.model.Genre;
import com.musicweb.model.Song;
import com.musicweb.model.User;
import com.musicweb.service.AlbumService;
import com.musicweb.service.Album_SongService;
import com.musicweb.service.ArtistService;
import com.musicweb.service.Artist_GenreService;
import com.musicweb.service.GenreService;
import com.musicweb.service.User_Album_LikedService;

@Controller
@RequestMapping(value = "albumController")
public class AlbumController {
	@Autowired
	private ArtistService artistService;
	@Autowired
	private GenreService genreService;
	@Autowired
	private Artist_GenreService artist_GenreService;
	@Autowired
	private AlbumService albumService;
	@Autowired
	private Album_SongService album_SongService;
	@Autowired
	private User_Album_LikedService user_Album_LikedService;

	@GetMapping(value = "/")
	@ResponseBody
	public ModelAndView display(@RequestParam(value = "attribute", required = false) String attribute,
			@RequestParam(value = "type", required = false) String type) {
		ModelAndView view = new ModelAndView();
		view.setViewName("album");

		String orderBy = "";
		if (attribute == null || type == null) {
			attribute = "id";
			type = "1";
			orderBy = "asc";
		} else {
			if (Integer.parseInt(type) == 1) {
				orderBy = "asc";
			} else {
				orderBy = "desc";
			}
		}
		view.addObject("attribute", attribute);// set attribute after sort
		view.addObject("type", type);// set type sort after sort
		view.addObject("albums", albumService.sort(attribute, orderBy));
		view.addObject("artists", artistService.sort("id", "asc"));
		return view;
	}

	@GetMapping(value = "/albumsub")
	@ResponseBody
	public ModelAndView displayHome(@RequestParam(name = "id", required = false) String id,
			@RequestParam(name = "artist", required = false) String artist, HttpSession session) {
		ModelAndView view = new ModelAndView();
		view.setViewName("albumsub");
		if (id == null)
			id = "0";
		if (artist == null)
			artist = "0";

		User user = null;
		List<Integer> albumIdLiked = new ArrayList<Integer>();
		List<Album> albums = albumService.getAlbumsOfArtist(Integer.parseInt(id));

		if (session.getAttribute("user") != null) {
			user = (User) session.getAttribute("user");
			for (Album s : albums) {
				if (user_Album_LikedService.isLiked(s.getId(), user.getId()))
					albumIdLiked.add(s.getId());
				else
					albumIdLiked.add(-1);
			}
		}

		view.addObject("albumidliked", albumIdLiked);
		view.addObject("albums", albums);
//		view.addObject("genres", genreService.getAll());
		view.addObject("artist", artist);
		return view;
	}

	@PostMapping(value = "/add")
	@ResponseBody
	public int add(@RequestBody Album album) {
		Artist artist = artistService.getById(album.getArtist().getId());
		album.setArtist(artist);
		albumService.insert(album);
		return albumService.getLastedId();
	}

	@PostMapping(value = "/update")
	@ResponseBody
	public void update(@RequestBody Album album) {
		album.setLiked(albumService.getById(album.getId()).getLiked());
		albumService.update(album);
	}

	@DeleteMapping(value = "/delete")
	@ResponseBody
	public void delete(@RequestBody Album album) {
//		System.out.println(genre.getName());
		album_SongService.deleteByAlbumId(album.getId());
		albumService.delete(album);
	}

	@GetMapping(value = "/album", produces = "application/json;charset=utf-8")
	@ResponseBody
	public Album getArtist(@RequestParam("id") String id) {
//		ObjectMapper mapper = new ObjectMapper();
//		Album album = albumService.getById(Integer.parseInt(id));
//		album.setAlbum_Songs(albumService.getById(Integer.parseInt(id)).getAlbum_Songs());
//		try {
//			System.out.println(mapper.writeValueAsString(album));
//			System.out.println(mapper.writeValueAsString(album.getAlbum_Songs()));
//		} catch (NumberFormatException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (JsonProcessingException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		return albumService.getById(Integer.parseInt(id));
	}

	@GetMapping("/albums")
	@ResponseBody
	public List<Object> getAlbumsOfArtist(@RequestParam("id") String id, HttpSession session) {
		List<Object> objs = new ArrayList<Object>();
		List<Album> albums = albumService.getAlbumsOfArtist(Integer.parseInt(id));
		List<Integer> albumIdLiked = new ArrayList<Integer>();
		User user = null;
		if (session.getAttribute("user") != null)
			user = (User) session.getAttribute("user");

		for (Album s : albums) {
			if (user != null)
				if (user_Album_LikedService.isLiked(s.getId(), user.getId()))
					albumIdLiked.add(s.getId());
				else
					albumIdLiked.add(-1);
			else
				albumIdLiked.add(-1);
		}
		objs.add(albums);
		objs.add(albumIdLiked);
		return objs;
	}

	@GetMapping("/songs")
	@ResponseBody
	public List<Song> getSongsOfAlbum(@RequestParam("album_id") String id) {
		return albumService.getSongsOfAlbum(Integer.parseInt(id));
	}

	@GetMapping("/genres")
	@ResponseBody
	public List<Genre> getGenresOfArtist(@RequestParam("id") String id) {
		return artistService.getGenresByArtistId(Integer.parseInt(id));
	}

	@PostMapping("/genres")
	@ResponseBody
	public List<Genre> getGenresOfArtist(@RequestBody List<Integer> artistIds) {
		return artistService.getGenresByArtistIds(artistIds);
	}
}
