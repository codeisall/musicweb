package com.musicweb.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.musicweb.model.Artist_Genre;
import com.musicweb.service.Artist_GenreService;
import com.musicweb.service.GenreService;

@Controller
@RequestMapping(value = "testController")
public class Test {
	@Autowired
	GenreService genreService;
	@Autowired
	private Artist_GenreService artist_Genre;
	@GetMapping(value = "/")
	public String test() {
		List<Artist_Genre> list = artist_Genre.getAll();
		for(Artist_Genre s : list)
			System.out.println(s.getId().getArtist().getId());
		return "index";
	}
}
