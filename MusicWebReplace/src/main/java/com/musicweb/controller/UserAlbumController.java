package com.musicweb.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.musicweb.model.Album;
import com.musicweb.model.Artist;
import com.musicweb.model.User;
import com.musicweb.model.User_Album_Liked;
import com.musicweb.model.User_Artist_Liked;
import com.musicweb.service.AlbumService;
import com.musicweb.service.User_Album_LikedService;

@Controller
@RequestMapping(value = "/userAlbumController")
@SessionAttributes({ "user" })
public class UserAlbumController {
	@Autowired
	private User_Album_LikedService user_Album_LikedService;
	@Autowired
	private AlbumService albumService;

	@PostMapping("/add")
	@ResponseBody
	public void add(@RequestBody User_Album_Liked user_Album_Liked, @ModelAttribute User user) {
		// increase liked time
		Album album = albumService.getById(user_Album_Liked.getId().getAlbum().getId());
		album.setLiked(album.getLiked() + 1);
		albumService.update(album);
		// end increase liked time
		user_Album_Liked.getId().setUser(user);
		user_Album_LikedService.insert(user_Album_Liked);
	}
	
	@DeleteMapping("/delete")
	@ResponseBody
	public void delete(@RequestBody User_Album_Liked user_Album_Liked, @ModelAttribute User user) {
		// decrease liked time
		Album album = albumService.getById(user_Album_Liked.getId().getAlbum().getId());
		album.setLiked(album.getLiked() - 1);
		albumService.update(album);
		// end decrease liked time
		user_Album_Liked.getId().setUser(user);
		user_Album_LikedService.delete(user_Album_Liked);
	}
}
