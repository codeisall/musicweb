package com.musicweb.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.musicweb.model.AES;
import com.musicweb.model.User;
import com.musicweb.service.UserService;

@Controller
@RequestMapping(value = "loginController")
@SessionAttributes("user")
public class LoginController {
	@Autowired
	private UserService userService;
	private String secretKey = "musicweb";

	@GetMapping(value = "/")
	public String display() {
		return "login";
	}

	@GetMapping(value = "/isexist")
	@ResponseBody
	public boolean isExist(@RequestParam("email") String email) {
		return userService.getByEmail(email) == null ? false : true;
	}

	@GetMapping(value = "/signout")
	public String singout(SessionStatus status) {
		status.setComplete();
		return "redirect:../homeController/";
	}

	@PostMapping(value = "/signup")
	@ResponseBody
	public void signup(@RequestBody() User user) {
		user.setRole("user");
		String encoded = AES.encrypt(user.getPassword(), secretKey);
		user.setPassword(encoded);
		System.out.println(userService.insert(user));
		System.out.println(user);
	}

	@PostMapping(value = "/signin")
	@ResponseBody
	public List<Object> login(@RequestBody() User user, ModelMap map) {
		List<Object> list = new ArrayList<>();
		User u = userService.getByEmail(user.getEmail());
		if (u.getPassword().equals(AES.encrypt(user.getPassword(), secretKey))) {
			map.addAttribute("user", u);
			list.add(true);
			if (u.getRole().equals("user")) {
				list.add(1);
			} else {
				list.add(2);
			}
			return list;
		}
		list.add(false);
		return list;
	}
	
	@GetMapping(value = "/islogin")
	@ResponseBody
	public boolean isLoggedIn(HttpSession httpSession) {
		User user = null;
		user = (User) httpSession.getAttribute("user");
		if(user != null)
			return true;
		return false;
	}

	public static void main(String[] args) {
		String str = "123";
		String encoded = AES.encrypt(str, "musicweb");
		System.out.println("Encoded : " + encoded);
		String decoded = AES.decrypt(encoded, "musicweb");
		System.out.println("Decoded : " + decoded);
	}
}
