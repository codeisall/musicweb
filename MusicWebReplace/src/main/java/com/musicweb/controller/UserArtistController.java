package com.musicweb.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.musicweb.model.Artist;
import com.musicweb.model.User;
import com.musicweb.model.User_Artist_Liked;
import com.musicweb.service.ArtistService;
import com.musicweb.service.User_Artist_LikedService;

@Controller
@RequestMapping(value = "userArtistController")
@SessionAttributes({ "user" })
public class UserArtistController {
	@Autowired
	private User_Artist_LikedService user_Artist_LikedService;
	@Autowired
	private ArtistService artistService;
	
	@PostMapping("/add")
	@ResponseBody
	public void add(@RequestBody User_Artist_Liked user_Artist_Liked, @ModelAttribute User user) {
		// increase liked time
		Artist artist = artistService.getById(user_Artist_Liked.getId().getArtist().getId());
		artist.setLiked(artist.getLiked() + 1);
		artistService.update(artist);
		// end increase liked time
		user_Artist_Liked.getId().setUser(user);
		user_Artist_LikedService.insert(user_Artist_Liked);
	}
	
	@DeleteMapping("/delete")
	@ResponseBody
	public void delete(@RequestBody User_Artist_Liked user_Artist_Liked, @ModelAttribute User user) {
		// decrease liked time
		Artist artist = artistService.getById(user_Artist_Liked.getId().getArtist().getId());
		artist.setLiked(artist.getLiked() - 1);
		artistService.update(artist);
		// end decrease liked time
		user_Artist_Liked.getId().setUser(user);
		user_Artist_LikedService.delete(user_Artist_Liked);
	}
}
