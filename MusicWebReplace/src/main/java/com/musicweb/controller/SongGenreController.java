package com.musicweb.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.musicweb.model.Artist;
import com.musicweb.model.Genre;
import com.musicweb.model.Song_Artist;
import com.musicweb.model.Song_Genre;
import com.musicweb.model.User;
import com.musicweb.service.FavoriteService;
import com.musicweb.service.GenreService;
import com.musicweb.service.Song_GenreService;

@Controller
@RequestMapping(value = "songGenreController")
public class SongGenreController {
	@Autowired
	private Song_GenreService song_GenreService;
	@Autowired
	private FavoriteService favoriteService;
	@Autowired
	private GenreService genreService;

	@PostMapping("/add")
	@ResponseBody
	public void add(@RequestBody List<Song_Genre> song_Genres, @RequestParam("id") String id) {
		for (Song_Genre a : song_Genres) {
			System.out.println(a.getId().getGenre().getId());
			song_GenreService.insert(a);
		}
	}

	@PostMapping("/update")
	@ResponseBody
	public void update(@RequestBody List<Song_Genre> song_Genres, @RequestParam("id") String id) {
		song_GenreService.deleteBySongId(Integer.parseInt(id));
		for (Song_Genre a : song_Genres) {
			System.out.println(a.getId().getGenre().getId());
			song_GenreService.insert(a);
		}
	}

	@GetMapping(value = "/songs")
	@ResponseBody
	public List<Object> getSongsOfGenre(@RequestParam("id") String id, @RequestParam("play") boolean play,
			HttpSession session) {
		// Increase listen time
		if (play) {
			Genre g = genreService.getById(Integer.parseInt(id));
			g.setListen(g.getListen() + 1);
			genreService.update(g);
		}
		// end Increase listen time

		List<Object> objs = new ArrayList<Object>();
		List<Song_Genre> list = song_GenreService.getSongsOfGenre(Integer.parseInt(id));
		List<Integer> songIdLiked = new ArrayList<Integer>();
		List<Artist> artists = new ArrayList<Artist>();
		List<Object> objs_sub = new ArrayList<Object>();
		User user = null;
		if (session.getAttribute("user") != null)
			user = (User) session.getAttribute("user");
		for (Song_Genre s : list) {
			if (user != null) {
				if (favoriteService.isLiked(s.getId().getSong().getId(), user.getId()))
					songIdLiked.add(s.getId().getSong().getId());
				else
					songIdLiked.add(-1);
			} else
				songIdLiked.add(-1);

			artists = new ArrayList<Artist>();
			for (Song_Artist ss : s.getId().getSong().getSong_Artists())
				artists.add(ss.getId().getArtist());
			objs_sub.add(artists);
		}
		objs.add(list);
		objs.add(songIdLiked);
		objs.add(objs_sub);
		return objs;
	}
}
