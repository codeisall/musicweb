package com.musicweb.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.musicweb.model.User;
import com.musicweb.service.AlbumService;
import com.musicweb.service.ArtistService;
import com.musicweb.service.GenreService;
import com.musicweb.service.SongService;
import com.musicweb.service.UserService;

@Controller
@RequestMapping(value = "adminController")
public class AdminController {
	@Autowired
	private GenreService genreService;
	@Autowired
	private ArtistService artistService;
	@Autowired
	private AlbumService albumService;
	@Autowired
	private SongService songService;
	@Autowired
	private UserService userService;

	@GetMapping(value = "/")
	public String display(HttpSession httpSession) {
		String view = "";
		User user = null;
		user = (User) httpSession.getAttribute("user");
		if(user != null) {
			if(user.getRole().equals("admin")) 
				view = "admin";
			else
				view = "403";
		} else {
			view = "403";
		}
		return view;
	}

	@GetMapping("/search")
	@ResponseBody
	public List<Object> search(@RequestParam("content") String content, @RequestParam("table") String table) {
		List<Object> objs = new ArrayList<Object>();
		if (table.equalsIgnoreCase("genre")) {
			objs.add(genreService.search_admin(content));
		} else if (table.equalsIgnoreCase("artist")) {
			objs.add(artistService.search_admin(content));
		} else if (table.equalsIgnoreCase("album")) {
			objs.add(albumService.search_admin(content));
		} else if (table.equalsIgnoreCase("song")) {
			objs.add(songService.search_admin(content));
		} else if (table.equalsIgnoreCase("user")) {
			objs.add(userService.search_admin(content));
		}
		
		return objs;
	}
}
