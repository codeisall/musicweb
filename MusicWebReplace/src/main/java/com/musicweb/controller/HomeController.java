package com.musicweb.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.musicweb.service.GenreService;

@Controller
@RequestMapping(value = "homeController")
public class HomeController {
	@Autowired
	private GenreService genreService;
	
	@GetMapping(value = "/")
	public String display(ModelMap map) {
		map.addAttribute("genres", genreService.getAll());
		return "home";
	}
}
