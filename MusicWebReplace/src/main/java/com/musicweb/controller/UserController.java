package com.musicweb.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.musicweb.model.Artist;
import com.musicweb.model.User;
import com.musicweb.service.UserService;

@Controller
@RequestMapping(value = "userController")
public class UserController {
	@Autowired
	private UserService userService;
	
	
	@GetMapping(value = "/")
	@ResponseBody
	public ModelAndView display(@RequestParam(value = "attribute", required = false) String attribute,
			@RequestParam(value = "type", required = false) String type) {
		ModelAndView view = new ModelAndView();
		view.setViewName("user");

		String orderBy = "";
		if (attribute == null || type == null) {
			attribute = "id";
			type = "1";
			orderBy = "asc";
		} else {
			if (Integer.parseInt(type) == 1) {
				orderBy = "asc";
			} else {
				orderBy = "desc";
			}
		}
		view.addObject("attribute", attribute);// set attribute after sort
		view.addObject("type", type);// set type sort after sort
		view.addObject("users", userService.sort(attribute, orderBy));
		return view;
	}
	
	@PostMapping(value = "/add")
	@ResponseBody
	public int add(@RequestBody User user) {
		userService.insert(user);
		return userService.getLastedId();
	}
	
	@GetMapping("/user")
	@ResponseBody
	public User getUser(@RequestParam("id") String id) {
		return userService.getById(Integer.parseInt(id));
	}
	
	@PostMapping(value = "/update")
	@ResponseBody
	public void update(@RequestBody User user) {
		userService.update(user);
	}
	
	@DeleteMapping(value = "/delete")
	@ResponseBody
	public void delete(@RequestBody User user) {
		userService.delete(user);
	}
}
