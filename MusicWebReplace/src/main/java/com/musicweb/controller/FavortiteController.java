package com.musicweb.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.musicweb.model.Favorite;
import com.musicweb.model.Song;
import com.musicweb.model.User;
import com.musicweb.service.FavoriteService;
import com.musicweb.service.SongService;

@Controller
@RequestMapping(value = "favoriteController")
@SessionAttributes({ "user" })
public class FavortiteController {
	@Autowired
	private FavoriteService favoriteService;
	@Autowired
	private SongService songService;

	@PostMapping("/add")
	@ResponseBody
	public void add(@RequestBody Favorite favorite, @ModelAttribute User user) {
		// increase liked time
		Song song = songService.getById(favorite.getId().getSong().getId());
		song.setLiked(song.getLiked() + 1);
		songService.update(song);
		// end increase liked time
		favorite.getId().setUser(user);
		favoriteService.insert(favorite);
	}

	@DeleteMapping("/delete")
	@ResponseBody
	public void delete(@RequestBody Favorite favorite, @ModelAttribute User user) {
		// decrease liked time
		Song song = songService.getById(favorite.getId().getSong().getId());
		song.setLiked(song.getLiked() - 1);
		songService.update(song);
		// end decrease liked time
		favorite.getId().setUser(user);
		favoriteService.delete(favorite);
	}
}
