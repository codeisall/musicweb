package com.musicweb.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.musicweb.model.Artist;
import com.musicweb.model.Genre;
import com.musicweb.model.Song;
import com.musicweb.model.User;
import com.musicweb.service.ArtistService;
import com.musicweb.service.Artist_GenreService;
import com.musicweb.service.GenreService;
import com.musicweb.service.User_Artist_LikedService;

@Controller
@RequestMapping(value = "artistController")
public class ArtistController {
	@Autowired
	private ArtistService artistService;
	@Autowired
	private GenreService genreService;
	@Autowired
	private Artist_GenreService artist_GenreService;
	@Autowired
	private User_Artist_LikedService user_Artist_LikedService;

	@GetMapping(value = "/")
	@ResponseBody
	public ModelAndView display(@RequestParam(value = "attribute", required = false) String attribute,
			@RequestParam(value = "type", required = false) String type) {
		ModelAndView view = new ModelAndView();
		view.setViewName("artist");

		String orderBy = "";
		if (attribute == null || type == null) {
			attribute = "id";
			type = "1";
			orderBy = "asc";
		} else {
			if (Integer.parseInt(type) == 1) {
				orderBy = "asc";
			} else {
				orderBy = "desc";
			}
		}

		view.addObject("attribute", attribute);// set attribute after sort
		view.addObject("type", type);// set type sort after sort
		view.addObject("artists", artistService.sort(attribute, orderBy));
		view.addObject("genres", genreService.sort("id", "asc"));
		return view;
	}

	@GetMapping(value = "/artistsub")
	@ResponseBody
	public ModelAndView displayHome(@RequestParam(name = "id", required = false) String id,
			@RequestParam(name = "genreid", required = false) String genreId, HttpSession session) {
		ModelAndView view = new ModelAndView();
		view.setViewName("artistsub");
		if (id == null)
			id = "0";
		if (genreId == null)
			genreId = "0";
		
		User user = null;
		List<Integer> artistIdLiked = new ArrayList<Integer>();
		List<Artist> artists = artistService.getArtitsOfGenre(Integer.parseInt(id));

		if (session.getAttribute("user") != null) {
			user = (User) session.getAttribute("user");
			for (Artist s : artists) {
				if (user_Artist_LikedService.isLiked(s.getId(), user.getId()))
					artistIdLiked.add(s.getId());
				else
					artistIdLiked.add(-1);
			}
		}
		
		view.addObject("artistidliked", artistIdLiked);
		view.addObject("artists", artists);
		view.addObject("genres", genreService.getAll());
		view.addObject("genreid", genreId);
		return view;
	}

	@PostMapping(value = "/add")
	@ResponseBody
	public int add(@RequestBody Artist artist) {
		artistService.insert(artist);
		return artistService.getLastedId();
	}

	@PostMapping(value = "/update")
	@ResponseBody
	public void update(@RequestBody Artist artist) {
		artist.setLiked(artistService.getById(artist.getId()).getLiked());
		artistService.update(artist);
	}

	@DeleteMapping(value = "/delete")
	@ResponseBody
	public void delete(@RequestBody Artist artist) {
//		System.out.println(genre.getName());
		artist_GenreService.deleteByArtistId(artist.getId());
		artistService.delete(artist);
	}

	@GetMapping("/artist")
	@ResponseBody
	public Artist getArtist(@RequestParam("id") String id) {
		return artistService.getById(Integer.parseInt(id));
	}

	@GetMapping("/genres")
	@ResponseBody
	public List<Genre> getGenresOfArtist(@RequestParam("id") String id) {
		return artistService.getGenresByArtistId(Integer.parseInt(id));
	}

	@PostMapping("/genres")
	@ResponseBody
	public List<Genre> getGenresOfArtist(@RequestBody List<Integer> artistIds) {
		return artistService.getGenresByArtistIds(artistIds);
	}
}
