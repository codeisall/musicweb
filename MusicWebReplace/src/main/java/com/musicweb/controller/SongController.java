package com.musicweb.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.musicweb.model.Artist;
import com.musicweb.model.Favorite;
import com.musicweb.model.Genre;
import com.musicweb.model.Song;
import com.musicweb.model.Song_Artist;
import com.musicweb.model.User;
import com.musicweb.service.ArtistService;
import com.musicweb.service.Artist_GenreService;
import com.musicweb.service.FavoriteService;
import com.musicweb.service.SongService;
import com.musicweb.service.Song_ArtistService;
import com.musicweb.service.Song_GenreService;

@Controller
@RequestMapping("songController")
public class SongController {
	@Autowired
	private ArtistService artistService;
	@Autowired
	private Artist_GenreService artist_GenreService;
	@Autowired
	private SongService songService;
	@Autowired
	private Song_ArtistService song_ArtistService;
	@Autowired
	private Song_GenreService song_GenreService;
	@Autowired
	private FavoriteService favoriteService;

	@GetMapping(value = "/")
	@ResponseBody
	public ModelAndView display(@RequestParam(value = "attribute", required = false) String attribute,
			@RequestParam(value = "type", required = false) String type) {
		ModelAndView view = new ModelAndView();
		view.setViewName("song");

		String orderBy = "";
		if (attribute == null || type == null) {
			attribute = "id";
			type = "1";
			orderBy = "asc";
		} else {
			if (Integer.parseInt(type) == 1) {
				orderBy = "asc";
			} else {
				orderBy = "desc";
			}
		}
		view.addObject("attribute", attribute);// set attribute after sort
		view.addObject("type", type);// set type sort after sort
		view.addObject("songs", songService.sort(attribute, orderBy));
		view.addObject("artists", artistService.sort("id", "asc"));
		return view;
	}

	@PostMapping(value = "/add")
	@ResponseBody
	public int add(@RequestBody Song song) {
		songService.insert(song);
		return songService.getLastedId();
	}

	@PostMapping(value = "/update")
	@ResponseBody
	public void update(@RequestBody Song song) {
		Song songTmp = songService.getById(song.getId());
		song.setLiked(songTmp.getLiked());
		song.setListen(songTmp.getListen());
		songService.update(song);
	}

	@DeleteMapping(value = "/delete")
	@ResponseBody
	public void delete(@RequestBody Song song) {
//		System.out.println(genre.getName());
		song_GenreService.deleteBySongId(song.getId());
		song_ArtistService.deleteBySongId(song.getId());
		songService.delete(song);
	}

	@GetMapping("/song")
	@ResponseBody
	public Song getArtist(@RequestParam("id") String id) {
//		ObjectMapper objectMapper = new ObjectMapper();
//		try {
//			System.out.println(objectMapper.writeValueAsString(songService.getById2(Integer.parseInt(id)).getSong_Artists()));
//		} catch (NumberFormatException | JsonProcessingException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		return songService.getById2(Integer.parseInt(id));
	}

	@GetMapping("/artists")
	@ResponseBody
	public List<Artist> getArtistsOfSong(@RequestParam("id") String id) {
		return song_ArtistService.getArtistsOfSong(Integer.parseInt(id));
	}

	@GetMapping("/genres")
	@ResponseBody
	public List<Genre> getGenresOfSong(@RequestParam("id") String id) {
		return song_GenreService.getGenresOfSong(Integer.parseInt(id));
	}

//	@GetMapping("/topsongs")
//	@ResponseBody
//	public List<Song> getTopSongs() {
//		List<Song> list = songService.getTopSong(-1, 0);
//		for (Song s : list) {
//			s.setName(s.getName() + "~" + s.getSong_Artists().get(0).getId().getArtist().getName() + "~"
//					+ s.getSong_Artists().get(0).getId().getArtist().getImg());
//		}
//		return list;
//	}

	@GetMapping("/topsongs")
	@ResponseBody
	public List<Object> getTopSongs(@RequestParam("type") Integer type, HttpSession session) {
		List<Object> objs = new ArrayList<Object>();
		User user = null;
		if (session.getAttribute("user") != null)
			user = (User) session.getAttribute("user");
		List<Song> list = (type == 1) ?
				songService.getTopSongLike(-1, 0) :
					songService.getTopSongListen(-1, 0);
		List<Integer> songIdLiked = new ArrayList<Integer>();
		List<Object> objs_sub = new ArrayList<Object>();
		List<Artist> artists = new ArrayList<Artist>();
		for (Song s : list) {
			if (user != null) {
				if (favoriteService.isLiked(s.getId(), user.getId()))
					songIdLiked.add(s.getId());
				else
					songIdLiked.add(-1);

			} else {
				songIdLiked.add(-1);
			}
			
			artists = new ArrayList<Artist>();
			for (Song_Artist ss : s.getSong_Artists()) {
				artists.add(ss.getId().getArtist());
			}
			objs_sub.add(artists);
		}
		objs.add(list);
		objs.add(songIdLiked);
		objs.add(objs_sub);
		return objs;
	}

	@GetMapping("/getsong")
	@ResponseBody
	public List<Object> getASong(@RequestParam("id") String id) {
		List<Object> objs = new ArrayList<Object>();
		Song song = songService.getById2(Integer.parseInt(id));
		List<Artist> artists = new ArrayList<Artist>();
		for(Song_Artist s : song.getSong_Artists())
			artists.add(s.getId().getArtist());
		objs.add(song);
		objs.add(artists);
		return objs;
	}

	@GetMapping("/favoritesongs")
	@ResponseBody
	public List<Object> getFavoriteSongs(HttpSession session) {
		List<Object> objs = new ArrayList<Object>();
		User user = null;
		if (session.getAttribute("user") != null)
			user = (User) session.getAttribute("user");
		if (user != null) {
			List<Song> list = songService.getFavoriteSong(user.getId());
			List<Integer> songIdLiked = new ArrayList<Integer>();
			List<Object> objs_sub = new ArrayList<Object>();
			List<Artist> artists = new ArrayList<Artist>();
			for (Song s : list) {
				if (favoriteService.isLiked(s.getId(), user.getId()))
					songIdLiked.add(s.getId());
				else
					songIdLiked.add(-1);
				artists = new ArrayList<Artist>();
				for (Song_Artist ss : s.getSong_Artists()) {
					artists.add(ss.getId().getArtist());
				}
				objs_sub.add(artists);
			}
			objs.add(list);
			objs.add(songIdLiked);
			objs.add(objs_sub);
			return objs;
		}
		return objs;
	}
	
	@GetMapping(value = "/getsong", produces = "text/plain;charset=utf-8")
	@ResponseBody
	public String getResource(@RequestParam("id") String id) {
		return songService.getResource(Integer.parseInt(id));
	}
}
