package com.musicweb.controller;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

@Controller
@RequestMapping(value = "/fileUploadController")
public class FileUploadController {
	@Autowired
	ServletContext context;

	@GetMapping
	public String display() {
		return "upload";
	}

	@PostMapping(value = "/upload")
	@ResponseBody
	public void upload(MultipartHttpServletRequest httpServletRequest, @RequestParam("type") int type) {
		// context.getRealPath là nó đã ở webapp folder
		String realPath = context.getRealPath("/resources/img");
		if (type == 1) {// upload image
			realPath = context.getRealPath("/resources/img");
		} else if (type == 2) {// upload song
			realPath = context.getRealPath("/resources/audio");
		}
		File file;
		List<MultipartFile> fileNames = httpServletRequest.getFiles("file");
		MultipartFile mpfFile;
		String filename = "";
		for (MultipartFile m : fileNames) {
			System.out.println(m.getName());
			mpfFile = m;
			file = new File(realPath, mpfFile.getOriginalFilename());
			System.out.println("/resources/img/" + mpfFile.getOriginalFilename());
			try {
				mpfFile.transferTo(file);
			} catch (IllegalStateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
//		System.out.println(realPath);
	}
}
