package com.musicweb.service;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.musicweb.dao.Dao;
import com.musicweb.model.Album;
import com.musicweb.model.Artist;
import com.musicweb.model.User;

@Transactional
@Service
public class UserService extends Dao<User> {
	@Autowired
	private SessionFactory sessionFactory;
	@Autowired
	private Dao<User> UserDao;
	private String hql;

	@Override
	public User getById(Integer id) {
		return UserDao.getById(id);
	}

	@Override
	public List<User> getAll() {
		return UserDao.getAll();
	}

	@Override
	public Boolean insert(User t) {
		return UserDao.insert(t);
	}

	@Override
	public Boolean delete(User t) {
		return UserDao.delete(t);
	}

	@Override
	public Boolean update(User t) {
		return UserDao.update(t);
	}
	
	public User getByEmail(String email) {
		String hql = "from User where email like :email";
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery(hql);
		query.setParameter("email", email);
		if(query.getResultList().size() > 0)
			return (User) query.getSingleResult();
		return null;
	}
	
	public List<User> sort(String attribute, String orderBy) {
		Session session = sessionFactory.getCurrentSession();
//		String hql = "from Genre order by :attribute :orderBy";
		String hql = "from User order by " + attribute + " " + orderBy;
		Query query = session.createQuery(hql);
//		query.setParameter("attribute", attribute);
//		query.setParameter("orderBy", orderBy);
		for(User a : (List<User>)query.getResultList()) {
			a.getComments().size();
			a.getFavorites().size();
		}
		return query.getResultList();
	}
	
	public List<User> search_admin(String content) {
		content = content.trim();
		// kiểm tra search content có phải chỉ là id hay tên
		int id = 0;
		String search_name = "select * from user where name like '%#content_1%'" + " or name like '%#content_2%'";
		String search_id = "select * from user where id = #id";
		Session session = sessionFactory.getCurrentSession();
		String hql = "";
		try {
			Integer.parseInt(content.replaceAll("\\s{1}", ""));
			id = Integer.parseInt(content.replaceAll("\\s{1}", ""));
			search_id = search_id.replace("#id", id + "");
			hql = search_id;
		} catch (Exception e) {
			search_name = search_name.replace("#content_1", content);
			content = content.replaceAll("\\s{2,}", " ");
			search_name = search_name.replace("#content_2", content);
			hql = search_name;
		}
		// end kiểm tra search content có phải chỉ là id hay tên
		Query query = session.createNativeQuery(hql).addEntity(User.class);
		return query.getResultList();
	}

	public int getLastedId() {
		Session session = sessionFactory.getCurrentSession();
		String hql = "select max(id) from User";
		Query query = session.createSQLQuery(hql);
		return (int) query.getSingleResult();
	}
}
