package com.musicweb.service;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.musicweb.dao.Dao;
import com.musicweb.model.Album_Song;
import com.musicweb.model.Genre;
import com.musicweb.model.Song_Genre;

@Transactional
@Service
public class Song_GenreService extends Dao<Song_Genre> {
	@Autowired
	private SessionFactory sessionFactory;
	@Autowired
	private Dao<Song_Genre> Song_GenreDao;
	private String hql;

	@Override
	public Song_Genre getById(Integer id) {
		return Song_GenreDao.getById(id);
	}

	@Override
	public List<Song_Genre> getAll() {
		return Song_GenreDao.getAll();
	}

	@Override
	public Boolean insert(Song_Genre t) {
		return Song_GenreDao.insert(t);
	}

	@Override
	public Boolean delete(Song_Genre t) {
		return Song_GenreDao.delete(t);
	}

	@Override
	public Boolean update(Song_Genre t) {
		return Song_GenreDao.update(t);
	}

	public void deleteBySongId(int id) {
		Session session = sessionFactory.getCurrentSession();
		String hql = "delete from Song_Genre where id.song.id = :id";
		Query query = session.createQuery(hql);
		query.setParameter("id", id);
		query.executeUpdate();
	}

	public List<Genre> getGenresOfSong(int id) {
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Genre a where a.id in (select id.genre.id from Song_Genre where id.song.id = :id)";
		Query query = session.createQuery(hql);
		query.setParameter("id", id);
		return query.getResultList();
	}

	public List<Song_Genre> getSongsOfGenre(int id) {
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Song_Genre where id.genre.id = :id";
		Query query = session.createQuery(hql);
		query.setParameter("id", id);
		for(Song_Genre s: (List<Song_Genre>)query.getResultList())
			s.getId().getSong().getSong_Artists().size();
		return query.getResultList();
	}
}
