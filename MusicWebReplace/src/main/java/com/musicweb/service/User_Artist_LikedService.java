package com.musicweb.service;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.musicweb.dao.Dao;
import com.musicweb.model.User_Artist_Liked;

@Transactional
@Service
public class User_Artist_LikedService extends Dao<User_Artist_Liked> {
	@Autowired
	private SessionFactory sessionFactory;
	@Autowired
	private Dao<User_Artist_Liked> User_Artist_LikedDao;
	private String hql;

	@Override
	public User_Artist_Liked getById(Integer id) {
		return User_Artist_LikedDao.getById(id);
	}

	@Override
	public List<User_Artist_Liked> getAll() {
		return User_Artist_LikedDao.getAll();
	}

	@Override
	public Boolean insert(User_Artist_Liked t) {
		return User_Artist_LikedDao.insert(t);
	}

	@Override
	public Boolean delete(User_Artist_Liked t) {
		return User_Artist_LikedDao.delete(t);
	}

	@Override
	public Boolean update(User_Artist_Liked t) {
		return User_Artist_LikedDao.update(t);
	}
	
	public boolean isLiked(int artistid, int userid) {
		Session session = sessionFactory.getCurrentSession();
		String hql = "from User_Artist_Liked where id.user.id = :userid "
				+ "and id.artist.id = :artistid";
		Query query = session.createQuery(hql);
		query.setParameter("userid", userid);
		query.setParameter("artistid", artistid);
		if(query.getResultList().size() <= 0)
			return false;
		return true;
	}
}
