package com.musicweb.service;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.musicweb.dao.Dao;
import com.musicweb.model.Favorite;

@Transactional
@Service
public class FavoriteService extends Dao<Favorite> {
	@Autowired
	private SessionFactory sessionFactory;
	@Autowired
	private Dao<Favorite> FavoriteDao;
	private String hql;

	@Override
	public Favorite getById(Integer id) {
		return FavoriteDao.getById(id);
	}

	@Override
	public List<Favorite> getAll() {
		return FavoriteDao.getAll();
	}

	@Override
	public Boolean insert(Favorite t) {
		return FavoriteDao.insert(t);
	}

	@Override
	public Boolean delete(Favorite t) {
		return FavoriteDao.delete(t);
	}

	@Override
	public Boolean update(Favorite t) {
		return FavoriteDao.update(t);
	}
	
	public boolean isLiked(int songid, int userid) {
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Favorite where id.user.id = :userid "
				+ "and id.song.id = :songid";
		Query query = session.createQuery(hql);
		query.setParameter("userid", userid);
		query.setParameter("songid", songid);
		if(query.getResultList().size() <= 0)
			return false;
		return true;
	}
	
	public List<Integer> songIdLiked(int userid) {
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Favorite where id.user.id";
		Query query = session.createQuery(hql);
		query.setParameter("userid", userid);
		return query.getResultList();
	}
}
