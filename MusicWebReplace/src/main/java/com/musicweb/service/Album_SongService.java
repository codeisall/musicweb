package com.musicweb.service;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.musicweb.dao.Dao;
import com.musicweb.model.Album_Song;

@Transactional
@Service
public class Album_SongService extends Dao<Album_Song> {
	@Autowired
	private SessionFactory sessionFactory;
	@Autowired
	private Dao<Album_Song> album_SongDao;

	@Override
	public Album_Song getById(Integer id) {
		return album_SongDao.getById(id);
	}

	@Override
	public List<Album_Song> getAll() {
		return album_SongDao.getAll();
	}

	@Override
	public Boolean insert(Album_Song t) {
		return album_SongDao.insert(t);
	}

	@Override
	public Boolean delete(Album_Song t) {
		return album_SongDao.delete(t);
	}

	@Override
	public Boolean update(Album_Song t) {
		return album_SongDao.update(t);
	}
	
	public void deleteByAlbumId(int id) {
		Session session = sessionFactory.getCurrentSession();
		String hql = "delete from Album_Song where id.album.id = :id";
		Query query = session.createQuery(hql);
		query.setParameter("id", id);
		query.executeUpdate();
	}
	
	public List<Album_Song> getSongsOfAlbum(int id) {
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Album_Song where id.album.id = :id";
		Query query = session.createQuery(hql);
		query.setParameter("id", id);
		for(Album_Song s : (List<Album_Song>)query.getResultList()) {
			s.getId().getSong().getSong_Artists().size();
		}
		return query.getResultList();
	}
}
