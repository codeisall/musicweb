package com.musicweb.service;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.musicweb.dao.Dao;
import com.musicweb.model.Artist;
import com.musicweb.model.Genre;

@Transactional
@Service
public class ArtistService extends Dao<Artist> {
	@Autowired
	private SessionFactory sessionFactory;
	@Autowired
	private Dao<Artist> ArtistDao;
	private String hql;

	@Override
	public Artist getById(Integer id) {
		return ArtistDao.getById(id);
	}

	@Override
	public List<Artist> getAll() {
		return ArtistDao.getAll();
	}

	@Override
	public Boolean insert(Artist t) {
		return ArtistDao.insert(t);
	}

	@Override
	public Boolean delete(Artist t) {
		return ArtistDao.delete(t);
	}

	@Override
	public Boolean update(Artist t) {
		return ArtistDao.update(t);
	}

	public int getLastedId() {
		Session session = sessionFactory.getCurrentSession();
		String hql = "select max(id) from Artist";
		Query query = session.createSQLQuery(hql);
		return (int) query.getSingleResult();
	}

	public List<Artist> sort(String attribute, String orderBy) {
		Session session = sessionFactory.getCurrentSession();
//		String hql = "from Genre order by :attribute :orderBy";
		String hql = "from Artist order by " + attribute + " " + orderBy;
		Query query = session.createQuery(hql);
//		query.setParameter("attribute", attribute);
//		query.setParameter("orderBy", orderBy);
		for (Artist a : (List<Artist>) query.getResultList()) {
			a.getArtist_Genres().size();
		}
		return query.getResultList();
	}

	public List<Genre> getGenresByArtistId(int id) {
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Genre where id in "
				+ "(select id.genre.id from Artist_Genre where id.artist.id = :artist_id)";
		Query query = session.createQuery(hql);
		query.setParameter("artist_id", id);
		return query.getResultList();
	}

	public List<Genre> getGenresByArtistIds(List<Integer> ids) {
		Session session = sessionFactory.getCurrentSession();
		String hql = "select * from genre where id in (#subquery)";
		String hqlSubQuery = "";
		for (Integer i : ids) {
			if (ids.indexOf(i) != ids.size() - 1) {
				hqlSubQuery += "(select genre_id from artist_genre where artist_id = " + i + ") union";
			} else {
				hqlSubQuery += "(select genre_id from artist_genre where artist_id = " + i + ")";
			}
		}
		hql = hql.replace("#subquery", hqlSubQuery);
		System.out.println(hql);
		Query query = session.createSQLQuery(hql).addEntity(Genre.class);
		return query.getResultList();
	}

	public List<Artist> getTopArtist(int limit) {
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Artist order by liked desc";
		Query query = session.createQuery(hql);
		query.setMaxResults(limit);
		for (Artist a : (List<Artist>) query.getResultList()) {
			a.getArtist_Genres().size();
		}
		return query.getResultList();
	}

	public Artist getById2(int id) {
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Artist where id = :id";
		Query query = session.createQuery(hql);
		query.setParameter("id", id);
		for (Artist a : (List<Artist>) query.getResultList()) {
			a.getSong_Artists().size();
			a.getAlbums().size();
			a.getArtist_Genres().size();
		}
		return (Artist) query.getResultList().get(0);
	}

	public List<Artist> getArtitsOfGenre(int id) {
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Artist";
		if (id > 0) {
			hql += " where id in " + "(select id.artist.id from Artist_Genre where id.genre.id = :id)";
		}
		Query query = session.createQuery(hql);
		if (id > 0) {
			query.setParameter("id", id);
		}
		for (Artist a : (List<Artist>) query.getResultList()) {
			a.getArtist_Genres().size();
		}
		return query.getResultList();
	}
	
	public List<Artist> search(String content) {
		Session session = sessionFactory.getCurrentSession();
		String hql = "select * from artist where name like '%#words%'";
		hql = hql.replace("#words", content);
		Query query = session.createNativeQuery(hql).addEntity(Artist.class);
		return query.getResultList();
	}
	
	public List<Artist> search_admin(String content) {
		content = content.trim();
		// kiểm tra search content có phải chỉ là id hay tên
		int id = 0;
		String search_name = "select * from artist where name like '%#content_1%'" + " or name like '%#content_2%'";
		String search_id = "select * from artist where id = #id";
		Session session = sessionFactory.getCurrentSession();
		String hql = "";
		try {
			Integer.parseInt(content.replaceAll("\\s{1}", ""));
			id = Integer.parseInt(content.replaceAll("\\s{1}", ""));
			search_id = search_id.replace("#id", id + "");
			hql = search_id;
		} catch (Exception e) {
			search_name = search_name.replace("#content_1", content);
			content = content.replaceAll("\\s{2,}", " ");
			search_name = search_name.replace("#content_2", content);
			hql = search_name;
		}
		// end kiểm tra search content có phải chỉ là id hay tên
		Query query = session.createNativeQuery(hql).addEntity(Artist.class);
		return query.getResultList();
	}
}
