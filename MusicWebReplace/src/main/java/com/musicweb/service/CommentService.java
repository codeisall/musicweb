package com.musicweb.service;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.musicweb.dao.Dao;
import com.musicweb.model.Comment;

@Transactional
@Service
public class CommentService extends Dao<Comment> {
	@Autowired
	private SessionFactory sessionFactory;
	@Autowired
	private Dao<Comment> CommentDao;
	private String hql;

	@Override
	public Comment getById(Integer id) {
		return CommentDao.getById(id);
	}

	@Override
	public List<Comment> getAll() {
		return CommentDao.getAll();
	}

	@Override
	public Boolean insert(Comment t) {
		return CommentDao.insert(t);
	}

	@Override
	public Boolean delete(Comment t) {
		return CommentDao.delete(t);
	}

	@Override
	public Boolean update(Comment t) {
		return CommentDao.update(t);
	}
	
	public List<Comment> getCommentsOfASong(int id) {
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Comment where song_id = :id";
		Query query = session.createQuery(hql);
		query.setParameter("id", id);
		return query.getResultList();
	}
	
	public Comment getLastComment() {
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Comment where id = "
				+ "(select max(c.id) from Comment c)";
		Query query = session.createQuery(hql);
		return (Comment) query.getSingleResult();
	}
}
