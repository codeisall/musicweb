package com.musicweb.service;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.musicweb.dao.Dao;
import com.musicweb.model.Album;
import com.musicweb.model.Artist;
import com.musicweb.model.Song;

@Transactional
@Service
public class AlbumService extends Dao<Album> {
	@Autowired
	private SessionFactory sessionFactory;
	@Autowired
	private Dao<Album> AlbumDao;
	private String hql;

	@Override
	public Album getById(Integer id) {
		return AlbumDao.getById(id);
	}

	@Override
	public List<Album> getAll() {
		return AlbumDao.getAll();
	}

	@Override
	public Boolean insert(Album t) {
		return AlbumDao.insert(t);
	}

	@Override
	public Boolean delete(Album t) {
		return AlbumDao.delete(t);
	}

	@Override
	public Boolean update(Album t) {
		return AlbumDao.update(t);
	}

	public int getLastedId() {
		Session session = sessionFactory.getCurrentSession();
		String hql = "select max(id) from Album";
		Query query = session.createSQLQuery(hql);
		return (int) query.getSingleResult();
	}

	public List<Album> sort(String attribute, String orderBy) {
		Session session = sessionFactory.getCurrentSession();
//		String hql = "from Genre order by :attribute :orderBy";
		String hql = "from Album order by " + attribute + " " + orderBy;
		Query query = session.createQuery(hql);
//		query.setParameter("attribute", attribute);
//		query.setParameter("orderBy", orderBy);
		for (Album a : (List<Album>) query.getResultList()) {
			a.getArtist();
		}
		return query.getResultList();
	}

	public List<Song> getSongsOfAlbum(int id) {
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Song where id in " + "(select id.song.id from Album_Song where id.album.id = :id)";
		Query query = session.createQuery(hql);
		query.setParameter("id", id);
		return query.getResultList();
	}

	public List<Album> getTopAlbum(int limit) {
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Album order by liked desc";
		Query query = session.createQuery(hql);
		query.setMaxResults(limit);
		return query.getResultList();
	}

//	public List<Album> getAlbumsOfArtist(int id) {
//		Session session = sessionFactory.getCurrentSession();
//		String hql = "from Artist where id = :id";
//		Query query = session.createQuery(hql);
//		query.setParameter("id", id);
//		for(Artist a : (List<Artist>)query.getResultList()) {
//			a.getAlbums().size();
//		}
//		return ((Artist)query.getSingleResult()).getAlbums();
//	}

	public List<Album> getAlbumsOfArtist(int id) {
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Artist";
		if (id > 0) {
			hql = "from Artist where id = :id";
		} else {
			hql = "from Album";
		}
		Query query = session.createQuery(hql);
		if (id > 0) {
			query.setParameter("id", id);
			for (Artist a : (List<Artist>) query.getResultList()) {
				a.getAlbums().size();
			}
			return ((Artist)query.getSingleResult()).getAlbums();
		}
		return query.getResultList();
	}
	
	public List<Album> search(String content) {
		Session session = sessionFactory.getCurrentSession();
		String hql = "select * from album where name like '%#words%'";
		hql = hql.replace("#words", content);
		Query query = session.createNativeQuery(hql).addEntity(Album.class);
		return query.getResultList();
	}
	
	public List<Album> search_admin(String content) {
		content = content.trim();
		// kiểm tra search content có phải chỉ là id hay tên
		int id = 0;
		String search_name = "select * from album where name like '%#content_1%'" + " or name like '%#content_2%'";
		String search_id = "select * from album where id = #id";
		Session session = sessionFactory.getCurrentSession();
		String hql = "";
		try {
			Integer.parseInt(content.replaceAll("\\s{1}", ""));
			id = Integer.parseInt(content.replaceAll("\\s{1}", ""));
			search_id = search_id.replace("#id", id + "");
			hql = search_id;
		} catch (Exception e) {
			search_name = search_name.replace("#content_1", content);
			content = content.replaceAll("\\s{2,}", " ");
			search_name = search_name.replace("#content_2", content);
			hql = search_name;
		}
		// end kiểm tra search content có phải chỉ là id hay tên
		Query query = session.createNativeQuery(hql).addEntity(Album.class);
		return query.getResultList();
	}
}
