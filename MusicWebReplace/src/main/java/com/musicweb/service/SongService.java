package com.musicweb.service;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.musicweb.dao.Dao;
import com.musicweb.model.Artist;
import com.musicweb.model.Song;

@Transactional
@Service
public class SongService extends Dao<Song> {
	@Autowired
	private SessionFactory sessionFactory;
	@Autowired
	private Dao<Song> SongDao;
	private String hql;

	@Override
	public Song getById(Integer id) {
		return SongDao.getById(id);
	}

	@Override
	public List<Song> getAll() {
		return SongDao.getAll();
	}

	@Override
	public Boolean insert(Song t) {
		return SongDao.insert(t);
	}

	@Override
	public Boolean delete(Song t) {
		return SongDao.delete(t);
	}

	@Override
	public Boolean update(Song t) {
		return SongDao.update(t);
	}
	
	public int getLastedId() {
		Session session = sessionFactory.getCurrentSession();
		String hql = "select max(id) from Song";
		Query query = session.createSQLQuery(hql);
		return (int) query.getSingleResult();
	}
	
	public Song getById2(int id) {
		Session session =  sessionFactory.getCurrentSession();
		String hql = "from Song where id = :id";
		Query query = session.createQuery(hql);
		query.setParameter("id", id);
		for(Song a : (List<Song>)query.getResultList()) {
			a.getSong_Artists().size();
			a.getSong_Genres().size();
		}
		return (Song) query.getSingleResult();
	}
	
	public List<Song> sort(String attribute, String orderBy) {
		Session session = sessionFactory.getCurrentSession();
//		String hql = "from Genre order by :attribute :orderBy";
		String hql = "from Song order by " + attribute + " " + orderBy;
		Query query = session.createQuery(hql);
//		query.setParameter("attribute", attribute);
//		query.setParameter("orderBy", orderBy);
		for(Song a : (List<Song>)query.getResultList()) {
			a.getSong_Artists().size();
			a.getSong_Genres().size();
		}
		return query.getResultList();
	}
	
	public List<Song> getTopSongLike(int limit, int start) {
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Song order by liked desc";
		Query query = session.createQuery(hql);
		query.setFirstResult(start);
		if(limit != -1)
			query.setMaxResults(limit);
		for (Song s : (List<Song>)query.getResultList())
			s.getSong_Artists().size();
		return query.getResultList();
	}
	
	public List<Song> getTopSongListen(int limit, int start) {
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Song order by listen desc";
		Query query = session.createQuery(hql);
		query.setFirstResult(start);
		if(limit != -1)
			query.setMaxResults(limit);
		for (Song s : (List<Song>)query.getResultList())
			s.getSong_Artists().size();
		return query.getResultList();
	}
	
	public List<Song> getFavoriteSong(int userid) {
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Song where id in "
				+ "(select id.song.id from Favorite where id.user.id = :userid)";
		Query query = session.createQuery(hql);
		query.setParameter("userid", userid);
		for (Song s : (List<Song>)query.getResultList())
			s.getSong_Artists().size();
		return query.getResultList();
	}
	
	public List<Song> search(String content) {
		Session session = sessionFactory.getCurrentSession();
		String hql = "select * from song where name like '%#words%'";
		hql = hql.replace("#words", content);
		Query query = session.createNativeQuery(hql).addEntity(Song.class);
		return query.getResultList();
	}
	
	public List<Song> search_admin(String content) {
		content = content.trim();
		// kiểm tra search content có phải chỉ là id hay tên
		int id = 0;
		String search_name = "select * from song where name like '%#content_1%'" + " or name like '%#content_2%'";
		String search_id = "select * from song where id = #id";
		Session session = sessionFactory.getCurrentSession();
		String hql = "";
		try {
			Integer.parseInt(content.replaceAll("\\s{1}", ""));
			id = Integer.parseInt(content.replaceAll("\\s{1}", ""));
			search_id = search_id.replace("#id", id + "");
			hql = search_id;
		} catch (Exception e) {
			search_name = search_name.replace("#content_1", content);
			content = content.replaceAll("\\s{2,}", " ");
			search_name = search_name.replace("#content_2", content);
			hql = search_name;
		}
		// end kiểm tra search content có phải chỉ là id hay tên
		Query query = session.createNativeQuery(hql).addEntity(Song.class);
		return query.getResultList();
	}
	
	public String getResource(int id) {
		Session session =  sessionFactory.getCurrentSession();
		String hql = "select resource from Song where id = :id";
		Query query = session.createQuery(hql);
		query.setParameter("id", id);
		return (String) query.getSingleResult();
	}
}
