package com.musicweb.service;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.musicweb.dao.Dao;
import com.musicweb.model.Album;
import com.musicweb.model.Genre;
import com.musicweb.model.Song;

@Transactional
@Service
public class GenreService extends Dao<Genre> {
	@Autowired
	private SessionFactory sessionFactory;
	@Autowired
	private Dao<Genre> GenreDao;
	private String hql;

	@Override
	public Genre getById(Integer id) {
		return GenreDao.getById(id);
	}

	@Override
	public List<Genre> getAll() {
		return GenreDao.getAll();
	}

	@Override
	public Boolean insert(Genre t) {
		return GenreDao.insert(t);
	}

	@Override
	public Boolean delete(Genre t) {
		return GenreDao.delete(t);
	}

	@Override
	public Boolean update(Genre t) {
		return GenreDao.update(t);
	}

	public List<Genre> sort(String attribute, String orderBy) {
		Session session = sessionFactory.getCurrentSession();
//		String hql = "from Genre order by :attribute :orderBy";
		String hql = "from Genre order by " + attribute + " " + orderBy;
		Query query = session.createQuery(hql);
//		query.setParameter("attribute", attribute);
//		query.setParameter("orderBy", orderBy);
		return query.getResultList();
	}

	public List<Album> getTopGenre(int limit) {
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Genre order by listen desc";
		Query query = session.createQuery(hql);
		query.setMaxResults(limit);
		return query.getResultList();
	}

	public List<Genre> search(String content) {
		Session session = sessionFactory.getCurrentSession();
		String hql = "select * from genre where name like '%#words%'";
		hql = hql.replace("#words", content);
		Query query = session.createNativeQuery(hql).addEntity(Genre.class);
		return query.getResultList();
	}

	public List<Genre> search_admin(String content) {
		content = content.trim();
		// kiểm tra search content có phải chỉ là id hay tên
		int id = 0;
		String search_name = "select * from genre where name like '%#content_1%'" + " or name like '%#content_2%'";
		String search_id = "select * from genre where id = #id";
		Session session = sessionFactory.getCurrentSession();
		String hql = "";
		try {
			Integer.parseInt(content.replaceAll("\\s{1}", ""));
			id = Integer.parseInt(content.replaceAll("\\s{1}", ""));
			search_id = search_id.replace("#id", id + "");
			hql = search_id;
		} catch (Exception e) {
			search_name = search_name.replace("#content_1", content);
			content = content.replaceAll("\\s{2,}", " ");
			search_name = search_name.replace("#content_2", content);
			hql = search_name;
		}
		// end kiểm tra search content có phải chỉ là id hay tên
		Query query = session.createNativeQuery(hql).addEntity(Genre.class);
		return query.getResultList();
	}

	public static void main(String[] args) {
		try {
			System.out.println("nguyen   lam thanh    1".replaceAll("\\s{1}", ""));
			Integer.parseInt("   1    ".replaceAll("\\s{1}", ""));
		} catch (Exception e) {
			System.out.println(e);
		}
	}
}
