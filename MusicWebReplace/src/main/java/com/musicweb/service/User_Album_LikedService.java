package com.musicweb.service;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.musicweb.dao.Dao;
import com.musicweb.model.User_Album_Liked;

@Transactional
@Service
public class User_Album_LikedService extends Dao<User_Album_Liked> {
	@Autowired
	private SessionFactory sessionFactory;
	@Autowired
	private Dao<User_Album_Liked> User_Album_LikedDao;
	private String hql;

	@Override
	public User_Album_Liked getById(Integer id) {
		return User_Album_LikedDao.getById(id);
	}

	@Override
	public List<User_Album_Liked> getAll() {
		return User_Album_LikedDao.getAll();
	}

	@Override
	public Boolean insert(User_Album_Liked t) {
		return User_Album_LikedDao.insert(t);
	}

	@Override
	public Boolean delete(User_Album_Liked t) {
		return User_Album_LikedDao.delete(t);
	}

	@Override
	public Boolean update(User_Album_Liked t) {
		return User_Album_LikedDao.update(t);
	}
	
	public boolean isLiked(int albumid, int userid) {
		Session session = sessionFactory.getCurrentSession();
		String hql = "from User_Album_Liked where id.user.id = :userid "
				+ "and id.album.id = :albumid";
		Query query = session.createQuery(hql);
		query.setParameter("userid", userid);
		query.setParameter("albumid", albumid);
		if(query.getResultList().size() <= 0)
			return false;
		return true;
	}
}
