package com.musicweb.service;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.musicweb.dao.Dao;
import com.musicweb.model.Artist;
import com.musicweb.model.Song;
import com.musicweb.model.Song_Artist;

@Transactional
@Service
public class Song_ArtistService extends Dao<Song_Artist> {
	@Autowired
	private SessionFactory sessionFactory;
	@Autowired
	private Dao<Song_Artist> Song_ArtistDao;
	private String hql;

	@Override
	public Song_Artist getById(Integer id) {
		return Song_ArtistDao.getById(id);
	}

	@Override
	public List<Song_Artist> getAll() {
		return Song_ArtistDao.getAll();
	}

	@Override
	public Boolean insert(Song_Artist t) {
		return Song_ArtistDao.insert(t);
	}

	@Override
	public Boolean delete(Song_Artist t) {
		return Song_ArtistDao.delete(t);
	}

	@Override
	public Boolean update(Song_Artist t) {
		return Song_ArtistDao.update(t);
	}
	
	public void deleteBySongId(int id) {
		Session session = sessionFactory.getCurrentSession();
		String hql = "delete from Song_Artist where id.song.id = :id";
		Query query = session.createQuery(hql);
		query.setParameter("id", id);
		query.executeUpdate();
	}
	
	public List<Artist> getArtistsOfSong(int id) {
		Session session =  sessionFactory.getCurrentSession();
		String hql = "from Artist a where a.id in (select id.artist.id from Song_Artist where id.song.id = :id)";
		Query query = session.createQuery(hql);
		query.setParameter("id", id);
		return query.getResultList();
	}
	
//	public List<Song> getSongsOfArtist(int id) {
//		Session session =  sessionFactory.getCurrentSession();
//		String hql = "from Song a where a.id in (select id.song.id from Song_Artist where id.artist.id = :id)";
//		Query query = session.createQuery(hql);
//		query.setParameter("id", id);
//		for(Song s : (List<Song>)query.getResultList())
//			s.getSong_Artists().size();
//		return query.getResultList();
//	}
	
	public List<Song> getSongsOfArtist(List<Integer> id) {
		Session session =  sessionFactory.getCurrentSession();
		String tmp_1 = "select * from song where id in "
				+ "(select song_id from song_artist where artist_id = #id)";
		String tmp_2 = "";
		String hql = "";
		for(int i = 0; i < id.size(); i++) {
			tmp_2 = tmp_1;
			tmp_2 = tmp_2.replace("#id", id.get(i) + "");
			if(i != id.size() - 1)
				tmp_2 += "union distinct \n";
			hql += tmp_2;
		}
		System.out.println(hql);
		Query query = session.createNativeQuery(hql).addEntity(Song.class);
//		query.setParameter("id", id);
		for(Song s : (List<Song>)query.getResultList())
			s.getSong_Artists().size();
		return query.getResultList();
	}
}
