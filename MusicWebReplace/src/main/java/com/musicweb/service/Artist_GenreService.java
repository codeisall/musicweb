package com.musicweb.service;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.musicweb.dao.Dao;
import com.musicweb.model.Artist_Genre;

@Transactional
@Service
public class Artist_GenreService extends Dao<Artist_Genre> {
	@Autowired
	private SessionFactory sessionFactory;
	@Autowired
	private Dao<Artist_Genre> Artist_GenreDao;
	private String hql;

	@Override
	public Artist_Genre getById(Integer id) {
		return Artist_GenreDao.getById(id);
	}

	@Override
	public List<Artist_Genre> getAll() {
		return Artist_GenreDao.getAll();
	}

	@Override
	public Boolean insert(Artist_Genre t) {
		return Artist_GenreDao.insert(t);
	}

	@Override
	public Boolean delete(Artist_Genre t) {
		return Artist_GenreDao.delete(t);
	}

	@Override
	public Boolean update(Artist_Genre t) {
		return Artist_GenreDao.update(t);
	}
	
	public void deleteByArtistId(int id) {
		Session session = sessionFactory.getCurrentSession();
		String hql = "delete from Artist_Genre where id.artist.id = :id";
		Query query = session.createQuery(hql);
		query.setParameter("id", id);
		query.executeUpdate();
	}
}
