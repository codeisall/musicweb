package com.musicweb.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.musicweb.model.Album_Song;

@Repository
public class Album_SongDao extends Dao<Album_Song>{
	@Autowired
	SessionFactory sessionFactory;

	@Override
	public Album_Song getById(Integer id) {
		return sessionFactory.getCurrentSession().get(Album_Song.class, id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Album_Song> getAll() {
		return (List<Album_Song>)sessionFactory.getCurrentSession().createQuery("from Album_Song").list();
	}

	@Override
	public Boolean insert(Album_Song t) {
		try {
			sessionFactory.getCurrentSession().persist(t);
		} catch (Exception e) {
			return Boolean.FALSE;
		}
		return Boolean.TRUE;
	}

	@Override
	public Boolean delete(Album_Song t) {
		try {
			sessionFactory.getCurrentSession().remove(t);
		} catch (Exception e) {
			return Boolean.FALSE;
		}
		return Boolean.TRUE;
	}

	@Override
	public Boolean update(Album_Song t) {
		try {
			sessionFactory.getCurrentSession().update(t);
		} catch (Exception e) {
			return Boolean.FALSE;
		}
		return Boolean.TRUE;
	}

}
