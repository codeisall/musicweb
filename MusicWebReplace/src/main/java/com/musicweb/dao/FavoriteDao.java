package com.musicweb.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.musicweb.model.Favorite;

@Repository
public class FavoriteDao extends Dao<Favorite> {
	@Autowired
	SessionFactory sessionFactory;

	@Override
	public Favorite getById(Integer id) {
		return sessionFactory.getCurrentSession().get(Favorite.class, id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Favorite> getAll() {
		return (List<Favorite>)sessionFactory.getCurrentSession().createQuery("from Favorite").list();
	}

	@Override
	public Boolean insert(Favorite t) {
		try {
			sessionFactory.getCurrentSession().persist(t);
		} catch (Exception e) {
			return Boolean.FALSE;
		}
		return Boolean.TRUE;
	}

	@Override
	public Boolean delete(Favorite t) {
		try {
			sessionFactory.getCurrentSession().remove(t);
		} catch (Exception e) {
			return Boolean.FALSE;
		}
		return Boolean.TRUE;
	}

	@Override
	public Boolean update(Favorite t) {
		try {
			sessionFactory.getCurrentSession().update(t);
		} catch (Exception e) {
			return Boolean.FALSE;
		}
		return Boolean.TRUE;
	}

}
