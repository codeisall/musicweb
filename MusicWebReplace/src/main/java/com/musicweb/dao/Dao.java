package com.musicweb.dao;

import java.util.List;

public abstract class Dao<Type> {
	public abstract Type getById(Integer id);
	public abstract List<Type> getAll();
	public abstract Boolean insert(Type t);
	public abstract Boolean delete(Type t);
	public abstract Boolean update(Type t);
}
