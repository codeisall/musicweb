package com.musicweb.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.musicweb.model.Genre;

@Repository
public class GenreDao extends Dao<Genre> {
	@Autowired
	SessionFactory sessionFactory;

	@Override
	public Genre getById(Integer id) {
		return sessionFactory.getCurrentSession().get(Genre.class, id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Genre> getAll() {
		return (List<Genre>)sessionFactory.getCurrentSession().createQuery("from Genre").list();
	}

	@Override
	public Boolean insert(Genre t) {
		try {
			sessionFactory.getCurrentSession().persist(t);
		} catch (Exception e) {
			return Boolean.FALSE;
		}
		return Boolean.TRUE;
	}

	@Override
	public Boolean delete(Genre t) {
		try {
			sessionFactory.getCurrentSession().remove(t);
		} catch (Exception e) {
			return Boolean.FALSE;
		}
		return Boolean.TRUE;
	}

	@Override
	public Boolean update(Genre t) {
		try {
			sessionFactory.getCurrentSession().update(t);
		} catch (Exception e) {
			return Boolean.FALSE;
		}
		return Boolean.TRUE;
	}

}
