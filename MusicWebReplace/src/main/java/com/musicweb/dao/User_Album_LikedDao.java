package com.musicweb.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.musicweb.model.User_Album_Liked;

@Repository
public class User_Album_LikedDao extends Dao<User_Album_Liked> {
	@Autowired
	SessionFactory sessionFactory;

	@Override
	public User_Album_Liked getById(Integer id) {
		return sessionFactory.getCurrentSession().get(User_Album_Liked.class, id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<User_Album_Liked> getAll() {
		return (List<User_Album_Liked>) sessionFactory.getCurrentSession().createQuery("from User_Album_Liked").list();
	}

	@Override
	public Boolean insert(User_Album_Liked t) {
		try {
			sessionFactory.getCurrentSession().persist(t);
		} catch (Exception e) {
			return Boolean.FALSE;
		}
		return Boolean.TRUE;
	}

	@Override
	public Boolean delete(User_Album_Liked t) {
		try {
			sessionFactory.getCurrentSession().remove(t);
		} catch (Exception e) {
			return Boolean.FALSE;
		}
		return Boolean.TRUE;
	}

	@Override
	public Boolean update(User_Album_Liked t) {
		try {
			sessionFactory.getCurrentSession().update(t);
		} catch (Exception e) {
			return Boolean.FALSE;
		}
		return Boolean.TRUE;
	}
}
