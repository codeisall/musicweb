package com.musicweb.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.musicweb.model.Song;

@Repository
public class SongDao extends Dao<Song>{
	@Autowired
	SessionFactory sessionFactory;

	@Override
	public Song getById(Integer id) {
		return sessionFactory.getCurrentSession().get(Song.class, id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Song> getAll() {
		return (List<Song>)sessionFactory.getCurrentSession().createQuery("from Song").list();
	}

	@Override
	public Boolean insert(Song t) {
		try {
			sessionFactory.getCurrentSession().persist(t);
		} catch (Exception e) {
			return Boolean.FALSE;
		}
		return Boolean.TRUE;
	}

	@Override
	public Boolean delete(Song t) {
		try {
			sessionFactory.getCurrentSession().remove(t);
		} catch (Exception e) {
			return Boolean.FALSE;
		}
		return Boolean.TRUE;
	}

	@Override
	public Boolean update(Song t) {
		try {
			sessionFactory.getCurrentSession().update(t);
		} catch (Exception e) {
			return Boolean.FALSE;
		}
		return Boolean.TRUE;
	}

}
