package com.musicweb.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.musicweb.model.Song_Artist;

@Repository
public class Song_ArtistDao extends Dao<Song_Artist> {
	@Autowired
	SessionFactory sessionFactory;

	@Override
	public Song_Artist getById(Integer id) {
		return sessionFactory.getCurrentSession().get(Song_Artist.class, id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Song_Artist> getAll() {
		return (List<Song_Artist>)sessionFactory.getCurrentSession().createQuery("from Song_Artist").list();
	}

	@Override
	public Boolean insert(Song_Artist t) {
		try {
			sessionFactory.getCurrentSession().persist(t);
		} catch (Exception e) {
			return Boolean.FALSE;
		}
		return Boolean.TRUE;
	}

	@Override
	public Boolean delete(Song_Artist t) {
		try {
			sessionFactory.getCurrentSession().remove(t);
		} catch (Exception e) {
			return Boolean.FALSE;
		}
		return Boolean.TRUE;
	}

	@Override
	public Boolean update(Song_Artist t) {
		try {
			sessionFactory.getCurrentSession().update(t);
		} catch (Exception e) {
			return Boolean.FALSE;
		}
		return Boolean.TRUE;
	}

}
