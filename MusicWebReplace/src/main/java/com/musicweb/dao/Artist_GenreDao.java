package com.musicweb.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.musicweb.model.Artist_Genre;

@Repository
public class Artist_GenreDao extends Dao<Artist_Genre> {
	@Autowired
	SessionFactory sessionFactory;

	@Override
	public Artist_Genre getById(Integer id) {
		return sessionFactory.getCurrentSession().get(Artist_Genre.class, id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Artist_Genre> getAll() {
		return (List<Artist_Genre>)sessionFactory.getCurrentSession().createQuery("from Artist_Genre").list();
	}

	@Override
	public Boolean insert(Artist_Genre t) {
		try {
			sessionFactory.getCurrentSession().persist(t);
		} catch (Exception e) {
			return Boolean.FALSE;
		}
		return Boolean.TRUE;
	}

	@Override
	public Boolean delete(Artist_Genre t) {
		try {
			sessionFactory.getCurrentSession().remove(t);
		} catch (Exception e) {
			return Boolean.FALSE;
		}
		return Boolean.TRUE;
	}

	@Override
	public Boolean update(Artist_Genre t) {
		try {
			sessionFactory.getCurrentSession().update(t);
		} catch (Exception e) {
			return Boolean.FALSE;
		}
		return Boolean.TRUE;
	}

}
