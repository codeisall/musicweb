package com.musicweb.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.musicweb.model.Album;
import com.musicweb.model.Artist;

@Repository
public class AlbumDao extends Dao<Album> {
	@Autowired
	SessionFactory sessionFactory;

	@Override
	public Album getById(Integer id) {
		Album album = sessionFactory.getCurrentSession().get(Album.class, id);
		album.getArtist();
		album.getAlbum_Songs().size();
		return album;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Album> getAll() {
		return (List<Album>)sessionFactory.getCurrentSession().createQuery("from Album").list();
	}

	@Override
	public Boolean insert(Album t) {
		try {
			Artist artist = sessionFactory.getCurrentSession().get(Artist.class, t.getArtist().getId());
			t.setArtist(artist);
			sessionFactory.getCurrentSession().save(t);
		} catch (Exception e) {
			return Boolean.FALSE;
		}
		return Boolean.TRUE;
	}

	@Override
	public Boolean delete(Album t) {
		try {
			sessionFactory.getCurrentSession().remove(t);
		} catch (Exception e) {
			return Boolean.FALSE;
		}
		return Boolean.TRUE;
	}

	@Override
	public Boolean update(Album t) {
		try {
			sessionFactory.getCurrentSession().update(t);
		} catch (Exception e) {
			return Boolean.FALSE;
		}
		return Boolean.TRUE;
	}
}
