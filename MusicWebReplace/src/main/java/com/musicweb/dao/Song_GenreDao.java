package com.musicweb.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.musicweb.model.Song_Genre;

@Repository
public class Song_GenreDao extends Dao<Song_Genre> {
	@Autowired
	SessionFactory sessionFactory;

	@Override
	public Song_Genre getById(Integer id) {
		return sessionFactory.getCurrentSession().get(Song_Genre.class, id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Song_Genre> getAll() {
		return (List<Song_Genre>)sessionFactory.getCurrentSession().createQuery("from Song_Genre").list();
	}

	@Override
	public Boolean insert(Song_Genre t) {
		try {
			sessionFactory.getCurrentSession().persist(t);
		} catch (Exception e) {
			return Boolean.FALSE;
		}
		return Boolean.TRUE;
	}

	@Override
	public Boolean delete(Song_Genre t) {
		try {
			sessionFactory.getCurrentSession().remove(t);
		} catch (Exception e) {
			return Boolean.FALSE;
		}
		return Boolean.TRUE;
	}

	@Override
	public Boolean update(Song_Genre t) {
		try {
			sessionFactory.getCurrentSession().update(t);
		} catch (Exception e) {
			return Boolean.FALSE;
		}
		return Boolean.TRUE;
	}

}
