package com.musicweb.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.musicweb.model.Artist;

@Repository
public class ArtistDao extends Dao<Artist>{
	@Autowired
	SessionFactory sessionFactory;

	@Override
	public Artist getById(Integer id) {
		return sessionFactory.getCurrentSession().get(Artist.class, id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Artist> getAll() {
		return (List<Artist>)sessionFactory.getCurrentSession().createQuery("from Artist").list();
	}

	@Override
	public Boolean insert(Artist t) {
		try {
			sessionFactory.getCurrentSession().persist(t);
		} catch (Exception e) {
			return Boolean.FALSE;
		}
		return Boolean.TRUE;
	}

	@Override
	public Boolean delete(Artist t) {
		try {
			sessionFactory.getCurrentSession().remove(t);
		} catch (Exception e) {
			return Boolean.FALSE;
		}
		return Boolean.TRUE;
	}

	@Override
	public Boolean update(Artist t) {
		try {
			sessionFactory.getCurrentSession().update(t);
		} catch (Exception e) {
			return Boolean.FALSE;
		}
		return Boolean.TRUE;
	}

}
