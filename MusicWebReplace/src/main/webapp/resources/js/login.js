window.addEventListener("DOMContentLoaded", function() {
	var links = document.querySelectorAll(".link a");
	var signInUpBtn = document.querySelector(".signinup-btn");
	var emptyPattern = /\s/g;
	
	// Sign up | Sign in form
	links[0].addEventListener("click", function() {
		document.querySelector(".name-container").classList.toggle("signup");
		document.querySelector(".confirm-password-container").classList
				.toggle("signup");
		if(document.querySelector(".name-container").classList.contains("signup")) {
			document.querySelector(".signinup-form h1").textContent = "Sign Up";
			document.querySelector(".signinup-btn").textContent = "Sign Up";
			this.textContent = "I already had an account!";
		} else {
			document.querySelector(".signinup-form h1").textContent = "Sign In";
			document.querySelector(".signinup-btn").textContent = "Sign In";
			this.textContent = "Sign up for free!"
		}
	});
	//
	
	//Press sign in | sign up
	signInUpBtn.addEventListener("click", function() {
		var emailInput = document.querySelector(".email-container input");
		var passwordInput = document.querySelector(".password-container input");
		if(document.querySelector(".name-container").classList.contains("signup")) { //Sign up
			var nameInput = document.querySelector(".name-container input");
			var confirmInput = document.querySelector(".confirm-password-container input");
			if(nameInput.value.trim().length <= 0) { // empty
				document.querySelector(".error").textContent = nameInput.placeholder + " cannot be empty";
				nameInput.focus();
			} else {
//				if(emptyPattern.test(nameInput.value)) { //contain whitespace character
//					document.querySelector(".error").textContent = nameInput.placeholder + " cannot contain whitespace character";
//					nameInput.focus();
//				} else {
					console.log(checkForEmailPassoword(emailInput, passwordInput))
					if(checkForEmailPassoword(emailInput, passwordInput)) {
						if(confirmInput.value.trim().length <= 0) { // empty
							document.querySelector(".error").textContent = confirmInput.placeholder + " cannot be empty";
							confirmInput.focus();
						} else {
							if(emptyPattern.test(confirmInput.value)) { //contain whitespace character
								document.querySelector(".error").textContent = confirmInput.placeholder + " cannot contain whitespace character";
								confirmInput.focus();
							} else {
								if(confirmInput.value === passwordInput.value) {
									document.querySelector(".error").textContent = "Success";
									var user = {
										id : null,
										email:emailInput.value,
										name:nameInput.value,
										password:passwordInput.value
									}
									console.log(JSON.stringify(user))
									$.ajax({
										url : "./signup",
										type : "post",
//										dataType: "json",
										data : JSON.stringify(user),
										contentType : "application/json; charset=utf-8",
										success : function(data) {
											alert("Sign up successfully");
											links[0].click();
										},
										async : false,
										error : function(e) {
											console.log(e);
										}
									});
								} else {
									document.querySelector(".error").textContent = "Cofirm pass does not match"
								}
							}
						}
					}
//				}
			}
		} else { //Sign in
			if(checkForEmailPassoword(emailInput, passwordInput)) {
				var user = {
						email:emailInput.value,
						password:passwordInput.value
					}
				$.ajax({
					url : "./signin",
					type : "post",
//					dataType: "json",
					data : JSON.stringify(user),
					contentType : "application/json; charset=utf-8",
					success : function(data) {
						if(data[0]) {
							if(data[1] == 1)
								location.href = "../homeController/";
							else
								location.href = "../adminController/";
						} else {
							document.querySelector(".error").textContent = "Email or password are incorrect"
						}
					},
					async : false,
					error : function(e) {
						console.log(e);
					}
				});
			} else {
				
			}
		}
	});
	//
	
	function checkForEmailPassoword(emailInput, passwordInput) {
		var emailPattern = /[\w\d]+@(hcmiu|gmail|yahoo)(\.com){0,1}(\.vn){0,1}/g;
		var done = false;
//		console.log(emailInput.value.match(emailPattern))
		if(emailInput.value.trim().length <= 0) { // empty
			document.querySelector(".error").textContent = emailInput.placeholder + " cannot be empty";
			emailInput.focus();
		} else {
			if(emptyPattern.test(emailInput.value)) { //contain whitespace character
				document.querySelector(".error").textContent = emailInput.placeholder + " cannot contain whitespace character";
				emailInput.focus();
			} else {
				if(emptyPattern.test(emailInput.value)) { //contain whitespace character
					document.querySelector(".error").textContent = emailInput.placeholder + " cannot contain whitespace character";
					emailInput.focus();
				} else {
					if(emailInput.value.match(emailPattern) == null) { //contain whitespace character
						document.querySelector(".error").textContent = emailInput.placeholder + " is invalid";
						emailInput.focus();
					} else {
						if(emailInput.value.match(emailPattern)[0].length < emailInput.value.length) {
							document.querySelector(".error").textContent = emailInput.placeholder + " is invalid";
							emailInput.focus();
						} else {
							$.ajax({
								url : "./isexist",
								type : "get",
								data : {
									email:emailInput.value
								},
								contentType : "application/json; charset=utf-8",
								success : function(data) {
//									console.log(data)
									if(data) {
										if(document.querySelector(".name-container").classList.contains("signup")) {
											document.querySelector(".error").textContent = emailInput.placeholder + " has already used";
											emailInput.focus();
										} else {
											if(passwordInput.value.trim().length <= 0) { // empty
												document.querySelector(".error").textContent = passwordInput.placeholder + " cannot be empty";
												passwordInput.focus();
											} else {
												if(emptyPattern.test(passwordInput.value)) { //contain whitespace character
													document.querySelector(".error").textContent = passwordInput.placeholder + " cannot contain whitespace character";
													passwordInput.focus();
												} else {
													done = true;
												}
											}
										}
									} else {
										if(!document.querySelector(".name-container").classList.contains("signup")) {
											document.querySelector(".error").textContent = "Account does not exist";
											emailInput.focus();
											return false;
										}
										if(passwordInput.value.trim().length <= 0) { // empty
											document.querySelector(".error").textContent = passwordInput.placeholder + " cannot be empty";
											passwordInput.focus();
										} else {
											if(emptyPattern.test(passwordInput.value)) { //contain whitespace character
												document.querySelector(".error").textContent = passwordInput.placeholder + " cannot contain whitespace character";
												passwordInput.focus();
											} else {
												done = true;
											}
										}
									}
								},
								async : false,
								error : function(e) {
									console.log(e);
								}
							});
						}
					}
				}
			}
		} 
		return done;
	}
});