//window.addEventListener("DOMContentLoaded", function() {
	var blurEle = document.getElementById("blur");
	var blur2Ele = document.getElementById("blur2");
	// links navbar
	document.querySelector("#home").addEventListener("click", function() {
		$(".ajax-container").load("../homeSubController/", function() {
		});
	});
	document.querySelector("#artist").addEventListener("click", function() {
		$(".ajax-container").load("../artistController/artistsub", function() {
		});
	});
	// end links navbar
	// header
		// search
		var searchBtn = document.getElementById("search");
		searchBtn.addEventListener("click", function() {
			document.getElementById("search-box").classList.toggle("appear");
			document.getElementById("search-box").querySelector("input").focus();
			
			blurEle.classList.remove("appear");
			document.getElementById("navbar").classList.toggle("appear");
		});
		// end search
		
		// navbar toggle
		var navbarToggleBtn = document.getElementById("navbar-toggle");
		navbarToggleBtn.addEventListener("click", function() {
			document.getElementById("navbar").classList.toggle("appear");
			
			blurEle.classList.toggle("appear");
			document.getElementById("search-box").classList.remove("appear");
		});
		// end navbar toggle
		
		// close navbar
		var closeNavbarBtn = document.getElementById("closeNavbar-button");
		closeNavbarBtn.addEventListener("click", function() {
			document.getElementById("navbar").classList.toggle("appear");
			
			blurEle.classList.toggle("appear");
		});
		// end close navbar
		
		// user
		var userBtn = document.getElementById("user");
		userBtn.addEventListener("click", function() {
			hideNavbarBoxes(document.getElementById("user-box"));
			document.getElementById("user-box").classList.toggle("appear");
		});
		// end user
		
		// genre
		var genreBtn = document.getElementById("genre");
		genreBtn.addEventListener("click", function() {
			hideNavbarBoxes(document.getElementById("genres-box"));
			document.getElementById("genres-box").classList.toggle("appear");
		});
		// end genre
		
		// sign in button
		var signinBtn = document.getElementById("signin-btn");
		if(signinBtn != null) {
			signinBtn.addEventListener("click", function() {
				location.href = "/MusicWebReplace/loginController/";
			});
		}
		// end sign up button
		
		// sign out button
		var signoutBtn = document.getElementById("signout-btn");
		if(signoutBtn != null) {
			signoutBtn.addEventListener("click", function() {
				location.href = "/MusicWebReplace/loginController/signout";
			});
		}
		// end sign out button
		
		// blur element
		blurEle.addEventListener("click", function() {
			document.getElementById("navbar").classList.toggle("appear");
			
			blurEle.classList.toggle("appear");
			document.getElementById("search-box").classList.remove("appear");
		});
		// end blur element
		
		// blur2 element
		blur2Ele.addEventListener("click", function() {})
		blur2Ele.addEventListener("click", function() {
			if(songsPopUp.classList.contains("active") && artistPopUp.classList.contains("active")) {
				songsPopUp.classList.remove("active");
				artistPopUp.classList.remove("stay");
			} else {
				artistPopUp.classList.remove("active");
				songsPopUp.classList.remove("active");
				this.classList.remove("appear");
			}
		});
		// end blur2 element
		
		// logo
		var logo = document.getElementsByClassName("logo");
		Array.from(logo).forEach(aLogo => {
			aLogo.addEventListener("click", function() {
				location.reload();
			});
		});
		// end logo
		
		// header onscroll
		var isOver = false;
		// Nav
		var header = document.getElementById("header");
		window.onscroll = function() {
			var vertical = window.pageYOffset;
			if (vertical > 60) {
				if (!isOver) {
					document.querySelector(".ajax-container").style.paddingTop = "60px";
					header.classList.add("fixed");
				}
				isOver = true;
			} else {
				if (isOver) {
					document.querySelector(".ajax-container").style.paddingTop = "0px";
					header.classList.remove("fixed");
				}
				isOver = false;
			}
		};
		// end header onscroll
	// end header
	
	// Slide
	var forwardBtn = document.querySelectorAll("div.top i.fas.fa-chevron-right");
	var backwardBtn = document.querySelectorAll("div.top i.fas.fa-chevron-left");
	var frames = null;
	
	// Forward
	forwardBtn.forEach(e => {
		e.addEventListener("click", function() {
			frames = this.parentElement.getElementsByClassName("frame");
			var activeFrame = this.parentElement.getElementsByClassName("active")[0];
			forwardSlide(activeFrame);
		});
	});
	
	function forwardSlide(activeFrame) { // reuse for number
		var nextFrame;
		if(activeFrame.dataset.order == frames.length) {
			nextFrame = frames[0];
		} else {
			nextFrame = activeFrame.nextElementSibling;
		}
		
		activeFrame.classList.add("move-next-active");
		activeFrame.addEventListener("webkitAnimationEnd", function() {
			this.classList.remove("move-next-active");
			this.classList.remove("active");
		});
		
		nextFrame.classList.add("move-next-next");
		nextFrame.addEventListener("webkitAnimationEnd", function() {
			this.classList.remove("move-next-next");
			this.classList.add("active");
		});
	}
	// End Forward
	
	// Backward
	backwardBtn.forEach(e => {
		e.addEventListener("click", function() {
			frames = this.parentElement.getElementsByClassName("frame");
			var activeFrame = this.parentElement.getElementsByClassName("active")[0];
			backwardSlide(activeFrame);
		});
	});
	
	function backwardSlide(activeFrame) { // reuse for number
		if(activeFrame.dataset.order == 1) {
			var preFrame;
			preFrame = frames[frames.length-1];
		} else {
			preFrame = activeFrame.previousElementSibling;
		}
		
		activeFrame.classList.add("move-pre-active");
		activeFrame.addEventListener("webkitAnimationEnd", function() {
			this.classList.remove("move-pre-active");
			this.classList.remove("active");
		});
		
		preFrame.classList.add("move-pre-pre");
		preFrame.addEventListener("webkitAnimationEnd", function() {
			this.classList.remove("move-pre-pre");
			this.classList.add("active");
		});
	}
	// End Backward
	// end Slide
	
	//artist select
	var artistSelect = document.querySelector(".artist-select");
	if(artistSelect != null) {
		artistSelect.addEventListener("change", function() {
			$(".ajax-container").load("../artistController/artistsub?id="+this.value+"&genreid="+this.value);
		});
	}
	//end artist select
	
	//artist popup
	var artistPopUp = document.querySelector(".artist-popup");
	//end artist popup
	
	//songs popup
	var songsPopUp = document.querySelector(".songs-popup");
	//end songs popup
	
	// artist more button
	var artistMoreBtn = document.getElementsByClassName("artist-more-button");
	Array.from(artistMoreBtn).forEach(e => {
		artistMoreBtnClicked(e);
	});
	// end artist more button
	
	//album more button 
	var albumMoreBtn = document.getElementsByClassName("album-more-button");
	Array.from(albumMoreBtn).forEach(e => {
		albumMoreBtnClicked(e);
	});
	//end album more button
	
	// top 100 song
	var topSong = document.querySelectorAll("div.top-song div.songs div.song");
	topSong.forEach(e => {
		e.addEventListener("dblclick", function() {
			dblClickSong(e, topSong);
		});
	});
	// end top 100 song
	
	// function double click to song
	function dblClickSong(ele, songs) {
		songs.forEach(e => {
			e.classList.remove("active");
		});
		ele.classList.toggle("active");
		
		// resource of song
		if(songs.length > 0) {
			const resource = songs[0].parentElement.querySelector("div.song.active").getElementsByTagName("input")[0].value;
			console.log("Resource song : " + resource);
		}
		// end resource of song
	}
	// end function double click to song
	
	// function hide all box on navbar
	function hideNavbarBoxes(ele) {
		const boxes = document.querySelectorAll("div#header li div");
		boxes.forEach(box => {
			if(box != ele)
				box.classList.remove("appear");
		});
	}
	// end function hide all box on navbar
	
	//function when artist more button clicked
	function artistMoreBtnClicked(e) {
		e.addEventListener("click", function() {
			$.ajax({
				url: "../artistController/artist?id="+this.dataset.id,
				type: "get",
				data: "",
				contentType: "application/json; charset=utf-8",
				success: function (data) {
//					console.log(data)
					artistPopUp.querySelector("img").src = ".."+data.img;
					artistPopUp.querySelector(".artitst-info p:nth-child(1)").textContent = data.description;
				
					$.ajax({
						url: "../artistController/genres?id="+data.id,
						type: "get",
						data: "",
						contentType: "application/json; charset=utf-8",
						success: function (data) {
							artistPopUp.querySelector(".artitst-info p:nth-child(2)").innerHTML = "";
							data.forEach((e, index) => {
								var span = document.createElement("span");
								
								if(index == data.length-1) {
									span.textContent = e.name;
								} else {
									span.textContent = e.name + ", ";
								}
								artistPopUp.querySelector(".artitst-info p:nth-child(2)").appendChild(span);
							});
						},
						async : false,
						error : function(e) {
							console.log(e);
						}
					});
					
					$.ajax({
						url: "../songArtistController/songs?id="+data.id,
						type: "get",
						data: "",
						contentType: "application/json; charset=utf-8",
						success: function (data) {
							artistPopUp.querySelector(".songs").innerHTML = "";
							data.forEach(e => {
								var divSong = document.createElement("div");
								divSong.classList.add("song");
								var img = document.createElement("img");
								img.alt = "artist";	
								img.src = artistPopUp.querySelector("img").src;
								divSong.appendChild(img);
								var input = document.createElement("input");
								input.hidden = true;
								input.value = e.resource;
								divSong.appendChild(input);
								var divSongInfo = document.createElement("div");
								var p = document.createElement("p");
								p.textContent = e.name;
								divSongInfo.appendChild(p);
								p = document.createElement("p");
								p.textContent = artistPopUp.querySelector(".artitst-info p:nth-child(1)").textContent;
								divSongInfo.appendChild(p);
								divSong.appendChild(divSongInfo);
								artistPopUp.querySelector(".songs").appendChild(divSong);
							});
						},
						async : false,
						error : function(e) {
							console.log(e);
						}
					});
					
					$.ajax({
						url: "../albumController/albums?id="+data.id,
						type: "get",
						data: "",
						contentType: "application/json; charset=utf-8",
						success: function (data) {
//							console.log(data);
							const frameContainer = document.querySelector(".frame-container");
							frameContainer.innerHTML = "";
							var frame = Math.floor(data.length / 5);
							if(data.length % 5 > 0)
								frame++;
							for(let i = 0; i < frame; i++) {
								var frameDiv = document.createElement("div");
								frameDiv.classList.add("frame");
								frameDiv.dataset.order = i+1;
								if(i == 0) {
									frameDiv.classList.add("active");
								}
								var start = 0;
								var end = start+5;
								for(let j = start; j < end; j++) {
									if(j < data.length) {
										var frameElementDiv = document.createElement("div");
										frameElementDiv.classList.add("frame-element");
										var img = document.createElement("img");
										img.alt = "artist";
										img.src = ".."+data[j].artist.img;
										frameElementDiv.appendChild(img);
										var p = document.createElement("p");
										p.textContent = data[j].name;
										frameElementDiv.appendChild(p);
										var genreDiv = document.createElement("div");
										genreDiv.classList.add("genres");
										var a = document.createElement("a");
										a.href = "#";
										a.textContent = data[j].artist.name;
										genreDiv.appendChild(a);
										frameElementDiv.appendChild(genreDiv);
										var moreDiv = document.createElement("div");
										moreDiv.classList.add("more");
										var span = document.createElement("span");
										span.classList.add("far");
										span.classList.add("fa-heart");
										moreDiv.appendChild(span);
										span = document.createElement("span");
										span.textContent = data[j].liked;
										moreDiv.appendChild(span);
										span = document.createElement("span");
										span.textContent = "More";
										span.classList.add("album-more-button");
										span.dataset.id = data[j].id;
										moreDiv.appendChild(span);
										frameElementDiv.appendChild(moreDiv);
										frameDiv.appendChild(frameElementDiv);
									}
								}
								start += 5;
								frameContainer.appendChild(frameDiv);
							}
						},
						async : false,
						error : function(e) {
							console.log(e);
						}
					});
				},
				async : false,
				error : function(e) {
					console.log(e);
				}
			});
			
			const songOfArtist = artistPopUp.querySelectorAll(".song");
			songOfArtist.forEach(e => {
				e.addEventListener("dblclick", function() {
					dblClickSong(e, songOfArtist);
				});
			});
			
			const albumOfArtist = artistPopUp.querySelectorAll(".album-more-button");
			Array.from(albumOfArtist).forEach(e => {
				albumMoreBtnClicked(e);
			});
			
			artistPopUp.classList.toggle("active");
			blur2Ele.classList.toggle("appear");
		});
	}
	//end function when artist more button clicked
	
	//function when album more button clicked
	function albumMoreBtnClicked(e) {
		e.addEventListener("click", function() {
//			console.log(e);
			const genreTitle = document.querySelector(".genre-title");
			const albumTitle = document.querySelector(".album-title");
			const songsDiv = songsPopUp.querySelector(".songs");
			
			let url = "";
			if(e.classList.contains("album-more-button")) {
				genreTitle.style.display = "none";
				albumTitle.style.display = "block";
				url = "../albumSongController/songs?id="+this.dataset.id;
			} else {
				genreTitle.style.display = "block";
				albumTitle.style.display = "none";
				url = "../songGenreController/songs?id="+this.dataset.id;
			}
			
			songsDiv.innerHTML = "";
			
			$.ajax({
				url: url,
				type: "get",
				data: "",
				contentType: "application/json; charset=utf-8",
				success: function (data) {
					data.forEach(e => {
						var divSong = document.createElement("div");
						divSong.classList.add("song");
						var img = document.createElement("img");
						img.alt = "artist";	
						img.src = ".."+e.id.album.artist.img;
						divSong.appendChild(img);
						var input = document.createElement("input");
						input.hidden = true;
						input.value = e.id.song.resource;
						divSong.appendChild(input);
						var divSongInfo = document.createElement("div");
						var p = document.createElement("p");
						p.textContent = e.id.song.name;
						divSongInfo.appendChild(p);
						p = document.createElement("p");
						p.textContent = e.id.album.artist.name;
						divSongInfo.appendChild(p);
						divSong.appendChild(divSongInfo);
						songsDiv.appendChild(divSong);
					});
				},
				async : false,
				error : function(e) {
					console.log(e);
				}
			});
			
			const songOfArtist = songsPopUp.querySelectorAll(".song");
			songOfArtist.forEach(e => {
				e.addEventListener("dblclick", function() {
					dblClickSong(e, songOfArtist);
				});
			});
			songsPopUp.classList.toggle("active");
			if(artistPopUp.classList.contains("active")) {
				artistPopUp.classList.add("stay");
			}
			blur2Ele.classList.add("appear");
		});
	}
	//end function when album more button clicked
// });
