window.addEventListener("DOMContentLoaded", function() {
	$(".ajax-container").load("../genreController/", function() {
		general();
	});
	
	var blur = document.getElementById("blur");
	var blur2 = document.getElementById("blur2");
	const logoutBtn = document.getElementById("logout");
	
	//logout
	logoutBtn.addEventListener("click", function() {
		location.href = "/MusicWebReplace/loginController/signout";
	});
	//end logout
	
	// Blur
	blur.addEventListener("click", function() {
		addForm.classList.remove("add-form-appear");
		blur.classList.remove("appear");
	});
	// End Blur
	
	// Blur2
	blur2.addEventListener("click", function() {
		document.querySelector(".add-song-box").classList.remove("add-form-appear");
		this.classList.remove("appear2");
	});
	// End Blur2
	
	// Slide
	var forwardBtn = document.querySelector(".slide-table i.fas.fa-chevron-right");
	var backwardBtn = document.querySelector(".slide-table i.fas.fa-chevron-left");
	var numbers = document.querySelectorAll(".slide-table .number span");
	var tables = document.querySelectorAll(".slide-table table");
	
	function slideRefresh() {
		forwardBtn = document.querySelector(".slide-table i.fas.fa-chevron-right");
		backwardBtn = document.querySelector(".slide-table i.fas.fa-chevron-left");
		numbers = document.querySelectorAll(".slide-table .number span");
		tables = document.querySelectorAll(".slide-table table");
		
		forwardBtn.addEventListener("click", function() {
			var activeTable = document.querySelector(".slide-table table.active");
			forwardSlide(activeTable, null);
		});
		
		backwardBtn.addEventListener("click", function() {
			var activeTable = document.querySelector(".slide-table table.active");
			backwardSlide(activeTable, null);
		});
		
		for(var i = 0; i < numbers.length; i++) {
			numbers[i].addEventListener("click", function() {
				
				var activeNumber = document.querySelector(".slide-table .number span.active");
				var activeTable = tables[activeNumber.dataset.order-1];
				var nextTable;
				var preTable;
				let order_number = parseInt(this.dataset.order);
				let order_active_table = parseInt(activeNumber.dataset.order);
				console.log(order_number);
				console.log(order_active_table);
				
				if(order_number < order_active_table) {
					console.log("smaller");
					preTable = tables[this.dataset.order-1];
					backwardSlide(activeTable, preTable);
				} else {
					console.log("bigger");
					nextTable = tables[this.dataset.order-1];
					forwardSlide(activeTable, nextTable)
				}
				
			});
		}
	}
	
	// Forward
	function forwardSlide(activeTable, nextTableOfNumberClicked) { // reuse for number
		var nextTable;
		if(activeTable.dataset.order == tables.length) {
			nextTable = tables[0];
		} else {
			nextTable = activeTable.nextElementSibling;
		}
		
		if(nextTableOfNumberClicked != null) {
			nextTable = nextTableOfNumberClicked;
		}
		
		activeTable.classList.add("move-next-active");
		activeTable.addEventListener("webkitAnimationEnd", function() {
			this.classList.remove("move-next-active");
			this.classList.remove("active");
			numbers[parseInt(this.dataset.order-1)].classList.remove("active");
		});
		
		nextTable.classList.add("move-next-next");
		nextTable.addEventListener("webkitAnimationEnd", function() {
			this.classList.remove("move-next-next");
			this.classList.add("active");
			numbers[parseInt(this.dataset.order-1)].classList.add("active");
		});
	}
	// End Forward
	
	// Backward
	function backwardSlide(activeTable, preTableOfNumberClicked) { // reuse for number
		if(activeTable.dataset.order == 1) {
			var preTable;
			preTable = tables[tables.length-1];
		} else {
			preTable = activeTable.previousElementSibling;
		}
		
		if(preTableOfNumberClicked != null) {
			preTable = preTableOfNumberClicked;
		}
		
		activeTable.classList.add("move-pre-active");
		activeTable.addEventListener("webkitAnimationEnd", function() {
			this.classList.remove("move-pre-active");
			this.classList.remove("active");
			numbers[this.dataset.order-1].classList.remove("active");
		});
		
		preTable.classList.add("move-pre-pre");
		preTable.addEventListener("webkitAnimationEnd", function() {
			this.classList.remove("move-pre-pre");
			this.classList.add("active");
			numbers[this.dataset.order-1].classList.add("active");
		});
	}
	// End Backward
	// End Slide
	
	// Add form
	var infoBtns = document.querySelectorAll(".ajax-container button.info");
	var openAddFormBtn = document.querySelector(".ajax-container button.add");
	var addForm = document.querySelector(".ajax-container div.add-form");
	var closeFormBtns = document.querySelectorAll("h2 i");
	var addBtn = document.querySelector(".ajax-container div.add-form .add-btn");
	
	function addFormFunc() {
		infoBtns = document.querySelectorAll(".ajax-container button.info");
		openAddFormBtn = document.querySelector(".ajax-container button.add");
		addForm = document.querySelector(".ajax-container div.add-form");
		closeFormBtns = document.querySelectorAll("h2 i");
		addBtn = document.querySelector(".ajax-container div.add-form .add-btn");
		
		infoBtns.forEach(infoBtn => { //info button in album page
			infoBtn.addEventListener("click", function() {
				document.querySelector(`button.delete[value='${this.value}']`).click();
				document.querySelector(".ajax-container div.add-form h2 span").textContent = "Detail of";
				addBtn.style.display = "none";
				var addSongBtn = document.querySelector(".input-container .add-song");
				addSongBtn.textContent = "List of Songs";
			});
		});
		
		openAddFormBtn.addEventListener("click", function() {
			var inputs = document.querySelectorAll(".ajax-container div.add-form input");
			Array.from(inputs).forEach(e => {
				if(e.type == "file") {
					e.disabled = false;
				} else {
					e.readOnly = false;
				}
			});
			resetInputTags(inputs);
			keyUp(inputs);
			var resetBtns = document.querySelectorAll(".add-form .input-container div i");
			Array.from(resetBtns).forEach(iTag => iTag.style.display = "none");
			
			var tableName = document.querySelector(".navbar ul li.active").dataset.tablename;
			if(tableName == "genre") {
			} else if (tableName == "artist") {
				document.querySelector(".description-container textarea").readOnly = false;
			} else if (tableName == "album") {
				eventAddSongToBox(1);
				
				var addSongBtn = document.querySelector(".input-container .add-song");
				addSongBtn.textContent = "Add song";
				addSongBtn.addEventListener("click", function() {
					var addSongBox = document.querySelector(".add-song-box");
					addSongBox.classList.add("add-form-appear");
					blur2.classList.add("appear2");
				})
			} else if (tableName == "song") {
				document.querySelector(".lyric-container textarea").readOnly = false;
				
				//function thêm sự kiện khi chọn artist sẽ hiển thị genre
				getGenresOfArtist();
				//end function thêm sự kiện khi chọn artist sẽ hiển thị genre
				
				//get name of song
				document.querySelector(".name-container input").readOnly = false;
				document.querySelector(".song-container input").addEventListener("change", function() {
					document.querySelector(".name-container input").value = this.files[0].name.split(/\.mp[34]/)[0];
				});
				//end get name of song
				
			} else if (tableName == "user") {
				document.getElementsByTagName("select")[2].disabled = false;
			}
			
			resetGenreCheckBox();
			disableCheckboxes(false)
			
			addBtn.style.display = "inline-block";
			addForm.classList.add("add-form-appear");
			document.querySelector(".ajax-container div.add-form h2 span").textContent = "Add";
			document.querySelector(".id-container").style.display = "none";
			addBtn.textContent = "Add";
			addBtn.dataset.type = 1;
			blur.classList.add("appear");
		});
		
		
		// Close add form
		closeFormBtns.forEach(closeFormBtn => {
			closeFormBtn.addEventListener("click", function() {
				if(this.parentElement.parentElement.classList[0] == "add-song-box") {
					document.querySelector(".add-song-box").classList.remove("add-form-appear");
					blur2.classList.remove("appear2");
				} else {
					addForm.classList.remove("add-form-appear");
					blur.classList.remove("appear");
				}
			});
		});
	}
	// End Close add form
	// End Add form
	
	// Modify
	var openModifyFormBtns = document.querySelectorAll(".ajax-container button.modify");
	function modifyForm() {
		openModifyFormBtns = document.querySelectorAll(".ajax-container button.modify");
		for(var i = 0; i < openModifyFormBtns.length; i++) {
			modify_form_general(openModifyFormBtns[i]);
		}
	}
	
	function modify_form_general(e) {
		e.addEventListener("click", function() {
			var inputs = document.querySelectorAll(".ajax-container div.add-form input");
			Array.from(inputs).forEach(e => {
				if(e.type == "file") {
					e.disabled = false;
				} else {
					e.readOnly = false;
				}
			});
			resetInputTags(inputs);
			keyUp(inputs);
			var resetBtns = document.querySelectorAll(".add-form .input-container div i");
			Array.from(resetBtns).forEach(iTag => iTag.style.display = "block");
			previousValue();
			
			var tableName = document.querySelector(".navbar ul li.active").dataset.tablename;
			var url = ""
			if(tableName == "genre") {
				url = "../genreController/genre?id=";
			} else if (tableName == "artist") {
				document.querySelector(".description-container textarea").readOnly = false;
				url = "../artistController/artist?id=";
			} else if (tableName == "album") {
				url = "../albumController/album?id=";	
				eventAddSongToBox(1);
				
				var addSongBtn = document.querySelector(".input-container .add-song");
				addSongBtn.textContent = "Add song";
				addSongBtn.addEventListener("click", function() {
					var addSongBox = document.querySelector(".add-song-box");
					addSongBox.classList.add("add-form-appear");
					blur2.classList.add("appear2");
				})
			} else if (tableName == "song") {
				document.querySelector(".lyric-container textarea").readOnly = false;
				url = "../songController/song?id=";	
			} else if (tableName == "user") {
				url = "../userController/user?id=";
				document.getElementsByTagName("select")[2].disabled = false;
			}
			
			disableCheckboxes(false)
			
			getDataForDeleteAndModifyBox(tableName, url, this);
			document.querySelector(".name-container input").readOnly = false;
			
			addBtn.style.display = "inline-block";
			addForm.classList.add("add-form-appear");
			document.querySelector(".ajax-container div.add-form h2 span").textContent = "Modify";
			document.querySelector(".id-container").style.display = "flex";
			document.querySelector(".id-container input").readOnly = true;
			addBtn.textContent = "Modify";
			addBtn.dataset.type = 2;
			blur.classList.add("appear");
		});
	}
	// End modify
	
	// Delete
	var openDeleteFormBtns = document.querySelectorAll(".ajax-container button.delete");
	function deleteForm() {
		openDeleteFormBtns = document.querySelectorAll(".ajax-container button.delete");
		for(var i = 0; i < openModifyFormBtns.length; i++) {
			delete_form_general(openDeleteFormBtns[i]);
		}
	}
	
	function delete_form_general(e) {
		e.addEventListener("click", function() {
			addForm.classList.add("add-form-appear");
			document.querySelector(".ajax-container div.add-form h2 span").textContent = "Delete";
			document.querySelector(".id-container").style.display = "flex";
			addBtn.textContent = "Delete";
			addBtn.dataset.type = 3;
			blur.classList.add("appear");
			
			var tableName = document.querySelector(".navbar ul li.active").dataset.tablename;
			var url = ""
			if(tableName == "genre") {
				url = "../genreController/genre?id=";
			} else if (tableName == "artist") {
				document.querySelector(".description-container textarea").readOnly = true;
				disableCheckboxes(true)
				url = "../artistController/artist?id=";
			} else if (tableName == "album") {
				url = "../albumController/album?id=";
				eventAddSongToBox(2);
				
				var addSongBtn = document.querySelector(".input-container .add-song");
				addSongBtn.textContent = "List of Songs";
				addSongBtn.addEventListener("click", function() {
					var addSongBox = document.querySelector(".add-song-box");
					addSongBox.classList.add("add-form-appear");
					blur2.classList.add("appear2");
				});
			} else if (tableName == "song") {
				document.querySelector(".lyric-container textarea").readOnly = true;
				disableCheckboxes(true)
				url = "../songController/song?id=";
			} else if (tableName == "user") {
				url = "../userController/user?id=";
				document.getElementsByTagName("select")[2].disabled = true;
			}
			
			getDataForDeleteAndModifyBox(tableName ,url ,this);
			disableCheckboxes(true)
			
			addBtn.style.display = "inline-block";
			var inputs = document.querySelectorAll(".ajax-container div.add-form input");
			Array.from(inputs).forEach(e => {
				if(e.type == "file") {
					e.disabled = true;
				} else {
					e.readOnly = true;
				}
			});
			var resetBtns = document.querySelectorAll(".add-form .input-container div i");
			Array.from(resetBtns).forEach(iTag => iTag.style.display = "none");
		});
	}
	// End Delete
	
	//button in add-modify-delete form
	function bottonButtonFormClick() {
		addBtn.addEventListener("click", function() {
			var tableName = document.querySelector(".navbar ul li.active").dataset.tablename;
			var error = document.querySelector(".error");
			if(this.dataset.type == 1) { // add
				console.log("Add");
				
				var inputName = document.querySelector(".name-container input");
				if(inputName.value.trim().length <= 0) {
					error.textContent = "Name cannot be empty";
					return false;
				}
				
				if(tableName == "genre") { //add genre
					addUpdateGenre(1, null, inputName.value);
				} else if (tableName == "artist") { //add artist
					addUpdateArtist(1, null, inputName.value);
				} else if (tableName == "album") {//add album
					addUpdateAlbum(1, null, inputName.value);
				} else if (tableName == "song") {//add song
					addUpdateSong(1, null, inputName.value, error);
				} else if (tableName == "user") { //add user
					addUpdateUser(1, inputName.value, error);
				}
			} else if (this.dataset.type == 2) { // modify
				console.log("Modify");
				
				var inputName = document.querySelector(".name-container input");
				if(inputName.value.trim().length <= 0) {
					error.textContent = "Name cannot be empty";
					return false;
				}
				
				var inputId = document.querySelector(".id-container input");
				
				if(tableName == "genre") { //modify genre
					addUpdateGenre(2, inputId.value, inputName.value);
				} else if (tableName == "artist") { //modify artist
					addUpdateArtist(2, inputId.value, inputName.value);
				} else if (tableName == "album") { //modify album
					var inputId = document.querySelector(".id-container input");
					addUpdateAlbum(2, inputId.value, inputName.value);
				} else if (tableName == "song") { //modify song
					addUpdateSong(2, inputId.value, inputName.value, error);
				} else if (tableName == "user") { //modify user
					addUpdateUser(2, inputName.value, error);
				}
			} else if (this.dataset.type == 3) { // delete
				console.log("Delete");
				var inputId = document.querySelector(".id-container input");
				var object;
				var loadPage;
				var url;
				var alertContent;
				if(tableName == "genre") { //delete genre
					var genre = {
							id: inputId.value,
							name : ""
					}
					object = genre;
					url = "../genreController/delete";
					alertContent = "Delete Genre Successfully";
					loadPage = "../genreController/";
				} else if (tableName == "artist") { //delete artist
					var genre = {
							id: inputId.value,
							name : ""
					}
					object = genre;
					url = "../artistController/delete";
					alertContent = "Delete Artist Successfully";
					loadPage = "../artistController/";
				} else if (tableName == "album") { //delete album
					var album = {
							id:inputId.value,
							name : ""
					}
					object = album;
					url = "../albumController/delete";
					alertContent = "Delete Album Successfully";
					loadPage = "../albumController/";
					
				} else if (tableName == "song") { //delete song
					var song = {
							id:inputId.value,
							name:"",
							resource:"",
							lyric:"",
							liked:0,
							listen:0
					}
					object = song;
					url = "../songController/delete";
					alertContent = "Delete Song Successfully";
					loadPage = "../songController/";
				} else if (tableName == "user") { //delete user
					const user = {
							id:inputId.value,
							name : ""
					}
					object = user;
					url = "../userController/delete";
					alertContent = "Delete User Successfully";
					loadPage = "../userController/";
				}
				
				$.ajax({
					url : url,
					type : "delete",
					data: JSON.stringify(object),
					contentType : "application/json; charset=utf-8",
					success : function(data) {
						alert(alertContent);
						blur.click();
						$(".ajax-container").load(loadPage, function() {
							general();
						});
					},
					async : false,
					error : function(e) {
						console.log(e);
					}
				});
			}
		});
	}
	// End button in form
	
	//function reset input tags
	function resetInputTags(inputs) {
		var textArea = document.querySelector(".ajax-container div.add-form textArea");
		if(textArea != null) { // Description of artist
			textArea.value = "";
		}
		Array.from(inputs).forEach(input => input.value = "");
	}
	//End function reset input tags
	
	//function keyup for input tags
	function keyUp(inputs) {
		Array.from(inputs).forEach(input =>
			input.addEventListener("keyup", function() {
				document.querySelector(".error").textContent = "";
			})
		);
	}
	//End function keyup for input tags
	
	//function add event for artist radio buttons in album page
	//add song of that artist to add-song-box
	function eventAddSongToBox(type) {//1=update,add ; 2 = delete
		//add, update thì có thể chọn bài hát thêm vào album
		//delete thì ko cho chọn gì hêt
		var artistRadios = document.querySelectorAll(".artist-container div input");
		artistRadios.forEach(artistRadio => {
			artistRadio.addEventListener("click", function() {
				var songsContainer = document.querySelector(".songs");
				songsContainer.innerHTML = "";
				addSongToBox(this.dataset.id);
				if(type == 1)
					selectSongOnBox();
				playSongManageSong();
			});
		});
	}
	//end function add event for artist radio buttons in album page
	
	//function add song to add-song-box (song page)
	function addSongToBox(artistId) {
		var songsContainer = document.querySelector(".songs");
		songsContainer.innerHTML = "";
		
		$.ajax({//get songs of artist by id
			url: "../songArtistController/songs?id="+ artistId,
			type: "get",
			data: "",
			contentType: "application/json;charset=utf-8",
			success: function(data) {
				console.log(data);
				data[0].forEach(song => {
					//create div song 
					var divSong = document.createElement("div");
					divSong.classList.add("song");
					divSong.dataset.selected = 0;
					divSong.dataset.id = song.id;
					divSong.id = "song-div"+song.id;
					
					var pNameSong = document.createElement("p");
					pNameSong.textContent = song.name;
					divSong.appendChild(pNameSong);
					
					var audio = new Audio(".."+song.resource);
					audio.controls = true;
					audio.id = "audio"+song.id;
					audio.hidden = true;
					divSong.appendChild(audio);
					
					var i = document.createElement("i");
					i.classList.add("fas");
					i.classList.add("fa-play");
					i.dataset.status = 0;
					i.dataset.audioid = "audio"+song.id;
					i.style.cursor = "pointer";
					divSong.appendChild(i);
					
					songsContainer.appendChild(divSong);
					//
				});
			},
			async:false,
			error: function(error) {
				console.log(error);
			}
		});
		var t = "<div class=\"song\">\r\n" + 
		"			<p>Hay Trao Cho Anh</p>\r\n" + 
		"			<audio controls hidden=\"\" id=\"audio\">\r\n" + 
		"				<source src=\"../resources/audio/XXXTENTACION - SAD!.mp3\" type=\"audio/mp3\">\r\n" + 
		"			</audio>\r\n" + 
		"			<i class=\"fas fa-play\" data-status=\"0\"\r\n" + 
		"				data-audioid=\"audio${songs[i-1].id}\" style=\"cursor: pointer;\"></i>\r\n" + 
		"		</div>"	;
		
		
	}
	//end function add song to add-song-box (song page)
	
	//function add event that double click on div song to select it
	function selectSongOnBox() {
		var songDivs = document.querySelectorAll(".song");
		songDivs.forEach(songDiv => {
			songDiv.addEventListener("dblclick", function() {
//				e.preventDefault();
				this.classList.toggle("selected");
				if(this.dataset.selected == 0) {//chưa chọn
					this.dataset.selected = 1;
				} else if(this.dataset.selected == 1) {//đã chọn
					this.dataset.selected = 0;
				}
			});
		});
	}
	//end function add event that double click on div song to select it
	
	//function to play audio in manage song
	function playSongManageSong() {
		var tableName = document.querySelector(".navbar ul li.active").dataset.tablename;
		if(tableName == "song" || tableName == "album") {// nếu đang ở trang manage song
			var playBtns = document.querySelectorAll(".fas.fa-play");
			Array.from(playBtns).forEach(playBtn => {
				playBtn.addEventListener("click", function() {
					
					//pause tất cả trước
					Array.from(playBtns).forEach(playBtnTmp => {
						//console.log(playBtnTmp != playBtn)
						if(playBtnTmp != playBtn) {
							document.querySelector("#"+playBtnTmp.dataset.audioid).pause();
							playBtnTmp.dataset.status = 0;
							playBtnTmp.classList.add("fa-play");
							playBtnTmp.classList.remove("fa-pause");
						}
					})
					//end pause tất cả trước
					
					if(this.dataset.status == 0) { //đang ở trang thái pause 
						document.querySelector("#"+this.dataset.audioid).play();
						this.dataset.status = 1;
						this.classList.remove("fa-play");
						this.classList.add("fa-pause");
					} else if (this.dataset.status == 1) { //đang ở trang thái play
						document.querySelector("#"+this.dataset.audioid).pause();
						this.dataset.status = 0;
						this.classList.add("fa-play");
						this.classList.remove("fa-pause");
					}
				})
			});
		}
	}
	playSongManageSong();
	//end function to play audio in manage song
	
	//2 function cho việc hiển thị genre khi chọn artist 
	function addGenresOfArtist() {
//		alert(this.dataset.id)
		var artistsChecked = document.querySelectorAll(".artist-container input:checked");
		var artistsIdChecked = [];
		Array.from(artistsChecked).forEach(artist => {
			artistsIdChecked.push(parseInt(artist.dataset.id));
		});
//		console.log(artistsIdChecked)
		
		var genreContainer = document.querySelector(".genre-container div");
		genreContainer.innerHTML = "";
		if(artistsIdChecked.length > 0) { //artist checked
			$.ajax({
				url: "../artistController/genres",
				type: "post",
				data: JSON.stringify(artistsIdChecked),
				contentType: "application/json; charset=utf-8",
				success: function (data) {
//					console.log(data)
					var genreCheckbox = document.createElement("input");
					data.forEach(genre => {
						genreCheckbox = document.createElement("input");
						genreCheckbox.type = "checkbox";
						genreCheckbox.dataset.id = genre.id;
						genreCheckbox.dataset.name = genre.name;
						genreCheckbox.id = "genre-checkbox"+genre.id;
						genreCheckbox.dataset.previousvalue = "";
						genreContainer.appendChild(genreCheckbox);
					});
				},
				async : false,
				error : function(e) {
					console.log(e);
				}
			});
		}
	}
	
	function getGenresOfArtist() {
		var artistCheckboxes = document.querySelectorAll(".artist-container input");
		Array.from(artistCheckboxes).forEach(artist => {
			artist.addEventListener("click", function() {
				addGenresOfArtist();
			});
		});
	}
	// end 2 function cho việc hiển thị genre khi chọn artist
	
	//function get data for open delete record box
	function getDataForDeleteAndModifyBox(tableName, url, deleteBtn) {
		$.ajax({
			url: url+deleteBtn.value,
			type: "get",
			data: "",
			contentType: "application/json; charset=utf-8",
			success: function (data) {
//				console.log(data);
				document.querySelector(".id-container input").value = data.id;
				document.querySelector(".name-container input").value = data.name;
				document.querySelector(".name-container input").dataset.previousvalue = data.name;
				
				if(tableName == "user") {
					document.querySelector(".img-container input").dataset.previousvalue = data.img;
					document.querySelector(".email-container input").dataset.previousvalue = data.email;
					document.querySelector(".email-container input").value = data.email;
					document.querySelector(".password-container input").dataset.previousvalue = data.password;
					document.querySelector(".password-container input").value = data.password;
					document.querySelector(".role-container select").dataset.previousvalue = data.role;
					document.querySelector(".role-container select").value = data.role;
				}
				
				if(tableName == "artist") {
					document.querySelector(".img-container input").dataset.previousvalue = data.img;
					document.querySelector(".description-container textarea").value = data.description;
					document.querySelector(".description-container textarea").dataset.previousvalue = data.description;
					
					$.ajax({
						url: "../artistController/genres?id="+data.id,
						type: "get",
						data: "",
						contentType: "application/json; charset=utf-8",
						success: function (data) {
							//console.log(data)
							resetGenreCheckBox();
							data.forEach(g=> {
								document.querySelector("#genre-checkbox"+g.id).dataset.previousvalue = g.id;
								document.querySelector("#genre-checkbox"+g.id).checked = true;
							})
						},
						async : false,
						error : function(e) {
							console.log(e);
						}
						
					});
				}
				
				if(tableName == "song") {
					document.querySelector(".song-container input").dataset.previousvalue = data.resource;
					document.querySelector(".lyric-container textarea").value = data.lyric;
					document.querySelector(".lyric-container textarea").dataset.previousvalue = data.lyric;
					
					//get name of song
					document.querySelector(".name-container input").readOnly = true;
					document.querySelector(".song-container input").addEventListener("change", function() {
						document.querySelector(".name-container input").value = this.files[0].name.split(/\.mp[34]/)[0];
					});
					//end get name of song
					
					$.ajax({
						url: "../songController/artists?id="+data.id,
						type: "get",
						data: "",
						contentType: "application/json; charset=utf-8",
						success: function (data) {
							console.log(data)
							resetGenreCheckBox();
							data.forEach(g=> {
								document.querySelector("#artist-checkbox"+g.id).dataset.previousvalue = g.id;
								document.querySelector("#artist-checkbox"+g.id).checked = true;
							})
						},
						async : false,
						error : function(e) {
							console.log(e);
						}
						
					});
					
					addGenresOfArtist();//hiển thị genren của artist của bài hat
					getGenresOfArtist();//thêm sự kiện tự union genre khi có thay đổi tên artist
					
					$.ajax({ //lấy genre của bài hát và checked nó trong những genre của artist
						url: "../songController/genres?id="+data.id,
						type: "get",
						data: "",
						contentType: "application/json; charset=utf-8",
						success: function (data) {
							//console.log(data)
//							resetGenreCheckBox();
							data.forEach(g=> {
								document.querySelector("#genre-checkbox"+g.id).dataset.previousvalue = g.id;
								document.querySelector("#genre-checkbox"+g.id).checked = true;
							})
						},
						async : false,
						error : function(e) {
							console.log(e);
						}
						
					});
				}
				
				if(tableName == "album") {
					//check artist
					document.getElementById("artist-checkbox"+data.artist.id).click();
					document.getElementById("artist-checkbox"+data.artist.id).dataset.previousvalue = data.artist.id;
					//end check artist
					
					var songs = [];
					//get song of this album
					$.ajax({
						url: "../albumController/songs",
						type: "get",
						data: {
							album_id : data.id
						},
						contentType: "application/json; charset=utf-8",
						success: function (data) {
							//make songs of this album have red background
							songs = data;
							//end make songs of this album have red background
						},
						async : false,
						error : function(e) {
							console.log(e);
						}
					});
					
					songs.forEach(d => {
						console.log(d)
						document.getElementById("song-div"+d.id).dataset.selected = 1;
						document.getElementById("song-div"+d.id).classList.add("selected");
					});
					//end get song of this album
				}
			},
			async : false,
			error : function(e) {
				console.log(e);
			}
			
		});
	}
	// end function get data for open delete record box
	
	//function add-update genre
	function addUpdateGenre(type, genre_id, genre_name) {
		const genre = {
				id:(type==1?null:genre_id),
				name:genre_name
		}
		$.ajax({
			url : "../genreController/"+(type==1?"add":"update"),
			type : "post",
			data : JSON.stringify(genre),
			contentType : "application/json; charset=utf-8",
			success : function(data) {
				console.log("genre");
				if(type == 1) {
					alert("Add Genre Successfully")
				} else {
					alert("Modify Genre Successfully")
				}
				blur.click();
				$(".ajax-container").load("../genreController/", function() {
					general();
				});
			},
			async : false,
			error : function(e) {
				console.log(e);
			}
		});
	}
	//end function add-update genre
	
	//function add-update artist
	function addUpdateArtist(type, artist_id, artist_name) {
		const artist = {
				id:(type==1?null:artist_id),
				name:artist_name,
				img:"/resources/img/",
				description:document.querySelector(".ajax-container div.add-form textArea").value,
				liked:0
		}

		//add artist first
		var form = new FormData();
		document.querySelectorAll(".img-container input").forEach(function(file) {
			form.append("file", file.files[0]);//date of form
			form.append("type", 1);
			if(file.files[0] != undefined) {
				//console.log(file.files[0])
				artist.img += file.files[0].name;
			}
		});
		
		if(artist.img.split("/img/")[artist.img.split("/img/").length-1] != "") {
			//nếu có ảnh thì split ra phần tử cuối là tên ảnh
			$.ajax({
				url : "../fileUploadController/upload",
				type:"post",
				data : form,
				contentType : false, //đã mã hóa ko cần type
				processData : false, //ép ko cho form xử lý như bth
				enctype : "multipart/form-data",
				success : function(data) {
					console.log("yes");
				},
				async: false,
				error : function(jqXHR, textStatus, errorThrown) {
					console.log(errorThrown);
				}
			});
		} else { //nếu ko đổi thì lấy lại ảnh cũ
			artist.img = document.querySelectorAll(".img-container input")[0].dataset.previousvalue;
		}
		
		$.ajax({
			url : "../artistController/"+(type==1?"add":"update"),
			type : "post",
			data : JSON.stringify(artist),
			contentType : "application/json; charset=utf-8",
			success : function(data) {
				if(type == 1){
					artist.id = data;
				}
			},
			async : false,
			error : function(e) {
				console.log(e);
			}
		});
		//end add artist first
		
		//add genres of that artist
		var genresChecked = document.querySelectorAll(".add-form .input-container div div input:checked");
//		console.log(genresChecked)
		const genre = {
			id: null,
			name:""
		}
		
		const artist_genre = {
			id : {
				genre: null,
				artist: artist
			}
		}
		
		const artist_genres = [
			
		]
		
		genresChecked.forEach((i, index) => {
			genre.id = i.dataset.id;
			genre.name = i.dataset.name;
			artist_genre.id.genre = genre;
			artist_genres[index] = JSON.parse(JSON.stringify(artist_genre));
		});
		$.ajax({
			url : "../artistGenreController/"+(type==1?"add":("update?id="+artist.id)),
			type : "post",
			data : JSON.stringify(artist_genres),
			contentType : "application/json; charset=utf-8",
			success : function(data) {
				console.log("artist")
				if(type == 1) {
					alert("Add Artist Successfully")
				} else {
					alert("Modify Artist Successfully")
				}
				blur.click();
				$(".ajax-container").load("../artistController/", function() {
					general();
				});
			},
			async : false,
			error : function(e) {
				console.log(e);
			}
		});
		//end add genres of that artist
	}
	//end function add-update artist
	
	//function add-update song
	function addUpdateSong(type, song_id, song_name, error) {
		if (document.querySelectorAll(".artist-container input:checked").length <= 0) {
			//chưa chọn ca sĩ
			error.textContent = "Artist must be defined";
			return false;
		} else if (document.querySelectorAll(".genre-container input:checked").length <= 0) {
			//chưa chọn thể loại nhạc cho bài hát
			error.textContent = "Genre must be defined";
			return false;
		}
		
		const song = {
				id:(type==1?null:song_id),
				name:song_name,
				resource:"/resources/audio/",
				lyric:document.querySelector(".ajax-container div.add-form textArea").value,
				liked:0,
				listen:0
		}
		
		if(song.id != null) {
			$.ajax({ //get the song
				url: "../songController/getsong?id=" + song.id,
				type: "get",
				data: "",
				contentType: "application/json; charset=utf-8",
				success: function(data) {
					song.listen = data.listen;
					song.liked = data.liked;
				},
				async: false,
				error: function(e) {
					console.log(e);
				}
			});
		}		
		//add song first
		var form = new FormData();
		document.querySelectorAll(".song-container input").forEach(function(file) {
			form.append("file", file.files[0]);//date of form
			form.append("type", 2);
			if(file.files[0] != undefined) {
				//console.log(file.files[0])
				song.resource += file.files[0].name;
			}
		});
		
		if(song.resource.split("/audio/")[song.resource.split("/audio/").length-1] != "") {
			//nếu có file song thì split ra phần tử cuối là tên song
			$.ajax({
				url : "../fileUploadController/upload",
				type:"post",
				data : form,
				contentType : false, //đã mã hóa ko cần type
				processData : false, //ép ko cho form xử lý như bth
				enctype : "multipart/form-data",
				success : function(data) {
					console.log("yes");
				},
				async: false,
				error : function(jqXHR, textStatus, errorThrown) {
					console.log(errorThrown);
				}
			});
		} else {
			song.resource = document.querySelectorAll(".song-container input")[0].dataset.previousvalue;
		}
		
		$.ajax({
			url : "../songController/"+(type==1?"add":"update"),
			type : "post",
			data : JSON.stringify(song),
			contentType : "application/json; charset=utf-8",
			success : function(data) {
//				console.log(data +" lasted id of song")
				if(type == 1) {
					song.id = data;
				}
			},
			async : false,
			error : function(e) {
				console.log(e);
			}
		});
		//end add song first
		
		//add genres of that song
		var genresChecked = document.querySelectorAll(".genre-container div input:checked");
//		console.log(genresChecked)
		const genre = {
			id: null,
			name:""
		}
		
		const song_genre = {
			id : {
				genre: null,
				song: song
			}
		}
		
		const song_genres = [
			
		]
		
		genresChecked.forEach((i, index) => {
			genre.id = i.dataset.id;
			genre.name = i.dataset.name;
			song_genre.id.genre = genre;
			song_genres[index] = JSON.parse(JSON.stringify(song_genre));
		});
		$.ajax({
			url : "../songGenreController/"+(type==1?"add?id=":"update?id=")+song.id,
			type : "post",
			data : JSON.stringify(song_genres),
			contentType : "application/json; charset=utf-8",
			success : function(data) {
				console.log("song genres")
			},
			async : false,
			error : function(e) {
				console.log(e);
			}
		});
		//end add genres of that song
		
		//add artists of that song
		var artistsChecked = document.querySelectorAll(".artist-container div input:checked");
//		console.log(genresChecked)
		var artist = {
			id: null,
			name:""
		}
		
		var song_artist = {
			id : {
				artist: null,
				song: song
			}
		}
		
		var song_artists = [
			
		]
		
		artistsChecked.forEach((i, index) => {
			artist.id = i.dataset.id;
			artist.name = i.dataset.name;
			song_artist.id.artist = artist;
			//console.log(song_artist)
			song_artists[index] = JSON.parse(JSON.stringify(song_artist));
		});
		console.log(song_artists);
		$.ajax({
			url : "../songArtistController/"+(type==1?"add?id=":"update?id=")+song.id,
			type : "post",
			data : JSON.stringify(song_artists),
			contentType : "application/json; charset=utf-8",
			success : function(data) {
				console.log("song artists");
				if(type == 1) {
					alert("Add Song Successfully")
				} else {
					alert("Modify Song Successfully")
				}
				blur.click();
				$(".ajax-container").load("../songController/", function() {
					general();
				});
			},
			async : false,
			error : function(e) {
				console.log(e);
			}
		});
		//end add artists of that song
		
	}
	//end function add-update song
	
	//function add-update user
	function addUpdateUser(type, user_name, error) {
		const emptyPattern = /\s/g;
		const email = document.querySelector(".email-container input");
		const password = document.querySelector(".password-container input");
		const role = document.querySelector(".role-container select");
		const emailPattern = /[\w\d]+@(hcmiu|gmail|yahoo)\.com(\.vn){0,1}/g;
		
		const user = {
				id:(type==1?null:(document.querySelector(".id-container input").value)),
				name:"",
				img:"/resources/img/",
				email:"",
				password:"",
				role:""
		}
		
		if(email.value.trim().length <= 0) {
			error.textContent = "Email cannot be empty";
			return false;
		} else if(emptyPattern.test(email.value)) {
			error.textContent = "Email cannot contain any space characters";
			return false;
		} else if(!emailPattern.test(email.value)) {
			error.textContent = "Email is invalid";
			return false;
		} else if(password.value.trim().length <= 0) {
			error.textContent = "Pasword cannot be empty";
			return false;
		} else if(emptyPattern.test(password.value)) {
			error.textContent = "Pasword cannot contain any space characters";
			return false;
		} else if (role.selectedOptions[0].value == "-1") {
			error.textContent = "Role must be defined";
			return false;
		}
		
		user.name = user_name;
		user.email = email.value;
		user.password = password.value;
		user.role= role.selectedOptions[0].value;
		
		var form = new FormData();
		document.querySelectorAll(".img-container input").forEach(function(file) {
			form.append("file", file.files[0]);//data of form
			form.append("type", 1);
			if(file.files[0] != undefined) {
				//console.log(file.files[0])
				user.img += file.files[0].name;
			}
		});
		
		if(user.img.split("/img/")[user.img.split("/img/").length-1] != "") {
			//nếu có file song thì split ra phần tử cuối là tên tên ảnh
			$.ajax({
				url : "../fileUploadController/upload",
				type:"post",
				data : form,
				contentType : false, //đã mã hóa ko cần type
				processData : false, //ép ko cho form xử lý như bth
				enctype : "multipart/form-data",
				success : function(data) {
					console.log("yes");
				},
				async: false,
				error : function(jqXHR, textStatus, errorThrown) {
					console.log(errorThrown);
				}
			});
		} else {
			user.img = document.querySelectorAll(".img-container input")[0].dataset.previousvalue;
		}
		
		$.ajax({
			url : "../userController/"+(type==1?"add":"update"),
			type : "post",
			data : JSON.stringify(user),
			contentType : "application/json; charset=utf-8",
			success : function(data) {
				console.log("user");
				if(type == 1) {
					alert("Add User Successfully")
				} else {
					alert("Modify User Successfully")
				}
				blur.click();
				$(".ajax-container").load("../userController/", function() {
					general();
				});
			},
			async : false,
			error : function(e) {
				console.log(e);
			}
		});
	}
	//end funtion add-update user
	
	//function add-update album
	function addUpdateAlbum(type, album_id, album_name) { //1 = add, 2 = update
		var artistSelected = false;
		var album = {
				id:type==1?null:album_id,
				name:album_name,
				liked:0,
				artist:null
		}
		
		var artist = {
				id:null,
				name:null
		}
		
		var album_songs = [
			
		]
		
		var album_song = {
				id : {
					album:null,
					song:null
				}
		}
		
		var song = {
				id:null,
				name:null
		}
		
		var artistRadios = document.querySelectorAll(".artist-container div input");
		artistRadios.forEach(artistRadio => {
			if(artistRadio.checked) {
				artistSelected = true;
				artist.id = artistRadio.dataset.id;
				album.artist = artist;
			}
		});
		
//		console.log(album)
		
		if(!artistSelected) {
			error.textContent = "Artist must be defined";
			return false;
		}
			
		artistSelected = false;
		 
		var songDivs = document.querySelectorAll(".song");
		songDivs.forEach(songDiv => {
			if(songDiv.dataset.selected == 1) {
				artistSelected = true;
				song.id = songDiv.dataset.id;
				album_song.id.song = JSON.parse(JSON.stringify(song));
				album_songs.push(JSON.parse(JSON.stringify(album_song)));
			}
		});
		
		if(!artistSelected) {
			var confirmBox = confirm("There are no song selected\nDo you want to select later?");
			if(!confirmBox) {
				document.querySelector(".input-container .add-song").click();
				return false;
			}
		}
		
//		console.log(album_songs);
		
		//add album first
		$.ajax({
			url : "../albumController/"+(type==1?"add":"update"),
			type : "post",
			data : JSON.stringify(album),
			contentType : "application/json; charset=utf-8",
			success : function(data) {
				console.log(data +" lasted id of album")
				if(type == 1) {
					album.id = data;
				}
				album_songs.forEach(as => {
					as.id.album = album;
				});
			},
			async : false,
			error : function(e) {
				console.log(e);
			}
		});
		//end add album first
		
		//add album_song
//		console.log(album_songs);
		$.ajax({
			url : "../albumSongController/"+(type==1?"add":"update")+"?id="+album.id,
			type : "post",
			data : JSON.stringify(album_songs),
			contentType : "application/json; charset=utf-8",
			success : function(data) {
				if(type == 1) {
					alert("Add Album Successfully")
				} else {
					alert("Modify Album Successfully")
				}
				blur.click();
				$(".ajax-container").load("../albumController/", function() {
					general();
				});
			},
			async : false,
			error : function(e) {
				console.log(e);
			}
		});
		//end add album_song 
	}
	//end function add-update album
	
	//function back to previous value of input tags
	function previousValue() {
		var tableName = document.querySelector(".navbar ul li.active").dataset.tablename;
		var resetBtns = document.querySelectorAll(".add-form .input-container div i");
		Array.from(resetBtns).forEach(iTag => 
			iTag.addEventListener("click", function() {
				var inputTag = this.parentElement.getElementsByTagName("input");
//				console.log(inputTag)
				if(inputTag.length != 0) {
					Array.from(inputTag).forEach(input => {
						if(input.type != "file")
							input.value = input.dataset.previousvalue;
						else
							input.value = "";
						if(iTag.parentElement.className == "artist-container") {//manage song mới có chọn artist 
							if(input.id == "artist-checkbox"+input.dataset.previousvalue) {
								input.checked = true;
								if(tableName == "album") {
									input.click();
									//get song of this album
									var songs = [];
									$.ajax({
										url: "../albumController/songs",
										type: "get",
										data: {
											album_id : document.querySelector(".id-container input").value
										},
										contentType: "application/json; charset=utf-8",
										success: function (data) {
											//make songs of this album have red background
											songs = data;
											//end make songs of this album have red background
										},
										async : false,
										error : function(e) {
											console.log(e);
										}
									});
									songs.forEach(song => {
										document.getElementById("song-div"+song.id).classList.add("selected");
									});
									//end get song of this album
								}
							} else {
								input.checked = false;
							}
							
							if(tableName == "song") {
								addGenresOfArtist();
								$.ajax({
									url: "../songController/genres?id="+document.querySelector(".id-container input").value,
									type: "get",
									data: "",
									contentType: "application/json; charset=utf-8",
									success: function (data) {
										//console.log(data)
										resetGenreCheckBox();
										data.forEach(g=> {
											document.querySelector("#genre-checkbox"+g.id).dataset.previousvalue = g.id;
											document.querySelector("#genre-checkbox"+g.id).checked = true;
										})
									},
									async : false,
									error : function(e) {
										console.log(e);
									}
									
								});
							} 
						}
						
						if(iTag.parentElement.className == "genre-container") {//manage song và mamage artist đều có
							if(input.id == "genre-checkbox"+input.dataset.previousvalue) {
								input.checked = true;
							} else {
								input.checked = false;
							}
						}
					});
				} else {
					inputTag = this.parentElement.getElementsByTagName("textarea")[0];
					if(inputTag != undefined)
						inputTag.value = inputTag.dataset.previousvalue;
					inputTag = resetBtns[4].parentElement.getElementsByTagName("select")[0];
					if(inputTag != undefined)
						inputTag.value = inputTag.dataset.previousvalue;
				}
			})
		);
	}
	//End function back to previous value of input tags
	
	//function reset genre checkbox
	function resetGenreCheckBox() {
		var tableName = document.querySelector(".navbar ul li.active").dataset.tablename;
		var checkboxes = document.querySelectorAll(".genre-container input");
		Array.from(checkboxes).forEach(chb => {
			chb.checked = false;
			chb.dataset.previousvalue = "";
		});
		
		if(tableName == "song" || tableName == "album") {
			checkboxes = document.querySelectorAll(".artist-container input");
			Array.from(checkboxes).forEach(chb => {
				chb.checked = false;
				chb.dataset.previousvalue = "";
			});
		}
	}
	//end function reset genre checkbox
	
	//function disable checkbox artist
	function disableCheckboxes(isDisable) {
		var tableName = document.querySelector(".navbar ul li.active").dataset.tablename;
		var genreCheckBoxes = document.querySelectorAll(".genre-container input");
		
		Array.from(genreCheckBoxes).forEach(chb => {
			if(isDisable) {
				chb.disabled = true;
			} else {
				chb.disabled = false;
			}
		});
		if(tableName == "song" || tableName == "album") {
			genreCheckBoxes = document.querySelectorAll(".artist-container input");
			Array.from(genreCheckBoxes).forEach(chb => {
				if(isDisable) {
					chb.disabled = true;
				} else {
					chb.disabled = false;
				}
			});
		}
		
	}
	//end function disable checkbox artist
	
	//Sort
	var sortBtn = document.querySelector(".sort-btn");
	var sortAttribute = document.querySelector(".sort-attribute");
//	sortAttribute.value = sortAttribute.dataset.currentsortattribute;//
	var sortType = document.querySelector(".sort-type");
//	sortType.value = sortType.dataset.currentsorttype;//
	function sortFunc() {
		sortBtn = document.querySelector(".sort-btn");
		sortAttribute = document.querySelector(".sort-attribute");
		sortAttribute.value = sortAttribute.dataset.currentsortattribute;//
		sortType = document.querySelector(".sort-type");
		sortType.value = sortType.dataset.currentsorttype;//
		sortBtn.addEventListener("click", function() {
			var tableName = document.querySelector(".navbar ul li.active").dataset.tablename;
			var url = ""
			if(tableName == "genre") {
				url = "../genreController/?";
			} else if (tableName == "artist") {
				url = "../artistController/?";
			} else if (tableName == "album") {
				url = "../albumController/?";
			} else if (tableName == "song") {
				url = "../songController/?";
			} else if (tableName == "user") {
				url = "../userController/?";
			}
			$(".ajax-container").load(url+
			`attribute=${sortAttribute.value}&type=${sortType.value}`, function(){
				general();
			});
		});
	}
	//End Sort
	
	//Search
	var searchBtn;
	var searchBox;
	var searchResult;
	var counter_search;
	//end Search
	
	//Navbar
	var lies =  document.querySelectorAll(".navbar ul li");
		for(var i = 0; i < lies.length-1; i++) {
			lies[i].addEventListener("click", function() {
				for(var j = 0; j < lies.length-1; j++) {
					lies[j].classList.remove("active");
				}
				this.classList.add("active");
			});
		}
		document.querySelector("#genre").addEventListener("click", function() {
			$(".ajax-container").load("../genreController/", function() {
				general();
			});
		});
		document.querySelector("#artist").addEventListener("click", function() {
			$(".ajax-container").load("../artistController/", function() {
				general();
			});
		});
		document.querySelector("#song").addEventListener("click", function() {
			$(".ajax-container").load("../songController/", function() {
				general();
			});
			document.querySelector(".slide-table").style.height = "1150px";
		});
		document.querySelector("#album").addEventListener("click", function() {
			$(".ajax-container").load("../albumController/", function() {
				general();
			});
		});
		document.querySelector("#user").addEventListener("click", function() {
			$(".ajax-container").load("../userController/", function() {
				general();
			});
		});
		// End Navbar
		
		//general
		function general() {
			slideRefresh();
			addFormFunc();
			modifyForm();
			deleteForm();
			bottonButtonFormClick();
			sortFunc();
			playSongManageSong();
			searchBtn = document.querySelector("div.tool i.fas.fa-search");
			searchBox = document.querySelector("div.tool input");
			searchResult = document.getElementById("search-result-container");
			
			searchBtn.addEventListener("click", function() {
				searchBox.classList.toggle("appear");
				searchResult.classList.toggle("appear");
				
				searchBox.focus();
			});
			
			searchBox.addEventListener("keyup", function(event) {
				let searchContent = this.value.trim();
				if((event.keyCode >= 65 && event.keyCode <= 90) || 
				   (event.keyCode >= 97 && event.keyCode <= 122) ||
				   (event.keyCode >= 48 && event.keyCode <= 57) ||
				   event.keyCode == 8 || event.keyCode == 13) {
					
					clearTimeout(counter_search);
					if(searchContent.length > 0) {
						searchResult.innerHTML = "";
						counter_search = setTimeout(function() {
							var tableName = document.querySelector(".navbar ul li.active").dataset.tablename;
							if(tableName == "genre") {
								url = "../adminController/search?content=" + searchContent.trim() + "&table=genre";
							} else if (tableName == "artist") {
								url = "../adminController/search?content=" + searchContent.trim() + "&table=artist";
							} else if (tableName == "album") {
								url = "../adminController/search?content=" + searchContent.trim() + "&table=album";
							} else if (tableName == "song") {
								url = "../adminController/search?content=" + searchContent.trim() + "&table=song";
							} else if (tableName == "user") {
								url = "../adminController/search?content=" + searchContent.trim() + "&table=user";
							}
							
							$.ajax({
								url: url,
								type: "get",
								data:"",
								contentType: "application/json; charset=utf-8",
								success: function(data) {
//									console.log(data);
									let table = document.createElement("table");
									let tr, td;
									let button;
									let i;
									data[0].forEach(e => {
										tr = document.createElement("tr");
										
										td = document.createElement("td");
										td.textContent = e.id;
										tr.appendChild(td);
										
										td = document.createElement("td");
										td.textContent = e.name;
										tr.appendChild(td);
										
										td = document.createElement("td");
										button = document.createElement("button");
										button.classList.add("modify");
										button.value = e.id;
										i = document.createElement("i");
										i.classList.add("far");
										i.classList.add("fa-edit");
										button.appendChild(i);
										
										td.appendChild(button);
										
										button = document.createElement("button");
										button.classList.add("delete");
										button.value = e.id;
										i = document.createElement("i");
										i.classList.add("far");
										i.classList.add("fa-trash-alt");
										button.appendChild(i);
										
										td.appendChild(button);
										
										tr.appendChild(td);
										
										table.appendChild(tr);
									});
									
									searchResult.appendChild(table);
									
									//add lại sự kiện ấn nút modify và delete
									let modifyBtns = searchResult.querySelectorAll("button.modify");
									modifyBtns.forEach(e => {
										modify_form_general(e);
									});
									
									
									let deleteBtns = searchResult.querySelectorAll("button.delete");
									deleteBtns.forEach(e => {
										delete_form_general(e);
									});
									//end add lại sự kiện ấn nút modify và delete
								},
								async: false,
								error: function(e) {
									console.log(e);
								}
							});
						}, 500);
					} else {
						searchResult.innerHTML = "";
					}
				}
			});
		}
		//end general
});