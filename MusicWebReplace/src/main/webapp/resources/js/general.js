window.addEventListener("DOMContentLoaded", function() {
	$(".ajax-container").load("../homeSubController/", function() {
		slideRefresh();
		songDoubleClick();
		artistMoreClick();
		albumMoreClick();
		loadMoreTopSong();
		genreMoreClick();
	});
	
	//get isLogedin from server
	var isLogedIn = false;
	function getIsLogedIn() {
		$.ajax({
			url: "../loginController/islogin",
			type: "get",
			data: "",
			contentType: "application/json; charset=utf-8",
			success: function(data) {
				isLogedIn = data;
				console.log("Logged in : " + isLogedIn);
			},
			async: false,
			error: function(e) {
				console.log(e);
			}
		});
	}
	getIsLogedIn();
	//end get isLogedin from server
	
	function setCookie(cname, cvalue, exdays) {
		var d = new Date();
		d.setTime(d.getTime() + (exdays*24*60*60*1000));
		var expires = "expires="+ d.toUTCString();
		document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
	}
	
	var blurEle = document.getElementById("blur");
	var blur2Ele = document.getElementById("blur2");
	
	//artist popup
	var artistPopUp = document.querySelector(".artist-popup");
	//end artist popup
	
	//songs popup
	var songsPopUp = document.querySelector(".songs-popup");
	//end songs popup
	
	//lyric popop
	var lyricPopUp = document.querySelector(".song-lyric");
	//end lyric popup
	
	// artist more button
	var artistMoreBtn = document.getElementsByClassName("artist-more-button");
	function artistMoreClick() {
		artistMoreBtn = document.getElementsByClassName("artist-more-button");
		Array.from(artistMoreBtn).forEach(e => {
			artistMoreBtnClicked(e);
		});
	}
	// end artist more button
	
	//album more button 
	var albumMoreBtn = document.getElementsByClassName("album-more-button");
	function albumMoreClick() {
		albumMoreBtn = document.getElementsByClassName("album-more-button");
		Array.from(albumMoreBtn).forEach(e => {
			albumMoreBtnClicked(e);
		});
	}
	//end album more button
	
	//genre more button
	var genreMoreBtn = document.getElementsByClassName("genre-more-button");
	function genreMoreClick() {
		genreMoreBtn = document.getElementsByClassName("genre-more-button");
		Array.from(genreMoreBtn).forEach(e => {
			genreMoreBtnClicked(e);
		});
	}
	//end more button
	
	//genre in genres-box navbar button
	var genresNavbar = document.querySelectorAll("#genres-box span");
	function genreNavBarClick() {
		genresNavbar = document.querySelectorAll("#genres-box span");
		Array.from(genresNavbar).forEach(e => {
			genreMoreBtnClicked(e);
		});
	}
	genreNavBarClick();
	//end genre in genres-box navbar button
	
	//back button
	var backBtn = document.querySelectorAll("i.far.fa-arrow-alt-circle-left");
	function backBtnClick() {
		backBtn = document.querySelectorAll("i.far.fa-arrow-alt-circle-left");
		backBtn.forEach(e => {
			e.addEventListener("click", function() {
				if(artistPopUp.classList.contains("stay")) {
					songsPopUp.classList.remove("active");
					artistPopUp.classList.remove("stay");
				} else {
					songsPopUp.classList.remove("active");
					blur2Ele.classList.remove("appear");
				}
			});
		});
	}
	
	//heart button
	//end heart button
	
	//More button in top song
	var songMoreBtns = document.querySelectorAll("#more-song-button");
	function loadMoreTopSong() {
		songMoreBtns = document.querySelectorAll("#more-song-button");
		let url = "";
		let songsDiv;
		
		songMoreBtns.forEach(e => {
			e.addEventListener("click", function() {
				if(this.dataset.table == "like") {
					url = "../homeSubController/loadmore?type=1&start=";
					songsDiv = document.querySelector("div.top-song.top div.songs[data-belong='like']");
				} else if (this.dataset.table == "listen") {
					url = "../homeSubController/loadmore?type=2&start=";
					songsDiv = document.querySelector("div.top-song.top div.songs[data-belong='listen']");
				}
				$.ajax({
					url: url + this.value,
					type: "get",
					data: "",
					contentType: "application/json; charset=utf-8",
					success: function(data) {
						//console.log(data)
						const song = JSON.parse(JSON.stringify(play_song));
						data[0].forEach((e, index) => {
							song.id = e.id;
							song.song_name = e.name.split("~")[0];
							song.artist_name = e.name.split("~")[1];
							song.img = e.name.split("~")[2];
							song.resource = e.resource;
							let divSong = createASong(song, data[1][index]);
							
							songsDiv.insertBefore(divSong, songsDiv.querySelectorAll("button")[0]);
							
							divSong.addEventListener("dblclick", function() {
								dblClickSong(this, songsDiv.querySelectorAll(".song"));
							});
							
							divSong.querySelector("div.img-container i").addEventListener("click", function() {
								dblClickSong(this.parentElement.parentElement, songsDiv.querySelectorAll(".song"));
							});
						});
	//					songMoreClick();
						
						e.value = parseInt(e.value) + 10;
					},
					async: false,
					error: function(e) {
						console.log(e);
					}
				});
			});
		})
	}
	//end more button in top song
	
	//function create a song for div songs
	function createASong(song, id_liked) {
		let divSong = document.createElement("div");
		divSong.classList.add("song");
		divSong.id = "song-" + song.id;
		let divImg = document.createElement("div");
		divImg.classList.add("img-container");
		let img = document.createElement("img");
		img.alt = "artist";	
		img.src = ".." + song.img;
		divImg.appendChild(img);
		let i_tag = document.createElement("i");
		i_tag.classList.add("fas");
		i_tag.classList.add("fa-play");
		let playBtnOnImg = i_tag;
		divImg.appendChild(i_tag);
		divSong.appendChild(divImg);
		let input = document.createElement("input");
		input.hidden = "hidden";
		input.type = "text";
		input.value = ".." + song.resource;
		divSong.appendChild(input);
		let divSongInfo = document.createElement("div");
		let p = document.createElement("p");
		p.textContent = song.song_name;
		divSongInfo.appendChild(p);
		p = document.createElement("p");
		p.textContent = song.artist_name;
		divSongInfo.appendChild(p);
		divSong.appendChild(divSongInfo);
		let divSongMore = document.createElement("div");
		divSongMore.classList.add("song-more-container");
		i_tag = document.createElement("i");
		i_tag.classList.add("fas");
		i_tag.classList.add("fa-ellipsis-h");
		i_tag.classList.add("song-more");
		divSongMore.appendChild(i_tag);
		let divSongMoreOptions = document.createElement("div");
		divSongMoreOptions.classList.add("song-more-options");
		i_tag = document.createElement("i");
		i_tag.classList.add("far");
		i_tag.classList.add("fa-heart");
		if(id_liked != -1)
			i_tag.classList.add("fas");
		i_tag.dataset.table = "song";
		i_tag.dataset.songid = song.id;
		divSongMoreOptions.appendChild(i_tag);
		i_tag = document.createElement("i");
		i_tag.classList.add("far");
		i_tag.classList.add("fa-comment");
		i_tag.dataset.table = "song";
		i_tag.dataset.songid = song.id;
		divSongMoreOptions.appendChild(i_tag);
		i_tag = document.createElement("i");
		i_tag.classList.add("fas");
		i_tag.classList.add("fa-music");
		i_tag.dataset.songid = song.id;
		divSongMoreOptions.appendChild(i_tag);
		divSongMore.appendChild(divSongMoreOptions);
		divSong.appendChild(divSongMore);
		
		return divSong;
	}
	//end function create a song for div songs
	
	//artist select
	var artistSelect = document.querySelector(".artist-select");
	function artistSelectClick() {
		artistSelect = document.querySelector(".artist-select");
		if(artistSelect != null) {
			artistSelect.addEventListener("change", function() {
				$(".ajax-container").load("../artistController/artistsub?id="+this.value+"&genreid="+this.value, function() {
					slideRefresh();
					songDoubleClick();
					artistMoreClick();
					albumMoreClick();
					artistSelectClick();
					
					songsPopUp = document.querySelector(".songs-popup");
					artistPopUp = document.querySelector(".artist-popup");
					artistMoreBtn = document.getElementsByClassName("artist-more-button");
					albumMoreBtn = document.getElementsByClassName("album-more-button");
				});
			});
		}
	}
	//end artist select
	
	//comments
	
	//end commments
	const commentPopup = document.querySelector(".song-comments");
	const send_user_comment_btn = document.getElementById("send-user-comment-btn");
	const user_comment_box = document.getElementById("user-comment");
	const comment = {
			id: null,
			song: {
				id: null
			},
			user: {
				id: null
			},
			img: null,
			user_name: null,
			content: null,
			liked: null
	}
	
	//send user comment button clicked
	send_user_comment_btn.addEventListener("click", function() {
//		let isLogged = parseInt(document.getElementById("music-web").dataset.musicweb);
		if(isLogedIn) {
			let user_comment = user_comment_box.value.trim();
			if(user_comment.length > 0) {
				let comment_clone = JSON.parse(JSON.stringify(comment));
				comment_clone.song.id = this.value;
				comment_clone.content = user_comment;
				
				$.ajax({
					url: "../commentController/comment",
					type: "post",
					data: JSON.stringify(comment_clone),
					contentType: "application/json; charset=utf-8",
					success: function(data) {
						console.log("comment sent");
						
						//add the newest comment into the top of div comment
						$.ajax({
							url: "../commentController/last?id=" + comment_clone.song.id,
							type: "get",
							data: "",
							contentType: "application/json; charset=utf-8",
							success: function(data) {
//								console.log(data);
								let divComments = commentPopup.querySelector("div.comments");
								comment_clone.img = ".." + data.user.img;
								comment_clone.user_name = data.user.name;
								comment_clone.content = data.content;
								let divComment = createAComment(comment_clone);
								divComments.prepend(divComment);
							},
							async : false,
							error: function(e) {
								console.log(e);
							}
						});
						//end add the newest comment into the top of div comment
					},
					async : false,
					error: function(e) {
						console.log(e);
					}
				});
			} else {
				alert("Must type something!");
				user_comment_box.focus();
			}
		} else {
			let confirm_login = confirm("You MUST log in first!\nDo you want to redirect to log in page");
			if(confirm_login) 
				location.href = "../loginController/";
		}
	});
	//end send user comment button clicked
	
	//function add all comments of song into div comments
	function addComments(id) {
		$.ajax({
			url: "../commentController/comments?id=" + id,
			type: "get",
			data: "",
			contentType: "application/json; charset=utf-8",
			success: function(data) {
//				console.log(data);
				let divComments = commentPopup.querySelector("div.comments");
				let comment_clone = JSON.parse(JSON.stringify(comment));
				divComments.innerHTML = "";
				data.forEach(e => {
					comment_clone.img = ".." + e.user.img;
					comment_clone.user_name = e.user.name;
					comment_clone.content = e.content;
					let divComment = createAComment(comment_clone);
					divComments.append(divComment);
				});
			},
			async : false,
			error: function(e) {
				console.log(e);
			}
		});
	}
	//end funtion add all comments of song into div comments
	
	//function create a comment
	function createAComment(comment) {
		let divComment = document.createElement("div");
		divComment.classList.add("comment");
		let img = document.createElement("img");
		img.alt = "user";
		img.src = comment.img;
		divComment.appendChild(img);
		let divCommentInfo = document.createElement("div");
		let p = document.createElement("p");
		p.textContent = comment.user_name;
		divCommentInfo.appendChild(p);
		p = document.createElement("p");
		p.textContent = comment.content;
		divCommentInfo.appendChild(p);
		divComment.appendChild(divCommentInfo);
		return divComment;
	}
	//end function create a comment
	// links navbar
	document.querySelector("#home").addEventListener("click", function() {
		$(".ajax-container").load("../homeSubController/", function() {
			slideRefresh();
			songDoubleClick();
			artistMoreClick()
			albumMoreClick();
			loadMoreTopSong();
			genreMoreClick();
			
			songsPopUp = document.querySelector(".songs-popup");
			artistPopUp = document.querySelector(".artist-popup");
			artistMoreBtn = document.getElementsByClassName("artist-more-button");
			albumMoreBtn = document.getElementsByClassName("album-more-button");
			
			blurEle.classList.remove("appear");
			document.getElementById("navbar").classList.remove("appear");
		});
	});
	document.querySelector("#artist").addEventListener("click", function() {
		$(".ajax-container").load("../artistController/artistsub", function() {
			slideRefresh();
			songDoubleClick();
			artistMoreClick();
			albumMoreClick();
			artistSelectClick();
			
			songsPopUp = document.querySelector(".songs-popup");
			artistPopUp = document.querySelector(".artist-popup");
			artistMoreBtn = document.getElementsByClassName("artist-more-button");
			albumMoreBtn = document.getElementsByClassName("album-more-button");
			
			blurEle.classList.remove("appear");
			document.getElementById("navbar").classList.remove("appear");
		});
	});
	
	document.querySelector("#album").addEventListener("click", function() {
		$(".ajax-container").load("../albumController/albumsub", function() {
			slideRefresh();
			songDoubleClick();
			artistMoreClick();
			albumMoreClick();
			artistSelectClick();
			
			songsPopUp = document.querySelector(".songs-popup");
			artistPopUp = document.querySelector(".artist-popup");
			artistMoreBtn = document.getElementsByClassName("artist-more-button");
			albumMoreBtn = document.getElementsByClassName("album-more-button");
			
			blurEle.classList.remove("appear");
			document.getElementById("navbar").classList.remove("appear");
		});
	});
	// end links navbar
	
	// slide
	function slideRefresh() {
		forwardBtn = document.querySelectorAll("div.top i.fas.fa-chevron-right");
		backwardBtn = document.querySelectorAll("div.top i.fas.fa-chevron-left");
		frames = null;
		forwardBtn.forEach(e => {
			e.addEventListener("click", function() {
				frames = this.parentElement.getElementsByClassName("frame");
				var activeFrame = this.parentElement.getElementsByClassName("active")[0];
				forwardSlide(activeFrame);
			});
		});
		
		backwardBtn.forEach(e => {
			e.addEventListener("click", function() {
				frames = this.parentElement.getElementsByClassName("frame");
				var activeFrame = this.parentElement.getElementsByClassName("active")[0];
				backwardSlide(activeFrame);
			});
		});
	}
	//
	
	// header
	
	// search
	var searchBtn = document.getElementById("search"); //search icon on navbar
	const searchBox = document.getElementById("search-box");
	const searchInput = searchBox.querySelector("input");
	const searchResultBox = document.getElementById("search-result-container");
	var counterSearch;
	searchBtn.addEventListener("click", function() {
		searchBox.classList.toggle("appear");
		searchBox.querySelector("input").focus();
		
		blurEle.classList.remove("appear");
		document.getElementById("navbar").classList.remove("appear");
		
		hideNavbarBoxes(this);
	});
	
	searchInput.addEventListener("keyup", function(event) {
//		console.log(event.keyCode + "===" + event.which);
		let searchContent = this.value.trim();
		if((event.keyCode >= 65 && event.keyCode <= 90) || 
		   (event.keyCode >= 97 && event.keyCode <= 122) ||
		   (event.keyCode >= 48 && event.keyCode <= 57) ||
		   event.keyCode == 8 || event.keyCode == 13) {
			clearTimeout(counterSearch);
			var divSongs = searchResultBox.querySelector(".songs")
			if(searchContent.length > 0) {
				counterSearch = setTimeout(function() {
//					console.log(searchContent);
					
					divSongs.innerHTML = "";
					$.ajax({
						url: "../homeSubController/search?content=" + searchContent.trim(),
						type: "get",
						data:"",
						contentType: "application/json; charset=utf-8",
						success: function(data) {
							let divSong;
							data[0].forEach(e => {//song
								divSongs.appendChild(createAResult("song", e));
							});
							data[1].forEach(e => {//artist
								divSongs.appendChild(createAResult("artist", e));
							});
							data[2].forEach(e => {//album
								divSongs.appendChild(createAResult("album", e));
							});
							data[3].forEach(e => {//genre
								divSongs.appendChild(createAResult("genre", e));
							});
							
							const songs = divSongs.querySelectorAll(".song");
							songs.forEach(e => {
								let data_table = e.dataset.table;
								if(data_table == "song") {
									e.addEventListener("click", function() {
										dblClickSong(e, [e]);
									});
								} else if (data_table == "artist") {
									artistMoreBtnClicked(e);
								} else if (data_table == "album") {
									albumMoreBtnClicked(e);
								} else if (data_table == "genre") {
									genreMoreBtnClicked(e);
								}
							});
						},
						async: false,
						error: function(e) {
							console.log(e);
						}
					});
				}, 500);
			} else {
				divSongs.innerHTML = "";
			}
		} else {
			if(searchContent.length <= 0) {
				this.value = "";
			}
		}
	});
	
	//function create a result for result search box
	function createAResult(type, e) {
		let divSong;
		let i_tag;
		let p_tag;
		let i_tag_class_1 = "";
		let i_tag_class_2 = "";
		let data_id = "";
		
		divSong = document.createElement("div");
		
		if(type == "song") {
			i_tag_class_1 = "fas";
			i_tag_class_2 = "fa-music";
			divSong.id = "song-" + e.id;
			divSong.dataset.table = "song";
		} else if (type == "artist"){
			i_tag_class_1 = "fas";
			i_tag_class_2 = "fa-user";
			data_id = e.id;
			divSong.dataset.table = "artist";
		} else if (type == "album") {
			i_tag_class_1 = "far";
			i_tag_class_2 = "fa-list-alt";
			data_id = e.id;
			divSong.dataset.table = "album";
		} else if (type == "genre") {
			i_tag_class_1 = "fas";
			i_tag_class_2 = "fa-th-large";
			data_id = e.id;
			divSong.dataset.table = "genre";
		}
		
		divSong.classList.add("song");
		if(type != "song")
			divSong.dataset.id = data_id;
		i_tag = document.createElement("i");
		i_tag.classList.add(i_tag_class_1);
		i_tag.classList.add(i_tag_class_2);
		divSong.appendChild(i_tag);
		p_tag = document.createElement("p");
		p_tag.textContent = e.name;
		divSong.appendChild(p_tag);
		return divSong
	}
	//end function create a result for result search box
	// end search
	
	// navbar toggle
	var navbarToggleBtn = document.getElementById("navbar-toggle");
	navbarToggleBtn.addEventListener("click", function() {
		document.getElementById("navbar").classList.toggle("appear");
		
		blurEle.classList.toggle("appear");
		document.getElementById("search-box").classList.remove("appear");
	});
	// end navbar toggle
	
	// close navbar
	var closeNavbarBtn = document.getElementById("closeNavbar-button");
	closeNavbarBtn.addEventListener("click", function() {
		document.getElementById("navbar").classList.toggle("appear");
		
		blurEle.classList.toggle("appear");
	});
	// end close navbar
	
	// user
	var userBtn = document.getElementById("user");
	userBtn.addEventListener("click", function() {
		hideNavbarBoxes(document.getElementById("user-box"));
		document.getElementById("user-box").classList.toggle("appear");
		document.getElementById("search-box").classList.remove("appear");
	});
	// end user
	
	// genre
	var genreBtn = document.getElementById("genre");
	genreBtn.addEventListener("click", function() {
		hideNavbarBoxes(document.getElementById("genres-box"));
		document.getElementById("genres-box").classList.toggle("appear");
		document.getElementById("search-box").classList.remove("appear");
	});
	// end genre
	
	// sign in button
	var signinBtn = document.getElementById("signin-btn");
	if(signinBtn != null) {
		signinBtn.addEventListener("click", function() {
			location.href = "/MusicWebReplace/loginController/";
		});
	}
	// end sign up button
	
	// sign out button
	var signoutBtn = document.getElementById("signout-btn");
	if(signoutBtn != null) {
		signoutBtn.addEventListener("click", function() {
			location.href = "/MusicWebReplace/loginController/signout";
		});
	}
	// end sign out button
	
	//favorite button 
	const favoriteBtn = document.getElementById("favorite-btn");
	if(favoriteBtn != null) {
		favoriteBtn.addEventListener("click", function() {
			console.log("favorite");
			const genreTitle = document.querySelector(".genre-title");
			const albumTitle = document.querySelector(".album-title");
			const songsDiv = songsPopUp.querySelector(".songs");
			
			songsPopUp.classList.add("active");
			blur2Ele.classList.add("appear");
			genreTitle.style.display = "block";
			albumTitle.style.display = "none";
			
			songsPopUp.querySelectorAll("i.far.fa-arrow-alt-circle-left").forEach(e => {
				if(!artistPopUp.classList.contains("active"))
					e.style.display = "none";
			});
			
			genreTitle.querySelector("span").textContent = "Favorite List";
			
			songsDiv.dataset.belong = "favorite";
			songsDiv.innerHTML = "";
			
			$.ajax({
				url: "../songController/favoritesongs",
				type: "get",
				data: "",
				contentType: "application/json; charset=utf-8",
				success: function(data) {
					//console.log(data);
					// 0 : songs, 2 : artist, 1: ids liked
					const song = JSON.parse(JSON.stringify(play_song));
					let artist_names = "";
					data[0].forEach((e, index) => {
						song.id = e.id;
						song.song_name = e.name;
						artist_names = "";
						data[2][index].forEach((ee, index_2) => {
							if(index_2 == data[2][index].length - 1)
								artist_names += ee.name;
							else 
								artist_names += ee.name + ", ";
						})
						song.artist_name = artist_names;
						song.img = data[2][index][0].img;
						song.resource = e.resource;
						let divSong = createASong(song, data[1][index]);

						songsDiv.appendChild(divSong);
					});
				},
				async: false,
				error: function(e) {
					console.log(e);
				}
			});
			
			const songOfArtist = songsPopUp.querySelectorAll(".song");
			songOfArtist.forEach(e => {
				e.addEventListener("dblclick", function() {
					dblClickSong(e, songOfArtist);
				});
			});
			
			const playBtnImg = songsPopUp.querySelectorAll(".song div.img-container i");
			playBtnImg.forEach(e => {
				e.addEventListener("click", function() {
					dblClickSong(e.parentElement.parentElement, songOfArtist);
				});
			});
		});
	}
	//end favorite button
	
	// blur element
	blurEle.addEventListener("click", function() {
		document.getElementById("navbar").classList.toggle("appear");
		
		blurEle.classList.toggle("appear");
		document.getElementById("search-box").classList.remove("appear");
	});
	// end blur element
	
	// blur2 element
	blur2Ele.addEventListener("click", function() {
		if(songsPopUp.classList.contains("active") && 
		   artistPopUp.classList.contains("active") &&
		   !songsPopUp.classList.contains("stay")) {
			songsPopUp.classList.remove("active");
			artistPopUp.classList.remove("stay");
		} else if (songsPopUp.classList.contains("active") && 
				   commentPopup.classList.contains("active")){
			commentPopup.classList.remove("active");
			songsPopUp.classList.remove("stay");
		} else if(commentPopup.classList.contains("active") && 
				   artistPopUp.classList.contains("active") &&
				   !songsPopUp.classList.contains("active")) {
			commentPopup.classList.remove("active");
			artistPopUp.classList.remove("stay");
		} else if(commentPopup.classList.contains("active") && 
				   artistPopUp.classList.contains("active") &&
				   songsPopUp.classList.contains("active")) {
			commentPopup.classList.remove("active");
			songsPopUp.classList.remove("stay");
		} else {
			artistPopUp.classList.remove("active");
			songsPopUp.classList.remove("active");
			lyricPopUp.classList.remove("active");
			commentPopup.classList.remove("active");
			this.classList.remove("appear");
		}
	});
	// end blur2 element
	
	// logo
	var logo = document.getElementsByClassName("logo");
	Array.from(logo).forEach(aLogo => {
		aLogo.addEventListener("click", function() {
			location.reload();
		});
	});
	// end logo
	
	// header onscroll
	var isOver = false;
	// Nav
	var header = document.getElementById("header");
	window.onscroll = function() {
		var vertical = window.pageYOffset;
		if (vertical > 60) {
			if (!isOver) {
				document.querySelector(".ajax-container").style.paddingTop = "60px";
				header.classList.add("fixed");
			}
			isOver = true;
		} else {
			if (isOver) {
				document.querySelector(".ajax-container").style.paddingTop = "0px";
				header.classList.remove("fixed");
			}
			isOver = false;
		}
	};
	// end header onscroll
// end header
	
	// function hide all box on navbar
	function hideNavbarBoxes(ele) {
		const boxes = document.querySelectorAll("div#header li div");
		boxes.forEach(box => {
			if(box != ele)
				box.classList.remove("appear");
		});
	}
	// end function hide all box on navbar
	
	// Slide
	var forwardBtn = document.querySelectorAll("div.top i.fas.fa-chevron-right");
	var backwardBtn = document.querySelectorAll("div.top i.fas.fa-chevron-left");
	var frames = null;
	
	// Forward
	function forwardSlide(activeFrame) { // reuse for number
		var nextFrame;
		if(activeFrame.dataset.order == frames.length) {
			nextFrame = frames[0];
		} else {
			nextFrame = activeFrame.nextElementSibling;
		}
		
		activeFrame.classList.add("move-next-active");
		activeFrame.addEventListener("webkitAnimationEnd", function() {
			this.classList.remove("move-next-active");
			this.classList.remove("active");
		});
		
		nextFrame.classList.add("move-next-next");
		nextFrame.addEventListener("webkitAnimationEnd", function() {
			this.classList.remove("move-next-next");
			this.classList.add("active");
		});
	}
	// End Forward
	
	// Backward
	function backwardSlide(activeFrame) { // reuse for number
		if(activeFrame.dataset.order == 1) {
			var preFrame;
			preFrame = frames[frames.length-1];
		} else {
			preFrame = activeFrame.previousElementSibling;
		}
		
		activeFrame.classList.add("move-pre-active");
		activeFrame.addEventListener("webkitAnimationEnd", function() {
			this.classList.remove("move-pre-active");
			this.classList.remove("active");
		});
		
		preFrame.classList.add("move-pre-pre");
		preFrame.addEventListener("webkitAnimationEnd", function() {
			this.classList.remove("move-pre-pre");
			this.classList.add("active");
		});
	}
	// End Backward
	// end Slide
	
	// top 100 song
	var topSong1 = document.querySelectorAll("div.top-song div.songs[data-belong='like'] div.song");
	var topSong2 = document.querySelectorAll("div.top-song div.songs[data-belong='listen'] div.song");
	function songDoubleClick() {
		topSong1 = document.querySelectorAll("div.top-song div.songs[data-belong='like'] div.song");
		topSong2 = document.querySelectorAll("div.top-song div.songs[data-belong='listen'] div.song");
		
		topSong1.forEach(e => {
			e.addEventListener("dblclick", function() {
				dblClickSong(e, document.querySelectorAll("div.top-song div.songs[data-belong='like'] div.song"));
			});
			
			e.querySelector("div.img-container i").addEventListener("click", function() {
				dblClickSong(e, document.querySelectorAll("div.top-song div.songs[data-belong='like'] div.song"));
			});
		});
		
		topSong2.forEach(e => {
			e.addEventListener("dblclick", function() {
				dblClickSong(e, document.querySelectorAll("div.top-song div.songs[data-belong='listen'] div.song"));
			});
			
			e.querySelector("div.img-container i").addEventListener("click", function() {
				dblClickSong(e, document.querySelectorAll("div.top-song div.songs[data-belong='listen'] div.song"));
			});
		});
	}
	// end top 100 song
	
	document.body.addEventListener("click", function(element, event) {
//		console.log(element.target.classList.contains("fa-times"))
		if(element.target.classList.contains("song-more")) {
				removeAllSongMoreOption(element.target.parentElement.querySelector("div.song-more-options"));
				element.target.parentElement.querySelector("div.song-more-options").classList.toggle("appear");
				element.target.addEventListener("dblclick", function(event) {
					event.stopPropagation();
				});
				element.target.parentElement.querySelector("div.song-more-options").addEventListener("dblclick", function(event) {
					event.stopPropagation();
				});
		} else if(element.target.classList.contains("fa-comment")) {
			if(songsPopUp.classList.contains("active")) {
				songsPopUp.classList.add("stay");
				commentPopup.classList.add("active");
			} else if(artistPopUp.classList.contains("active")) {
				artistPopUp.classList.add("stay");
				commentPopup.classList.add("active");
			} else if(artistPopUp.classList.contains("stay")) {
				songsPopUp.classList.add("stay");
				commentPopup.classList.add("active");
			} else {
				commentPopup.classList.add("active");
			}
			blur2Ele.classList.add("appear");
			
			//set song id for send user comment button
			send_user_comment_btn.value = element.target.dataset.songid;
			//end set song id for send user comment button
			
			//call function to add all comments of the song clicked
			addComments(element.target.dataset.songid);
			//end call function to add all comments of the song clicked
			
			element.target.addEventListener("dblclick", function(event) {
				event.stopPropagation();
			});
		} else if(element.target.classList.contains("fa-heart")) {
//			console.log(element.target);
			
			const target = element.target;
//			let isLogged = parseInt(document.getElementById("music-web").dataset.musicweb);
			if(isLogedIn) {
				let url = "";
				let type = "";
				let obj = null;
				let data_id = null;
				let data_table = element.target.dataset.table;
				
				if(element.target.dataset.songid != undefined) {
					data_id = "data-songid=" + "'" + element.target.dataset.songid + "'";
				} else if (element.target.dataset.artistid != undefined) {
					data_id = "data-artistid=" + "'" +  element.target.dataset.artistid + "'";
				} else if (element.target.dataset.albumid != undefined) {
					data_id = "data-albumid=" + "'" +  element.target.dataset.albumid + "'";
				}
				
				let hearts = 
					document.querySelectorAll(`i.fa-heart[data-table='${data_table}'][${data_id}]`);
//				console.log(hearts);return;
				if(element.target.classList.contains("fas")) { //unlike
					type = "delete";
					if(target.dataset.table == "song") {
						url = "../favoriteController/delete";
						obj = {
								id: {
									song: {
										id: element.target.dataset.songid
									},
									user: {
										id: null
									}
								}
						}
					} else if(target.dataset.table == "album") {
						url = "../userAlbumController/delete";
						obj = {
								id: {
									album: {
										id: element.target.dataset.albumid
									},
									user: {
										id: null
									}
								}
						}
					} else if(target.dataset.table == "artist") {
						url = "../userArtistController/delete";
						obj = {
								id: {
									artist: {
										id: element.target.dataset.artistid
									},
									user: {
										id: null
									}
								}
						}
					}
				} else { //like
					type = "post";
					if(target.dataset.table == "song") {
						url = "../favoriteController/add";
						obj = {
								id: {
									song: {
										id: element.target.dataset.songid
									},
									user: {
										id: null
									}
								}
						}
					} else if(target.dataset.table == "album") {
						url = "../userAlbumController/add";
						obj = {
								id: {
									album: {
										id: element.target.dataset.albumid
									},
									user: {
										id: null
									}
								}
						}
					} else if(target.dataset.table == "artist") {
						url = "../userArtistController/add";
						obj = {
								id: {
									artist: {
										id: element.target.dataset.artistid
									},
									user: {
										id: null
									}
								}
						}
					}
				}
				
				$.ajax({
					url: url,
					type: type,
					data: JSON.stringify(obj),
					contentType: "application/json; charset=utf-8",
					success: function(data) {
						
						if(element.target.classList.contains("fas")) {
							console.log("unliked");
						} else {
							console.log("liked");
						}
					},
					async: false,
					error: function(e) {
						console.log(e);
					}
				});

				hearts.forEach(e => {
					e.classList.toggle("fas");
				})
			}
		} else if (element.target.classList.contains("fa-times")) {//close button
			blur2Ele.classList.remove("appear");
			commentPopup.classList.remove("active");
			commentPopup.classList.remove("stay");
			songsPopUp.classList.remove("active");
			songsPopUp.classList.remove("stay");
			artistPopUp.classList.remove("active");
			artistPopUp.classList.remove("stay");
			lyricPopUp.classList.remove("active");
		} else if(element.target.classList.contains("fa-arrow-alt-circle-left")) { //về một câp khi mở popup
			blur2Ele.click();
		} else if(element.target.classList.contains("fa-music")) {//open lyric
			let lyric_container = lyricPopUp.querySelector(".lyric");
			lyric_container.innerHTML = "";
			
			//get lyric of this song
			$.ajax({
				url: "../songController/getsong?id=" + element.target.dataset.songid,
				type: "get",
				data: "",
				contentType: "application/json; charset=utf-8",
				success: function(data) {
//					console.log(data[0].lyric);
					lyric_container.textContent = data[0].lyric;
					lyricPopUp.classList.add("active");
					blur2Ele.classList.add("appear");
				}, 
				async: false,
				error: function(e) {
					console.log(e);
				}
			});
			//get lyric of this song
		}
	});
	
	//remove other song more options
	function removeAllSongMoreOption(ele) {
		const songMoreOptions = document.querySelectorAll("div.song-more-container div.song-more-options");
		songMoreOptions.forEach(e => {
			if(e != ele)
				e.classList.remove("appear");
		});
	}
	//end remove other song more options
	
	//audio tag
	const audioTag = document.getElementById("audio");
	const play_forward_btn = document.getElementById("play-forward");
	const play_backward_btn = document.getElementById("play-backward");
	const play_pause_btn = document.getElementById("play-play");
	const repeat_btn = document.getElementById("play-repeat");
	const shuffle_btn = document.getElementById("play-shuffle");
	const play_bar = document.getElementById("bar");
	const light = document.getElementById("light");
	const play_list_btn = document.getElementById("play-list-button");
	const play_list_div = document.getElementById("play-list-songs");
	const expand_footer = document.getElementById("expand-footer");
	const play_bar_container = document.getElementById("play-bar-container");
	
	var minute_current = 0;
	var second_current;
	var minute_end;
	var second_end;
	var playing_list = [];
	var play_song = {
			resource: null,
			id: null,
			song_name: null,
			artist_name: null,
			img: null,
			is_active: false
	}
	
	//shuffle an array
	function shuffle(array) {
		  array.sort(() => Math.random() - 0.5);
	}
	//end shuffle an array
	
	//find the current playing song
	function findIndexActivatedSong() {
		var indexOfSong = 0;
		playing_list.forEach((e, index) => {
			if(e.is_active)
				indexOfSong = index;
		});
		return indexOfSong;
	}
	//end find the current playing song
	
	//set active for a specific song
	function setActiveForASong(id) {
		playing_list[findIndexActivatedSong()].is_active = false;
		playing_list.forEach((e, index) => {
			if(e.id == id)
				e.is_active = true;
		});
	}
	//end set active for a specific song
	
	//return index of a song when shuffle button selected
	function getIndexOfNextSong(indexOfActivedSong, type) {
		let indexOfNextSong;
		if(shuffle_btn.classList.contains("active")) {
			do {
				indexOfNextSong = Math.floor((Math.random() * playing_list.length));
			} while(indexOfNextSong == indexOfActivedSong);
		} else {
			if(type == "forward") {
				if(indexOfActivedSong == playing_list.length - 1) {
					indexOfNextSong = 0;
				} else {
					indexOfNextSong = indexOfActivedSong + 1;
				}
			} else {
				if(indexOfActivedSong == 0) {
					indexOfNextSong = playing_list.length - 1;
				} else {
					indexOfNextSong = indexOfActivedSong - 1;
				}
			}
		}
		return indexOfNextSong;
	}
	//end return index of a song when shuffle button selected
	
	//play forward button clicked
	play_forward_btn.addEventListener("click", function() {
		console.log("forward song");
		//console.log(playing_list)
		let indexOfActivedSong = findIndexActivatedSong();
		let indexOfNextSong = getIndexOfNextSong(indexOfActivedSong, "forward");

		setActiveForASong(playing_list[indexOfNextSong].id);
		
		audioTag.src = ".." + playing_list[indexOfNextSong].resource;
		audioTag.play();
		
		//thông tin song in play bar
		var song_info = document.getElementById("song-info");
		song_info.querySelector("img").src = ".." + playing_list[indexOfNextSong].img;
		song_info.querySelector("p:nth-child(1)").textContent = playing_list[indexOfNextSong].song_name;
		song_info.querySelector("p:nth-child(2)").textContent = playing_list[indexOfNextSong].artist_name;
		//end thông tin song in play bar
		
		removeActiveAllSong();
		play_list_div.querySelector("#song-" + playing_list[indexOfNextSong].id).classList.add("active");
		
		
		if(play_pause_btn.classList.contains("fa-play-circle")) {
			play_pause_btn.classList.add("fa-pause-circle");
			play_pause_btn.classList.remove("fa-play-circle");
		}
//		setEndTime();
	});
	//end play forward button clicked
	
	//play backward button clicked
	play_backward_btn.addEventListener("click", function() {
		console.log("backward song");
		let indexOfActivedSong = findIndexActivatedSong();
		let indexOfNextSong = getIndexOfNextSong(indexOfActivedSong, "backward");

		setActiveForASong(playing_list[indexOfNextSong].id);
		
		audioTag.src = ".." + playing_list[indexOfNextSong].resource;
		audioTag.play();
		
		//thông tin song in play bar
		var song_info = document.getElementById("song-info");
		song_info.querySelector("img").src = ".." + playing_list[indexOfNextSong].img;
		song_info.querySelector("p:nth-child(1)").textContent = playing_list[indexOfNextSong].song_name;
		song_info.querySelector("p:nth-child(2)").textContent = playing_list[indexOfNextSong].artist_name;
		//end thông tin song in play bar
		
		removeActiveAllSong();
		play_list_div.querySelector("#song-" + playing_list[indexOfNextSong].id).classList.add("active");
		
		if(play_pause_btn.classList.contains("fa-play-circle")) {
			play_pause_btn.classList.add("fa-pause-circle");
			play_pause_btn.classList.remove("fa-play-circle");
		}
		
//		setEndTime();
	});
	//end play backward button clicked
	
	//shuffle button clicked
	shuffle_btn.addEventListener("click", function() {
		this.classList.toggle("active");
	});
	//end shuffle button clicked
	
	//update current time and light bar
	audioTag.addEventListener("timeupdate", function() {
		if(!is_set_end_time) {
			setEndTime();
			is_set_end_time = true;
		}
		var temp = Math.floor(this.currentTime);
		if(temp % 60 == 0)
			minute_current = temp / 60;
		second_current = temp % 60;
		play_bar.dataset.before = (minute_current >= 10 ? minute_current : "0" + minute_current) 
								  + ":" + 
								  (second_current >= 10 ? second_current : "0" + second_current);
		
		light.style.width = (temp * 100) / Math.floor(audioTag.duration) + "%";
	});
	//end update current time and light bar
	
	//check song đã chơi xong hay chưa
	audioTag.addEventListener("ended", function() {
		if(repeat_btn.dataset.type == 0) { //không bật repeat
			var indexOfActiveSong = findIndexActivatedSong();
			if(indexOfActiveSong != playing_list.length - 1) {
				play_forward_btn.click();
			} else {
				play_pause_btn.classList.add("fa-play-circle");
				play_pause_btn.classList.remove("fa-pause-circle");
			}
		} else if(repeat_btn.dataset.type == 1) {//đang bật repeat
			play_forward_btn.click();
		} else if(repeat_btn.dataset.type == 2) {//đang bật repeat one
			audioTag.currentTime = 0;
			audioTag.play();
		}
		
		//thông tin song in play bar
		var song_info = document.getElementById("song-info");
		song_info.querySelector("img").src = ".." + playing_list[findIndexActivatedSong()].img;
		song_info.querySelector("p:nth-child(1)").textContent = playing_list[findIndexActivatedSong()].song_name;
		song_info.querySelector("p:nth-child(2)").textContent = playing_list[findIndexActivatedSong()].artist_name;
		//end thông tin song in play bar
		
//		setEndTime();
	});
	//end check song đã chơi xong hay chưa
	
	//set end time of song
	var is_set_end_time = false;
	function setEndTime() {
		setTimeout(function() {
			minute_end = Math.floor(audioTag.duration/60) >= 10 ? 
					 Math.floor(audioTag.duration/60)
					 :
				    Math.floor(audioTag.duration/60);
			second_end = Math.floor(audioTag.duration) - parseInt(minute_end)*60;
			play_bar.dataset.after = (minute_end >= 10 ? minute_end : "0" + minute_end) 
									 + ":" + 
									 (second_end >= 10 ? second_end : "0" + second_end);
		}, 500);
	}
	//end set end time of song
	
	//khi click lên thanh chơi nhạc
	play_bar.addEventListener("click", function(e) {
		//khoảng cách từ cạnh trái với điểm click
//		console.log(e.clientX-this.offsetLeft);
		//tổng độ dài là 100%
		//phần trăm khi click = (khoảng cách đc tính  * 100) / tổng độ dài
		var percentage = ((e.clientX - this.offsetLeft) * 100) / this.offsetWidth;
		light.style.width = percentage + "%";
		// forward song đến thời điểm này, có phần trăm rồi
		// tổng giây là 100 %, thì giây đc forward = (phần trăm của thanh light * tổng giây ) / 100
		audioTag.currentTime = (percentage * Math.floor(audioTag.duration)) / 100;
		second_current = 0;
		minute_current = Math.floor(Math.floor(audioTag.currentTime) / 60);
//		console.log(minute_current);
	});
	//end khi click lên thanh chơi nhạc
	
	//nút play đc click
	play_pause_btn.addEventListener("click", function() {
		if(audioTag.src != "") {
			if(this.classList.contains("fa-play-circle")) {//đang dừng
				this.classList.remove("fa-play-circle");
				this.classList.add("fa-pause-circle");
				audioTag.play();
			} else {//đang phát
				this.classList.add("fa-play-circle");
				this.classList.remove("fa-pause-circle");
				audioTag.pause();
			}
		}
	});
	//end nút play đc click
	
	//repeat button clicked
	repeat_btn.addEventListener("click", function(){
		if(this.dataset.type == 0) {//chưa bật repeat
			this.classList.add("repeat");
			this.dataset.type = 1;
		} else if(this.dataset.type == 1) {//đang bật repeat 
			this.classList.add("repeat_one");
			this.dataset.type = 2;
		} else if(this.dataset.type == 2) {//đang bật repeat one
			this.classList.remove("repeat");
			this.classList.remove("repeat_one");
			this.dataset.type = 0;
		}
	});
	//end repeat button clicked
	
	//play list button clicked
	play_list_btn.addEventListener("click", function() {
		document.getElementById("play-list-songs").classList.toggle("appear");
	});
	//end play list button clicked
	//end audio tag
	
	//remove active all song
	function removeActiveAllSong() {
		const allSongs = document.getElementsByClassName("song");
		Array.from(allSongs).forEach(e => {
			e.classList.remove("active");
		});
	}
	//end funtion remove all song
	
	// function double click to song
	function dblClickSong(ele, songs) {
//		console.log(ele)
		
		//turn on play bar
		play_bar_container.classList.add("appear");
		expand_footer.classList.add("appear");
		//end turn on play bar
		removeActiveAllSong();
		is_set_end_time = false;
		if(ele.classList.contains("active")) {
			audioTag.currentTime = 0;
		} else {
			songs.forEach(e => {
				if(e != ele)
					e.classList.remove("active");
			});
			ele.classList.toggle("active");
			
			// resource of song
			if(songs.length > 0) {
				if(songs[0].parentElement.querySelector("div.song.active") != null) {
					let resource = "";
					let song = null;
					let artist = null;
					$.ajax({
						url: "../songController/getsong?id=" + ele.id.split("-")[1],
						type: "get",
						data: "",
						contentType: "text/plain; charset=utf-8",
						success: function(data) {
//							console.log(data)
							song = data[0];
							artist = data[1];
							resource = song.resource;
						},
						async: false,
						error: function(e) {
							console.log(e);
						}
					});
					
//					const resource = songs[0].parentElement.querySelector("div.song.active").getElementsByTagName("input")[0].value;
					console.log("Resource song : " + resource);
					audioTag.src = ".." + resource;
					audioTag.play();
					
					if(play_pause_btn.classList.contains("fa-play-circle")) {
						play_pause_btn.classList.remove("fa-play-circle");
						play_pause_btn.classList.add("fa-pause-circle");
					}
					
//					setEndTime();
					
					//thông tin song in play bar
					var song_info = document.getElementById("song-info");
					//console.log(artist);
					let artist_name = "";
//					song_info.querySelector("img").src = ele.querySelector("img").src;
					song_info.querySelector("img").src = ".." + artist[0].img;
//					song_info.querySelector("p:nth-child(1)").textContent = ele.querySelector("p:nth-child(1)").textContent;
					song_info.querySelector("p:nth-child(1)").textContent = song.name;
					for(let i = 0; i < artist.length; i++) {
						if(i == artist.length - 1)
							artist_name += artist[i].name;
						else
							artist_name += artist[i].name + ", ";
					}
//					song_info.querySelector("p:nth-child(2)").textContent = ele.querySelector("p:nth-child(2)").textContent;\
					song_info.querySelector("p:nth-child(2)").textContent = artist_name;
					//end thông tin song in play bar
					
					//get list of this song
//					console.log(ele.parentElement);
					var songsDiv = play_list_div.querySelector(".songs");
					songsDiv.dataset.belong = "play-list";
					var type = ele.parentElement.dataset.belong.split("-")[0];
					var id = "";
					//console.log(type)
					if(ele.parentElement.dataset.belong.split("-")[1] != undefined)
						id = ele.parentElement.dataset.belong.split("-")[1];
					if(type == "artist" || type == "search") {
						console.log("artist songs");
						
						if(type == "search") {
							for(let i = 0; i < artist.length; i++)
								if(i == artist.length - 1)
									id += artist[i].id;
								else
									id += artist[i].id + ",";
						}
						url = "../songArtistController/songs?id=" + id;
						artistAddSongToSongsDiv(url, songsDiv, true, ele);
					} else if(type == "album") {
						console.log("album songs");
						url = "../albumSongController/songs?id=" + id;
						albumAddSongToSongsDiv(url, songsDiv, true, ele);
					} else if(type == "genre") {
						console.log("genre songs");
						url = "../songGenreController/songs?id=" + id + "&play=true";
						genreAddSongToSongsDiv(url, songsDiv, true, ele);
					} else if(type == "like") { //1 = top like
						console.log("top-song songs like");
						url = "../songController/topsongs?type=1";
						topSongAddSongToSongsDiv(url, songsDiv, true, ele);
					} else if (type == "listen"){//2 = top listen
						console.log("top-song songs listen");
						url = "../songController/topsongs?type=2";
						topSongAddSongToSongsDiv(url, songsDiv, true, ele);
					} else if (type == "favorite") {
						console.log("favorite songs");
						url = "../songController/favoritesongs";
						favoriteAddSongToSongsDiv(url, songsDiv, true, ele);
					} else {//nếu double click on play list
						setActiveForASong(ele.id.split("-")[1]);
						console.log(playing_list)
					}
					if (type != "play") {
						const songsPlayList = songsDiv.querySelectorAll(".song");
						songsPlayList.forEach(e => {
							e.addEventListener("dblclick", function() {
								dblClickSong(e, songsPlayList);
							});
						});
						
						const playBtnImg = songsDiv.querySelectorAll(".song div.img-container i");
						playBtnImg.forEach(e => {
							e.addEventListener("click", function() {
								dblClickSong(e.parentElement.parentElement, songsPlayList);
							});
						});
					}
					//get list of this song
				}
				
				//active the song played in play list
				play_list_div.querySelector("#song-" + ele.id.split("-")[1]).classList.add("active");
				//end active the song played in play list
				
				//update listen time
				$.ajax({ //get the song
					url: "../songController/getsong?id=" + ele.id.split("-")[1],
					type: "get",
					data: "",
					contentType: "application/json; charset=utf-8",
					success: function(data) {
//						console.log(data);
						const song = data[0];
						
						song.listen = song.listen + 1;
						
						$.ajax({ //update song with listen added 1
							url: "../songController/update",
							type: "post",
							data: JSON.stringify(song),
							contentType: "application/json; charset=utf-8",
							success: function(data) {
								console.log("listen + 1");
							},
							async: false,
							error: function(e) {
								console.log(e);
							}
						});
					},
					async: false,
					error: function(e) {
						console.log(e);
					}
				});
				//end update listen time
			}
			// end resource of song
		}
	}
	// end function double click to song
	
	//function when artist more button clicked
	function artistMoreBtnClicked(e) {
		e.addEventListener("click", function() {
			songsPopUp.querySelectorAll("i.far.fa-arrow-alt-circle-left").forEach(e => {
				e.style.display = "block";
			});
			artistPopUp.querySelector(".songs").dataset.belong = "artist-" + this.dataset.id;
			$.ajax({
				url: "../artistController/artist?id="+this.dataset.id,
				type: "get",
				data: "",
				contentType: "application/json; charset=utf-8",
				success: function (data) {
//					console.log(data)
					artistPopUp.querySelector("img").src = ".."+data.img;
					artistPopUp.querySelector(".artitst-info p:nth-child(1)").textContent = data.description;
				
					$.ajax({
						url: "../artistController/genres?id="+data.id,
						type: "get",
						data: "",
						contentType: "application/json; charset=utf-8",
						success: function (data) {
							artistPopUp.querySelector(".artitst-info p:nth-child(2)").innerHTML = "";
							data.forEach((e, index) => {
								var span = document.createElement("span");
								
								if(index == data.length-1) {
									span.textContent = e.name;
								} else {
									span.textContent = e.name + ", ";
								}
								artistPopUp.querySelector(".artitst-info p:nth-child(2)").appendChild(span);
							});
						},
						async : false,
						error : function(e) {
							console.log(e);
						}
					});
					
					$.ajax({
						url: "../songArtistController/songs?id="+data.id,
						type: "get",
						data: "",
						contentType: "application/json; charset=utf-8",
						success: function (data) {
							artistPopUp.querySelector(".songs").innerHTML = "";
							const song = JSON.parse(JSON.stringify(play_song));
							let artist_names = "";
							data[0].forEach((e, index) => {
								song.id = e.id;
								song.song_name = e.name;
								artist_names = "";
								artist_names = "";
								data[2][index].forEach((ee, index_2) => {
									if(index_2 == data[2][index].length - 1)
										artist_names += ee.name;
									else 
										artist_names += ee.name + ", ";
								})
								song.artist_name = artist_names;
								song.img = data[2][index][0].img;
								song.resource = e.resource;
								let divSong = createASong(song, data[1][index]);
								
								artistPopUp.querySelector(".songs").appendChild(divSong);
							});
						},
						async : false,
						error : function(e) {
							console.log(e);
						}
					});
					
					$.ajax({
						url: "../albumController/albums?id="+data.id,
						type: "get",
						data: "",
						contentType: "application/json; charset=utf-8",
						success: function (data) {
//							console.log(data);
							const frameContainer = document.querySelector(".frame-container");
							frameContainer.innerHTML = "";
							var frame = Math.floor(data[0].length / 5);
							if(data[0].length % 5 > 0)
								frame++;
							for(let i = 0; i < frame; i++) {
								var frameDiv = document.createElement("div");
								frameDiv.classList.add("frame");
								frameDiv.dataset.order = i+1;
								if(i == 0) {
									frameDiv.classList.add("active");
								}
								var start = 0;
								var end = start+5;
								for(let j = start; j < end; j++) {
									if(j < data[0].length) {
										var frameElementDiv = document.createElement("div");
										frameElementDiv.classList.add("frame-element");
										var img = document.createElement("img");
										img.alt = "artist";
										img.src = ".."+data[0][j].artist.img;
										frameElementDiv.appendChild(img);
										var p = document.createElement("p");
										p.textContent = data[0][j].name;
										frameElementDiv.appendChild(p);
										var genreDiv = document.createElement("div");
										genreDiv.classList.add("genres");
										var a = document.createElement("a");
										a.href = "#";
										a.textContent = data[0][j].artist.name;
										genreDiv.appendChild(a);
										frameElementDiv.appendChild(genreDiv);
										var moreDiv = document.createElement("div");
										moreDiv.classList.add("more");
										var span = document.createElement("span");
										let i_tag = document.createElement("i");
										i_tag.classList.add("far");
										i_tag.classList.add("fa-heart");
										if(data[1][j] != -1)
											i_tag.classList.add("fas");
										i_tag.dataset.table = "album";
										i_tag.dataset.albumid = data[0][j].id;
										span.appendChild(i_tag);
										moreDiv.appendChild(span);
										span = document.createElement("span");
										span.textContent = data[0][j].liked;
										moreDiv.appendChild(span);
										span = document.createElement("span");
										span.textContent = "More";
										span.classList.add("album-more-button");
										span.dataset.id = data[0][j].id;
										moreDiv.appendChild(span);
										frameElementDiv.appendChild(moreDiv);
										frameDiv.appendChild(frameElementDiv);
									}
								}
								start += 5;
								frameContainer.appendChild(frameDiv);
							}
						},
						async : false,
						error : function(e) {
							console.log(e);
						}
					});
				},
				async : false,
				error : function(e) {
					console.log(e);
				}
			});
			
			const songOfArtist = artistPopUp.querySelectorAll(".song");
			songOfArtist.forEach(e => {
				e.addEventListener("dblclick", function() {
					dblClickSong(e, songOfArtist);
				});
			});
			
			const playBtnImg = artistPopUp.querySelectorAll(".song div.img-container i");
			playBtnImg.forEach(e => {
				e.addEventListener("click", function() {
					dblClickSong(e.parentElement.parentElement, songOfArtist);
				});
			});
			
			const albumOfArtist = artistPopUp.querySelectorAll(".album-more-button");
			Array.from(albumOfArtist).forEach(e => {
				albumMoreBtnClicked(e);
			});
			
			artistPopUp.classList.toggle("active");
			blur2Ele.classList.toggle("appear");
		});
	}
	//end function when artist more button clicked
	
	//function when album more button clicked
	function albumMoreBtnClicked(e) {
		e.addEventListener("click", function() {
//			console.log(e);
			const genreTitle = document.querySelector(".genre-title");
			const albumTitle = document.querySelector(".album-title");
			const songsDiv = songsPopUp.querySelector(".songs");
			
			let url = "";
			if(e.classList.contains("album-more-button")) {
				genreTitle.style.display = "none";
				albumTitle.style.display = "block";
				url = "../albumSongController/songs?id="+this.dataset.id;
				songsDiv.dataset.belong = "album-" + this.dataset.id;
			}
			
			songsPopUp.querySelectorAll("i.far.fa-arrow-alt-circle-left").forEach(e => {
				if(!artistPopUp.classList.contains("active"))
					e.style.display = "none";
			});
			
			songsDiv.innerHTML = "";
			
			$.ajax({
				url: url,
				type: "get",
				data: "",
				contentType: "application/json; charset=utf-8",
				success: function (data) {
					const song = JSON.parse(JSON.stringify(play_song));
					let artist_names = "";
					data[0].forEach((e, index) => {
						song.id = e.id.song.id;
						song.song_name = e.id.song.name;
						artist_names = "";
						data[2][index].forEach((ee, index_2) => {
							if(index_2 == data[2][index].length - 1)
								artist_names += ee.name;
							else 
								artist_names += ee.name + ", ";
						})
						song.artist_name = artist_names;
						song.img = data[2][index][0].img;
						song.resource = e.id.song.resource;
						let divSong = createASong(song, data[1][index]);

						songsDiv.appendChild(divSong);
					});
					
					if(e.classList.contains("album-more-button")) {
						albumTitle.querySelectorAll("span")[0].textContent = data[0][0].id.album.name;
						albumTitle.querySelectorAll("span")[1].textContent = data[0][0].id.album.artist.name;
					} else {
						genreTitle.querySelector("span").textContent = data[0][0].id.genre.name;
					}
				},
				async : false,
				error : function(e) {
					console.log(e);
				}
			});
			
			const songOfArtist = songsPopUp.querySelectorAll(".song");
			songOfArtist.forEach(e => {
				e.addEventListener("dblclick", function() {
					dblClickSong(e, songOfArtist);
				});
			});
			
			const playBtnImg = songsPopUp.querySelectorAll(".song div.img-container i");
			playBtnImg.forEach(e => {
				e.addEventListener("click", function() {
					dblClickSong(e.parentElement.parentElement, songOfArtist);
				});
			});
			
			songsPopUp.classList.toggle("active");
			if(artistPopUp.classList.contains("active")) {
				artistPopUp.classList.add("stay");
			}
			blur2Ele.classList.add("appear");
			
			blurEle.classList.remove("appear");
			document.getElementById("navbar").classList.remove("appear");
		});
	}
	//end function when album more button clicked
	
	//function genre more button clicked
	function genreMoreBtnClicked(e) {
		e.addEventListener("click", function() {
			const genreTitle = document.querySelector(".genre-title");
			const albumTitle = document.querySelector(".album-title");
			const songsDiv = songsPopUp.querySelector(".songs");
			let url = "";
			if(e.classList.contains("album-more-button")) {
			} else {
				genreTitle.style.display = "block";
				albumTitle.style.display = "none";
				url = "../songGenreController/songs?id="+this.dataset.id + "&play=false";
				songsDiv.dataset.belong = "genre-" + this.dataset.id;
			}
			
			songsPopUp.querySelectorAll("i.far.fa-arrow-alt-circle-left").forEach(e => {
				if(!artistPopUp.classList.contains("active"))
					e.style.display = "none";
			});
			
			songsDiv.innerHTML = "";
			
			$.ajax({
				url: url,
				type: "get",
				data: "",
				contentType: "application/json; charset=utf-8",
				success: function (data) {
					//console.log(data)
					var artistName = "";
					var artistImg = "";
					const song = JSON.parse(JSON.stringify(play_song));
					let artist_names = "";
					data[0].forEach((e, index) => {
						song.id = e.id.song.id;
						song.song_name = e.id.song.name;
						artist_names = "";
						data[2][index].forEach((ee, index_2) => {
							if(index_2 == data[2][index].length - 1)
								artist_names += ee.name;
							else 
								artist_names += ee.name + ", ";
						})
						song.artist_name = artist_names;
						song.img = data[2][index][0].img;
						song.resource = e.id.song.resource;
						let divSong = createASong(song, data[1][index]);
						
						songsDiv.appendChild(divSong);
					});
					
					if(e.classList.contains("album-more-button")) {
						albumTitle.querySelectorAll("span")[0].textContent = data[0][0].id.album.name;
						albumTitle.querySelectorAll("span")[1].textContent = data[0][0].id.album.artist.name;
					} else {
						genreTitle.querySelector("span").textContent = data[0][0].id.genre.name.split("~")[0];
					}
				},
				async : false,
				error : function(e) {
					console.log(e);
				}
			});
			
			const songOfArtist = songsPopUp.querySelectorAll(".song");
			songOfArtist.forEach(e => {
				e.addEventListener("dblclick", function() {
					dblClickSong(e, songOfArtist);
				});
			});
			
			const playBtnImg = songsPopUp.querySelectorAll(".song div.img-container i");
			playBtnImg.forEach(e => {
				e.addEventListener("click", function() {
					dblClickSong(e.parentElement.parentElement, songOfArtist);
				});
			});
			
			songsPopUp.classList.toggle("active");
			blur2Ele.classList.add("appear");
			
			blurEle.classList.remove("appear");
			document.getElementById("navbar").classList.remove("appear");
		});
	}
	//end function genre more button clicked
	
	//add song to songs div of album
	function albumAddSongToSongsDiv(url, songsDiv, isOutSidePlayList, eleActivated) {
		$.ajax({
			url: url,
			type: "get",
			data: "",
			contentType: "application/json; charset=utf-8",
			success: function (data) {
//				console.log(data)
				songsDiv.innerHTML = "";
				playing_list = [];
				const song = JSON.parse(JSON.stringify(play_song));
				let artist_names = "";
				data[0].forEach((e, index) => {
					song.id = e.id.song.id;
					song.song_name = e.id.song.name;
					artist_names = "";
					data[2][index].forEach((ee, index_2) => {
						if(index_2 == data[2][index].length - 1)
							artist_names += ee.name;
						else 
							artist_names += ee.name + ", ";
					})
					song.artist_name = artist_names;
					song.img = data[2][index][0].img;
					song.resource = e.id.song.resource;
					let divSong = createASong(song, data[1][index]);

					if(eleActivated.id.split("-")[1] == e.id.song.id) {
						songsDiv.prepend(divSong);
					} else {
						songsDiv.appendChild(divSong);
					}
					outsidePlayList(isOutSidePlayList, eleActivated, 
							createAnInfoSongObj(song.id, 
												song.song_name, 
												song.artist_name, 
												song.img, 
												song.resource));
				});
				//console.log(playing_list);
			},
			async : false,
			error : function(e) {
				console.log(e);
			}
		});
	}
	//end add song to songs div of album
	
	//add songs of top song to playlist
	function topSongAddSongToSongsDiv(url, songsDiv, isOutSidePlayList, eleActivated) {
		$.ajax({
			url: url,
			type: "get",
			data: "",
			contentType: "application/json; charset=utf-8",
			success: function (data) {
				//console.log(data)
				//0 : songs, 1: ids liked, 2 : artits 
				songsDiv.innerHTML = "";
				playing_list = [];
				const song = JSON.parse(JSON.stringify(play_song));
				let artist_name = "";
				data[0].forEach((e, index) => {
					song.id = e.id;
					song.song_name = e.name;
					artist_names = "";
					data[2][index].forEach((ee, index_2) => {
						if(index_2 == data[2][index].length - 1)
							artist_names += ee.name;
						else 
							artist_names += ee.name + ", ";
					})
					song.artist_name = artist_names;
					song.img = data[2][index][0].img;
					song.resource = e.resource;
					let divSong = createASong(song, data[1][index]);
					
					if(eleActivated.id.split("-")[1] == e.id) {
						songsDiv.prepend(divSong);
					} else {
						songsDiv.appendChild(divSong);
					}
					outsidePlayList(isOutSidePlayList, eleActivated, 
							createAnInfoSongObj(song.id, 
												song.song_name, 
												song.artist_name, 
												song.img, 
												song.resource));
				});
				//console.log(playing_list);
			},
			async : false,
			error : function(e) {
				console.log(e);
			}
		});
	}
	//end add songs of top song to playlist
	
	//add favorite songs to playlist
	function favoriteAddSongToSongsDiv(url, songsDiv, isOutSidePlayList, eleActivated) {
		$.ajax({
			url: url,
			type: "get",
			data: "",
			contentType: "application/json; charset=utf-8",
			success: function (data) {
//				console.log(data)
				songsDiv.innerHTML = "";
				playing_list = [];
				const song = JSON.parse(JSON.stringify(play_song));
				let artist_name = "";
				data[0].forEach((e, index) => {
					song.id = e.id;
					song.song_name = e.name;
					artist_names = "";
					data[2][index].forEach((ee, index_2) => {
						if(index_2 == data[2][index].length - 1)
							artist_names += ee.name;
						else 
							artist_names += ee.name + ", ";
					})
					song.artist_name = artist_names;
					song.img = data[2][index][0].img;
					song.resource = e.resource;
					let divSong = createASong(song, data[1][index]);
					
					if(eleActivated.id.split("-")[1] == e.id) {
						songsDiv.prepend(divSong);
					} else {
						songsDiv.appendChild(divSong);
					}
					outsidePlayList(isOutSidePlayList, eleActivated, 
							createAnInfoSongObj(e.id, 
												e.name, 
												song.artist_name, 
												song.img, 
												song.resource));
				});
				//console.log(playing_list);
			},
			async : false,
			error : function(e) {
				console.log(e);
			}
		});
	}
	//end add favorite songs  to playlist
	
	//add songs of genre to playlist
	function genreAddSongToSongsDiv(url, songsDiv, isOutSidePlayList, eleActivated) {
		$.ajax({
			url: url,
			type: "get",
			data: "",
			contentType: "application/json; charset=utf-8",
			success: function (data) {
//				console.log(data)
				songsDiv.innerHTML = "";
				playing_list = [];
				const song = JSON.parse(JSON.stringify(play_song));
				let artist_names = "";
				data[0].forEach((e, index) => {
					song.id = e.id.song.id;
					song.song_name = e.id.song.name;
					artist_names = "";
					data[2][index].forEach((ee, index_2) => {
						if(index_2 == data[2][index].length - 1)
							artist_names += ee.name;
						else 
							artist_names += ee.name + ", ";
					})
					song.artist_name = artist_names;
					song.img = data[2][index][0].img;
					song.resource = e.id.song.resource;
					let divSong = createASong(song, data[1][index]);
					
					if(eleActivated.id.split("-")[1] == e.id.song.id) {
						songsDiv.prepend(divSong);
					} else {
						songsDiv.appendChild(divSong);
					}
					outsidePlayList(isOutSidePlayList, eleActivated, 
							createAnInfoSongObj(song.id, 
												song.song_name, 
												song.artist_name, 
												song.img, 
												song.resource));
				});
				//console.log(playing_list);
			},
			async : false,
			error : function(e) {
				console.log(e);
			}
		});
	}
	//end add songs of genre to playlist
	
	//add songs of artist to playlist
	function artistAddSongToSongsDiv(url, songsDiv, isOutSidePlayList, eleActivated) {
		$.ajax({
			url: url,
			type: "get",
			data: "",
			contentType: "application/json; charset=utf-8",
			success: function (data) {
				//0: songs, 1 : ids liked
				songsDiv.innerHTML = "";
				playing_list = [];
				const song = JSON.parse(JSON.stringify(play_song));
				let artist_names = "";
				data[0].forEach((e, index) => {
					song.id = e.id;
					song.song_name = e.name;
					artist_names = "";
					data[2][index].forEach((ee, index_2) => {
						if(index_2 == data[2][index].length - 1)
							artist_names += ee.name;
						else 
							artist_names += ee.name + ", ";
					})
					song.artist_name = artist_names;
					song.img = data[2][index][0].img;
					song.resource = e.resource;
					let divSong = createASong(song, data[1][index]);
					
					if(eleActivated.id.split("-")[1] == e.id) {
						songsDiv.prepend(divSong);
					} else {
						songsDiv.appendChild(divSong);
					}
					outsidePlayList(isOutSidePlayList, eleActivated, 
							createAnInfoSongObj(song.id, 
												song.song_name, 
												song.artist_name, 
												song.img, 
												song.resource));
				});
				//console.log(playing_list);
			},
			async : false,
			error : function(e) {
				console.log(e);
			}
		});
	}
	//end add songs of artist to playlist
	
	//create an info song object
	function createAnInfoSongObj(id, song_name, artist_name, img, resource) {
		let info_song = JSON.parse(JSON.stringify(play_song));
//		console.log(song_name)
		info_song.id = id;
		info_song.song_name = song_name;
		info_song.artist_name = artist_name;
		info_song.img = img;
		info_song.resource = resource;
		return info_song;
	}
	//end create an info song object
	
	//outside playlist function
	function outsidePlayList(isOutSidePlayList, eleActivated, info_song) {
		if(isOutSidePlayList) {
			if(eleActivated.id.split("-")[1] == info_song.id) {
				info_song.is_active = true;
				playing_list.unshift(JSON.parse(JSON.stringify(info_song)));
			} else {
				info_song.is_active = false;
				playing_list.push(JSON.parse(JSON.stringify(info_song)));
			}
			
		}
	}
	//end outside playlist function
});