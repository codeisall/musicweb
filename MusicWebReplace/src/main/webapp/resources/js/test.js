
//window.addEventListener("DOMContentLoaded", function() {
	var blurEle = document.getElementById("blur");
	var blur2Ele = document.getElementById("blur2");
	import {hideNavbarBoxes} from '/MusicWebReplace/resources/js/home.js';
	export { blurEle, blur2Ele };
	// end links navbar
	// header
		// search
		var searchBtn = document.getElementById("search");
		searchBtn.addEventListener("click", function() {
			document.getElementById("search-box").classList.toggle("appear");
			document.getElementById("search-box").querySelector("input").focus();
			
			blurEle.classList.remove("appear");
			document.getElementById("navbar").classList.toggle("appear");
		});
		// end search
		
		// navbar toggle
		var navbarToggleBtn = document.getElementById("navbar-toggle");
		navbarToggleBtn.addEventListener("click", function() {
			document.getElementById("navbar").classList.toggle("appear");
			
			blurEle.classList.toggle("appear");
			document.getElementById("search-box").classList.remove("appear");
		});
		// end navbar toggle
		
		// close navbar
		var closeNavbarBtn = document.getElementById("closeNavbar-button");
		closeNavbarBtn.addEventListener("click", function() {
			document.getElementById("navbar").classList.toggle("appear");
			
			blurEle.classList.toggle("appear");
		});
		// end close navbar
		
		// user
		var userBtn = document.getElementById("user");
		userBtn.addEventListener("click", function() {
			hideNavbarBoxes(document.getElementById("user-box"));
			document.getElementById("user-box").classList.toggle("appear");
		});
		// end user
		
		// genre
		var genreBtn = document.getElementById("genre");
		$("#genre").off("click", function() {
			console.log("!")
			hideNavbarBoxes(document.getElementById("genres-box"));
			document.getElementById("genres-box").classList.toggle("appear");
		})
		genreBtn.addEventListener("click", function() {
			console.log("!")
			hideNavbarBoxes(document.getElementById("genres-box"));
			document.getElementById("genres-box").classList.toggle("appear");
		});
		// end genre
		
		// sign in button
		var signinBtn = document.getElementById("signin-btn");
		if(signinBtn != null) {
			signinBtn.addEventListener("click", function() {
				location.href = "/MusicWebReplace/loginController/";
			});
		}
		// end sign up button
		
		// sign out button
		var signoutBtn = document.getElementById("signout-btn");
		if(signoutBtn != null) {
			signoutBtn.addEventListener("click", function() {
				location.href = "/MusicWebReplace/loginController/signout";
			});
		}
		// end sign out button
		
		// blur element
		blurEle.addEventListener("click", function() {
			document.getElementById("navbar").classList.toggle("appear");
			
			blurEle.classList.toggle("appear");
			document.getElementById("search-box").classList.remove("appear");
		});
		// end blur element
		
		// blur2 element
		blur2Ele.addEventListener("click", function() {})
		blur2Ele.addEventListener("click", function() {
			if(songsPopUp.classList.contains("active") && artistPopUp.classList.contains("active")) {
				songsPopUp.classList.remove("active");
				artistPopUp.classList.remove("stay");
			} else {
				artistPopUp.classList.remove("active");
				songsPopUp.classList.remove("active");
				this.classList.remove("appear");
			}
		});
		// end blur2 element
		
		// logo
		var logo = document.getElementsByClassName("logo");
		Array.from(logo).forEach(aLogo => {
			aLogo.addEventListener("click", function() {
				location.reload();
			});
		});
		// end logo
		
		// header onscroll
		var isOver = false;
		// Nav
		var header = document.getElementById("header");
		window.onscroll = function() {
			var vertical = window.pageYOffset;
			if (vertical > 60) {
				if (!isOver) {
					document.querySelector(".ajax-container").style.paddingTop = "60px";
					header.classList.add("fixed");
				}
				isOver = true;
			} else {
				if (isOver) {
					document.querySelector(".ajax-container").style.paddingTop = "0px";
					header.classList.remove("fixed");
				}
				isOver = false;
			}
		};
		// end header onscroll
	// end header
//})