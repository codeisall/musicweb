<%@page import="com.musicweb.model.User"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<h1>Manage User</h1>
<div class="tool">
	<div style="position: relative;">
		<button class="add">
			<i class="far fa-plus-square"></i>
		</button>
		<i class="fas fa-search"></i> <input type="text"
			placeholder="Search for name or id...">
		<div id="search-result-container"></div>
	</div>
	<div class="sort-container">
		<select class="sort-attribute"
			data-currentSortAttribute="${attribute}">
			<option value="id">ID</option>
			<option value="name">Name</option>
		</select> <select class="sort-type" data-currentSortType="${type}">
			<option value="1">Increase</option>
			<option value="2">Decrease</option>
		</select>
		<button class="sort-btn">Sort</button>
	</div>
</div>
<div class="genre-table slide-table">
	<i class="fas fa-chevron-left"></i> <i class="fas fa-chevron-right"></i>
	<%
		int table = ((ArrayList<User>) request.getAttribute("users")).size() / 10;
	%>
	<c:if test="${fn:length(users)%10 > 0}">
		<%
			table++;
		%>
	</c:if>

	<c:set var="countGenre" value="1"></c:set>

	<c:forEach begin="1" end="<%=table%>" var="countTable">
		<table <c:if test="${countTable == 1}">class="active"</c:if> border=1
			data-order="${countTable}" style="table-layout: fixed;">
			<tr>
				<th>ID</th>
				<th>Name</th>
				<th>Image</th>
				<th>Email</th>
				<th>Password</th>
				<th>Role</th>
				<th>Action</th>
			</tr>
			<c:forEach varStatus="loop" begin="${countGenre}"
				end="${countGenre+9}" var="i">
				<c:if test="${i <= fn:length(users)}">
					<tr data-genreId="${users[i-1].id}" data-genreOrder="${i}">
						<td>${users[i-1].id}</td>
						<td>${users[i-1].name}</td>
						<td><img alt="img" src="..${users[i-1].img}"></td>
						<td style="word-break: break-all;">${users[i-1].email}</td>
						<td><input readonly="readonly"
							style="outline: none; height: inherit; width: -webkit-fill-available; border: none; background: none; font-size: 18px; text-align: center;"
							type="password" value="${users[i-1].password}"></td>
						<td>${users[i-1].role}</td>
						<td width="140px">
							<button class="modify" value="${users[i-1].id}">
								<i class="far fa-edit"></i>
							</button>
							<button class="delete" value="${users[i-1].id}">
								<i class="far fa-trash-alt"></i>
							</button>
						</td>
					</tr>
				</c:if>
			</c:forEach>
			<c:set var="countGenre" value="${countGenre+10}"></c:set>
		</table>
	</c:forEach>
	<div class="number">
		<c:forEach begin="1" end="<%=table%>" var="countTable">
			<span <c:if test="${countTable == 1}">class="active" </c:if>
				data-order="${countTable}">${countTable}</span>
		</c:forEach>
	</div>
</div>
<div class="genre-add add-form">
	<h2>
		<span>Add</span> User <i class="far fa-times-circle"></i>
	</h2>
	<div class="input-container">
		<div class="id-container">
			<label for="genre-id">ID</label> <input id="genre-id" type="text"
				placeholder="User id" readonly="readonly">
		</div>
		<div class="name-container">
			<label for="genre-name">Name</label> <input id="genre-name"
				type="text" placeholder="User name" data-previousvalue=""> <i
				class="fas fa-redo"></i>
		</div>
		<div class="img-container">
			<label for="genre-name">Image</label> <input id="genre-img"
				type="file" placeholder="User image" data-previousvalue="">
			<i class="fas fa-redo"></i>
		</div>
		<div class="email-container">
			<label for="user-email">Email</label> <input id="user-email"
				type="text" placeholder="User email" data-previousvalue="">
			<i class="fas fa-redo"></i>
		</div>
		<div class="password-container">
			<label for="user-password">Password</label> <input id="user-password"
				type="password" placeholder="User password" data-previousvalue="">
			<i class="fas fa-redo"></i>
		</div>
		<div class="role-container">
			<label for="">Role</label> <select id="">
				<option value="-1" selected="selected">Choose role</option>
				<option value="user">User</option>
				<option value="admin">Admin</option>
			</select> <i class="fas fa-redo"></i>
		</div>
	</div>
	<p class="error" style="color: red;"></p>
	<button class="add-btn" value="" data-type="1">Add</button>
</div>

<!-- <script type="application/javascript"
	src="/MusicWebReplace/resources/js/admin.js"></script> -->
	