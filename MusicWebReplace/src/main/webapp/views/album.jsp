<%@page import="com.musicweb.model.Album"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<h1>Manage Album</h1>
<div class="tool">
	<div style="position: relative;">
		<button class="add">
			<i class="far fa-plus-square"></i>
		</button>
		<i class="fas fa-search"></i> <input type="text"
			placeholder="Search for name or id...">
		<div id="search-result-container"></div>
	</div>
	<div class="sort-container">
		<select class="sort-attribute"
			data-currentSortAttribute="${attribute}">
			<option value="id">ID</option>
			<option value="name">Name</option>
			<option value="liked">Liked</option>
		</select> <select class="sort-type" data-currentSortType="${type}">
			<option value="1">Increase</option>
			<option value="2">Decrease</option>
		</select>
		<button class="sort-btn">Sort</button>
	</div>
</div>
<div class="genre-table slide-table">
	<i class="fas fa-chevron-left"></i> <i class="fas fa-chevron-right"></i>
	<%
		int table = ((ArrayList<Album>) request.getAttribute("albums")).size() / 10;
	%>
	<c:if test="${fn:length(albums)%10 > 0}">
		<%
			table++;
		%>
	</c:if>

	<c:set var="countGenre" value="1"></c:set>

	<c:forEach begin="1" end="<%=table%>" var="countTable">
		<table <c:if test="${countTable == 1}">class="active"</c:if> border=1
			data-order="${countTable}">
			<tr>
				<th>ID</th>
				<th>Name</th>
				<th>Artist</th>
				<th>Liked</th>
				<th>Detail</th>
				<th>Action</th>
			</tr>
			<c:forEach varStatus="loop" begin="${countGenre}"
				end="${countGenre+9}" var="i">
				<c:if test="${i <= fn:length(albums)}">
					<tr data-genreId="${albums[i-1].id}" data-genreOrder="${i}">
						<td>${albums[i-1].id}</td>
						<td>${albums[i-1].name}</td>
						<td>${albums[i-1].artist.name}</td>
						<td>${albums[i-1].liked}</td>
						<td>
							<button class="info" value="${albums[i-1].id}"
								data-artistid="${albums[i-1].artist.id}"
								data-albumid="${albums[i-1].id}">
								<i class="fas fa-info"></i>
							</button>
						</td>
						<td>
							<button class="modify" value="${albums[i-1].id}">
								<i class="far fa-edit"></i>
							</button>
							<button class="delete" value="${albums[i-1].id}">
								<i class="far fa-trash-alt"></i>
							</button>
						</td>
					</tr>
				</c:if>
			</c:forEach>
			<c:set var="countGenre" value="${countGenre+10}"></c:set>
		</table>
	</c:forEach>
	<div class="number">
		<c:forEach begin="1" end="<%=table%>" var="countTable">
			<span <c:if test="${countTable == 1}">class="active" </c:if>
				data-order="${countTable}">${countTable}</span>
		</c:forEach>
	</div>
</div>
<div class="genre-add add-form">
	<h2>
		<span>Add</span> Album <i class="far fa-times-circle"></i>
	</h2>
	<div class="input-container">
		<div class="id-container">
			<label for="genre-id">ID</label> <input id="genre-id" type="text"
				placeholder="Album id" readonly="readonly">
		</div>
		<div class="name-container">
			<label for="genre-name">Name</label> <input id="genre-name"
				type="text" placeholder="Album name" data-previousValue="">
			<i class="fas fa-redo"></i>
		</div>
		<div class="artist-container">
			<label for="genre-name">Artist</label>
			<div>
				<c:forEach items="${artists}" var="genre">
					<input type="radio" data-id="${genre.id}" data-name="${genre.name}"
						id="artist-checkbox${genre.id}" data-previousvalue=""
						name="artist-id">
				</c:forEach>
			</div>
			<i class="fas fa-redo"></i>
		</div>
		<div>
			<label for="genre-name">Song</label>
			<button class="add-song">Add Song</button>
		</div>
	</div>
	<p class="error" style="color: red;"></p>
	<button class="add-btn" value="" data-type="1">Add</button>
</div>
<div class="add-song-box">
	<h2>Add Song <i class="far fa-times-circle"></i></h2>
	<div class="songs">
		
	</div>
</div>

<!-- <script type="application/javascript"
	src="/MusicWebReplace/resources/js/admin.js"></script> -->
	