<%@page import="com.musicweb.model.Song"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<h1>Manage Song</h1>
<div class="tool">
	<div style="position: relative;">
		<button class="add">
			<i class="far fa-plus-square"></i>
		</button>
		<i class="fas fa-search"></i> <input type="text"
			placeholder="Search for name or id...">
		<div id="search-result-container"></div>
	</div>
	<div class="sort-container">
		<select class="sort-attribute"
			data-currentSortAttribute="${attribute}">
			<option value="id">ID</option>
			<option value="name">Name</option>
			<option value="liked">Liked</option>
			<option value="listen">Listen</option>
		</select> <select class="sort-type" data-currentSortType="${type}">
			<option value="1">Increase</option>
			<option value="2">Decrease</option>
		</select>
		<button class="sort-btn">Sort</button>
	</div>
</div>
<div class="genre-table slide-table">
	<i class="fas fa-chevron-left"></i> <i class="fas fa-chevron-right"></i>
	<%
		int table = ((ArrayList<Song>) request.getAttribute("songs")).size() / 10;
	%>
	<c:if test="${fn:length(songs)%10 > 0}">
		<%
			table++;
		%>
	</c:if>

	<c:set var="countGenre" value="1"></c:set>

	<c:forEach begin="1" end="<%=table%>" var="countTable">
		<table <c:if test="${countTable == 1}">class="active"</c:if> border=1
			data-order="${countTable}" style="table-layout: fixed;">
			<tr>
				<th style="width: 60px;">ID</th>
				<th>Name</th>
				<th>Lyric</th>
				<th style="width: 60px;">Liked</th>
				<th style="width: 60px;">Listen</th>
				<th>Artist</th>
				<th>Genre</th>
				<th style="width: 60px;">Play</th>
				<th style="width: 120px;">Action</th>
			</tr>
			<c:forEach varStatus="loop" begin="${countGenre}"
				end="${countGenre+9}" var="i">
				<c:if test="${i <= fn:length(songs)}">
					<tr data-genreId="${songs[i-1].id}" data-genreOrder="${i}">
						<td style="width: 40px;">${songs[i-1].id}</td>
						<td><div style="height: 100px; overflow: auto;word-break:break-word;">${songs[i-1].name}</div></td>
						<td><div style="height: 100px; overflow: auto;word-break:break-word;">${songs[i-1].lyric}</div></td>
						<td><div style="width: 60px;">${songs[i-1].liked}</div></td>
						<td><div style="width: 60px;">${songs[i-1].listen}</div></td>
						<td><c:forEach items="${songs[i-1].song_Artists}"
								var="artist_Genre" varStatus="loop">
								<div style="height: 100px; overflow: auto;display: inline;">
									<c:if
										test="${loop.index == fn:length(songs[i-1].song_Artists)-1}">
									${artist_Genre.id.artist.name}
								</c:if>
									<c:if
										test="${loop.index != fn:length(songs[i-1].song_Artists)-1}">
									${artist_Genre.id.artist.name},
								</c:if>
								</div>
							</c:forEach></td>
						<td><c:forEach items="${songs[i-1].song_Genres}"
								var="artist_Genre" varStatus="loop">
								<c:if
									test="${loop.index == fn:length(songs[i-1].song_Genres)-1}">
									${artist_Genre.id.genre.name}
								</c:if>
								<c:if
									test="${loop.index != fn:length(songs[i-1].song_Genres)-1}">
									${artist_Genre.id.genre.name}, 
								</c:if>
							</c:forEach></td>
						<td><audio controls hidden="" id="audio${songs[i-1].id}">
								<source src="..${songs[i-1].resource}" type="audio/mp3">
							</audio> <i class="fas fa-play" data-status="0"
							data-audioid="audio${songs[i-1].id}" style="cursor: pointer;"></i>
						</td>
						<td>
							<button class="modify" value="${songs[i-1].id}">
								<i class="far fa-edit"></i>
							</button>
							<button class="delete" value="${songs[i-1].id}">
								<i class="far fa-trash-alt"></i>
							</button>
						</td>
					</tr>
				</c:if>
			</c:forEach>
			<c:set var="countGenre" value="${countGenre+10}"></c:set>
		</table>
	</c:forEach>
	<div class="number">
		<c:forEach begin="1" end="<%=table%>" var="countTable">
			<span <c:if test="${countTable == 1}">class="active" </c:if>
				data-order="${countTable}">${countTable}</span>
		</c:forEach>
	</div>
</div>
<div class="genre-add add-form">
	<h2>
		<span>Add</span> Song <i class="far fa-times-circle"></i>
	</h2>
	<div class="input-container">
		<div class="id-container">
			<label for="genre-id">ID</label> <input id="genre-id" type="text"
				placeholder="Song id" readonly="readonly">
		</div>
		<div class="name-container">
			<label for="genre-name">Name</label> <input id="genre-name"
				type="text" placeholder="Song name" data-previousValue=""> <i
				class="fas fa-redo"></i>
		</div>
		<div class="song-container">
			<label for="genre-name">Song file</label> <input id="genre-img"
				type="file" placeholder="Artist image" data-previousvalue="">
			<i class="fas fa-redo"></i>
		</div>
		<div class="lyric-container">
			<label for="genre-name">Lyric</label>
			<textarea rows="8" cols="40" id="lyric" placeholder="Lyric"
				data-previousvalue=""></textarea>
			<i class="fas fa-redo"></i>
		</div>
		<div class="artist-container">
			<label for="genre-name">Artist</label>
			<div>
				<c:forEach items="${artists}" var="genre">
					<input type="checkbox" data-id="${genre.id}"
						data-name="${genre.name}" id="artist-checkbox${genre.id}"
						data-previousvalue="">
				</c:forEach>
			</div>
			<i class="fas fa-redo"></i>
		</div>
		<div class="genre-container">
			<label for="genre-name">Genre</label>
			<div></div>
			<i class="fas fa-redo"></i>
		</div>
	</div>
	<p class="error" style="color: red;"></p>
	<button class="add-btn" value="" data-type="1">Add</button>
</div>
<script type="text/javascript">
document.querySelector(".slide-table").style.height = "1150px";
</script>

<!-- <script type="application/javascript"
	src="/MusicWebReplace/resources/js/admin.js"></script> -->
	