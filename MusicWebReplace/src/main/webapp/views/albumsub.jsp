<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<%-- <select class="artist-select">

	<option value="0"
		<c:if test="${genreid <= 0}">selected="selected"</c:if>>All</option>

	<c:forEach items="${genres}" var="genre">
		<option value="${genre.id}"
			<c:if test="${genreid == genre.id}">selected="selected"</c:if>>${genre.name}</option>
	</c:forEach>
</select> --%>
<div class="albums">
	<c:forEach items="${albums}" var="album" varStatus="loop">
		<div class="frame-element">
			<img alt="artist" src="..${album.artist.img}">
			<p>${album.name}</p>
			<div class="genres">
				<a href="#">${album.artist.name}</a>
			</div>
			<div class="more">
				<span> <c:if test="${album.id != albumidliked[loop.index]}">
						<i class="far fa-heart" data-table="album"
							data-albumid="${album.id}"></i>
					</c:if> <c:if test="${album.id == albumidliked[loop.index]}">
						<i class="far fa-heart fas" data-table="album"
							data-albumid="${album.id}"></i>
					</c:if>
				</span> <span>${album.liked}</span> <span class="album-more-button"
					data-id="${album.id}">More</span>
			</div>
		</div>
	</c:forEach>
</div>
<!-- <script type="application/javascript"
	src="/MusicWebReplace/resources/js/home.js"></script> -->



