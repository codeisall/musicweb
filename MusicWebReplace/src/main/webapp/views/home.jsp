<%@ include file="library.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<jsp:include page="static.jsp"></jsp:include>
<link rel="stylesheet" type="text/css"
	href="<c:url value='/resources/css/home.css'></c:url>">
<script type="application/javascript"
	src="/MusicWebReplace/resources/js/general.js"></script>
<title>Home</title>
</head>
<body>
	<div class="container">
		<div id="header">
			<div id="logo" class="logo">MuW</div>
			<div id="blur"></div>
			<ul id="navbar">
				<li class="navbar-item" id="home">Home</li>
				<li class="navbar-item" id="artist">Artist</li>
				<li class="navbar-item" id="genre">Genre
					<div id="genres-box">
						<c:forEach items="${genres}" var="genre">
							<span data-id="${genre.id}">${genre.name}</span>
						</c:forEach>
						<input hidden="hidden" value="">
					</div>
				</li>
				<li class="navbar-item" id="album">Album</li>
				<li class="navbar-item" id="search"><i class="fas fa-search"></i></li>
				<li class="navbar-item" id="user"><c:if test="${user == null}">
						<i class="fas fa-user"></i>
					</c:if> <c:if test="${user != null}">
							${fn:substring(user.name,0, 3)}
						</c:if>
					<div id="user-box">
						<c:if test="${user == null}">
							<button id="signin-btn">Sign in</button>
						</c:if>
						<c:if test="${user != null}">
							<button id="signout-btn">Sign out</button>
							<button id="favorite-btn">Favorite</button>
						</c:if>
					</div> <input type="text" id="music-web" hidden="hidden"
					data-musicweb="${user == null ? 0 : 1}"></li>
				<li class="navbar-item close-btn" id="closeNavbar-button"><i
					class="fas fa-times" data-belong=""></i></li>
			</ul>
			<div id="search-box">
				<input type="text" placeholder="Search for song, album, artist">
				<div id="search-result-container">
					<div class="songs" data-belong="search"></div>
					<!-- <div class="song" data-table="artist">
						<i class="fas fa-user"></i>
						<p>Son Tung M-TP</p>
					</div>
					<div class="song" data-table="song">
						<i class="fas fa-music"></i>
						<p>Hay Trao Cho Anh</p>
					</div>
					<div class="song" data-table="album">
						<i class="far fa-list-alt"></i>
						<p>Hay Trao Cho Anh</p>
					</div>
					<div class="song" data-table="genre">
						<i class="fas fa-th-large"></i>
						<p>Hay Trao Cho Anh</p>
					</div> -->
				</div>
				<!-- <i class="fas fa-search"></i> -->
			</div>
			<div id="navbar-toggle">
				<i class="fas fa-bars"></i>
			</div>
		</div>
		<div class="ajax-container" style=""></div>
		<div id="footer">
			<div class="footer-info">
				<div class="logo">MuW</div>
				<div class="links">
					<span id="about">About</span> <span id="contact">Contact</span> <span
						id="policy">Policy</span> <span id="language">VN</span>
				</div>
			</div>
			<div class="footer-copyright">Copyright � 2020 by IU Students</div>
		</div>
		<div id="play-container"></div>
		<div id="expand-footer"></div>

		<div id="blur2"></div>
		<div class="artist-popup">
			<i class="fas fa-times" data-belong="artist-popup"></i>
			<div class="artitst-info">
				<img alt="artist" src="../resources/img/me.jpg">
				<div class="artist-detail">
					<p></p>
					<p></p>
				</div>
			</div>
			<div class="artist-song">
				<h1>
					Song <span></span>
				</h1>
				<div class="songs">
					<div class="song">
						<img alt="artist" src="../resources/img/me.jpg"> <input
							hidden="hidden" type="text" value="..">
						<div>
							<p>Hay Trao Cho Anh</p>
							<p>
								<span>Son Tung MTP</span>
							</p>
						</div>
					</div>
				</div>
				<div class="top">
					<h1>Album</h1>
					<i class="fas fa-chevron-left"></i> <i class="fas fa-chevron-right"></i>
					<div class="frame-container">
						<div class="frame active" data-order="1">
							<div class="frame-element">
								<img alt="artist" src="../resources/img/me.jpg">
								<p>Yeu</p>
								<div class="genres">
									<a href="#">Son Tung</a>
								</div>
								<div class="more">
									<span><i class="far fa-heart"></i></span> <span>11k</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="songs-popup">
			<h1 class="genre-title">
				<i class="far fa-arrow-alt-circle-left"></i><span></span><i
					class="fas fa-times" data-belong="songs-popup"></i>
			</h1>
			<h1 class="album-title">
				<i class="far fa-arrow-alt-circle-left"></i><span></span><br> <span></span><i
					class="fas fa-times" data-belong="songs-popup"></i>
			</h1>
			<div class="songs" data-belong=""></div>
		</div>
		<audio controls hidden="hidden" id="audio">
			<source src="" type="audio/mpeg">
		</audio>

		<div id="play-bar-container">
			<div class="song" id="song-info">
				<div class="img-container">
					<img alt="artist" src="../resources/img/me.jpg"> <input
						hidden="hidden" type="text" value=""> <i
						class="fas fa-play"></i>
				</div>

				<div>
					<p>Hay Trao Cho Anh</p>
					<p>
						<span>Son Tung MTP</span>
					</p>
				</div>
			</div>
			<div id="play-control">
				<div id="controls">
					<i class="fas fa-random" id="play-shuffle"></i> <i
						class="fas fa-step-backward" id="play-backward"></i> <i
						class="far fa-play-circle" id="play-play"></i> <i
						class="fas fa-step-forward" id="play-forward"></i> <i
						class="fas fa-redo" id="play-repeat" data-type="0"></i>
				</div>
				<div id="bar" data-before="00:00" data-after="00:00">
					<div id="light"></div>
				</div>
			</div>
			<div id="play-list">
				<i class="fas fa-align-center" id="play-list-button"></i>
			</div>


		</div>

		<div id="play-list-songs">
			<div class="songs">
				<!-- <div class="song">
							<img alt="artist" src="../resources/img/me.jpg"> <input
								hidden="hidden" type="text" value="..">
							<div>
								<p>Hay Trao Cho Anh</p>
								<p>
									<span>Son Tung MP</span>
								</p>
							</div>
						</div>
						<div class="song">
							<img alt="artist" src="../resources/img/me.jpg"> <input
								hidden="hidden" type="text" value="..">
							<div>
								<p>Hay Trao Cho Anh</p>
								<p>
									<span>Son Tung MP</span>
								</p>
							</div>
						</div> -->
			</div>
		</div>

		<div class="song-comments">
			<h1 class="song-comments-title">
				Comments <i class="fas fa-times" data-belong="song-comments"></i>
			</h1>

			<div class="comments"></div>
			<div class="user-comment-container">
				<input type="text" placeholder="Your comment..." id="user-comment">
				<button type="button" id="send-user-comment-btn">Send</button>
			</div>
		</div>
		<div class="song-lyric">
			<h1 class="song-comments-title">
				Lyric <i class="fas fa-times" data-belong="song-lyric"></i>
			</h1>
			<div class="lyric">
			duuashdiahduahduahduahdoahdoahdioad
			asdakdhadhad
			asdasdjasd
			adasdas
			</div>
		</div>
	</div>
</body>
</html>