<%@page import="com.musicweb.model.Artist"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<h1>Manage Artist</h1>
<div class="tool">
	<div style="position: relative;">
		<button class="add">
			<i class="far fa-plus-square"></i>
		</button>
		<i class="fas fa-search"></i> <input type="text"
			placeholder="Search for name or id...">
		<div id="search-result-container"></div>
	</div>
	<div class="sort-container">
		<select class="sort-attribute"
			data-currentSortAttribute="${attribute}">
			<option value="id">ID</option>
			<option value="name">Name</option>
			<option value="liked">Liked</option>
		</select> <select class="sort-type" data-currentSortType="${type}">
			<option value="1">Increase</option>
			<option value="2">Decrease</option>
		</select>
		<button class="sort-btn">Sort</button>
	</div>
</div>
<div class="genre-table slide-table">
	<i class="fas fa-chevron-left"></i> <i class="fas fa-chevron-right"></i>
	<%
		int table = ((ArrayList<Artist>) request.getAttribute("artists")).size() / 10;
	%>
	<c:if test="${fn:length(artists)%10 > 0}">
		<%
			table++;
		%>
	</c:if>

	<c:set var="countGenre" value="1"></c:set>

	<c:forEach begin="1" end="<%=table%>" var="countTable">
		<table <c:if test="${countTable == 1}">class="active"</c:if> border=1
			data-order="${countTable}" style="table-layout: fixed;">
			<tr>
				<th style="">ID</th>
				<th>Name</th>
				<th>Image</th>
				<th style="height: 50px; width: 250px;">Description</th>
				<th>Liked</th>
				<th>Genre</th>
				<th>Action</th>
			</tr>
			<c:forEach varStatus="loop" begin="${countGenre}"
				end="${countGenre+9}" var="i">
				<c:if test="${i <= fn:length(artists)}">
					<tr data-genreId="${artists[i-1].id}" data-genreOrder="${i}">
						<td style="width: 40px;">${artists[i-1].id}</td>
						<td style="width: 150px;">${artists[i-1].name}</td>
						<td style="width: 120px;"><img alt="img"
							src="..${artists[i-1].img}"></td>
						<td style="display: block; overflow-y: auto;">${artists[i-1].description}</td>
						<td style="width: 60px;">${artists[i-1].liked}</td>
						<td style="width: 60px; color: blue;"
							data-test="${artists[i-1].artist_Genres}"><c:forEach
								items="${artists[i-1].artist_Genres}" var="artist_Genre"
								varStatus="loop">
								<c:if
									test="${loop.index == fn:length(artists[i-1].artist_Genres)-1}">
									${artist_Genre.id.genre.name}
								</c:if>
								<c:if
									test="${loop.index != fn:length(artists[i-1].artist_Genres)-1}">
									${artist_Genre.id.genre.name}, 
								</c:if>
							</c:forEach></td>
						<td width="140px">
							<button class="modify" value="${artists[i-1].id}">
								<i class="far fa-edit"></i>
							</button>
							<button class="delete" value="${artists[i-1].id}">
								<i class="far fa-trash-alt"></i>
							</button>
						</td>
					</tr>
				</c:if>
			</c:forEach>
			<c:set var="countGenre" value="${countGenre+10}"></c:set>
		</table>
	</c:forEach>
	<div class="number">
		<c:forEach begin="1" end="<%=table%>" var="countTable">
			<span <c:if test="${countTable == 1}">class="active" </c:if>
				data-order="${countTable}">${countTable}</span>
		</c:forEach>
	</div>
</div>
<div class="genre-add add-form">
	<h2>
		<span>Add</span> Artist <i class="far fa-times-circle"></i>
	</h2>
	<div class="input-container">
		<div class="id-container">
			<label for="genre-id">ID</label> <input id="genre-id" type="text"
				placeholder="Artist id" readonly="readonly">
		</div>
		<div class="name-container">
			<label for="genre-name">Name</label> <input id="genre-name"
				type="text" placeholder="Artist name" data-previousvalue="">
			<i class="fas fa-redo"></i>
		</div>
		<div class="img-container">
			<label for="genre-name">Image</label> <input id="genre-img"
				type="file" placeholder="Artist image" data-previousvalue="">
			<i class="fas fa-redo"></i>
		</div>
		<div class="description-container">
			<label for="genre-name">Description</label>
			<textarea rows="8" cols="40" id="des"
				placeholder="Artist description" data-previousvalue=""></textarea>
			<i class="fas fa-redo"></i>
		</div>
		<div class="genre-container">
			<label for="genre-name">Genre</label>
			<div>
				<c:forEach items="${genres}" var="genre">
					<input type="checkbox" data-id="${genre.id}"
						data-name="${genre.name}" id="genre-checkbox${genre.id}"
						data-previousvalue="">
				</c:forEach>
			</div>
			<i class="fas fa-redo"></i>
		</div>
	</div>
	<p class="error" style="color: red;"></p>
	<button class="add-btn" value="" data-type="1">Add</button>
</div>

<!-- <script type="application/javascript"
	src="/MusicWebReplace/resources/js/admin.js"></script> -->
