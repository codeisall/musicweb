<%@ include file="library.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<jsp:include page="static.jsp"></jsp:include>
<link rel="stylesheet" type="text/css"
	href="<c:url value='/resources/css/login.css'></c:url>">
<script type="application/javascript"
	src="<c:url value='/resources/js/login.js'></c:url>"></script>

<title>Sign In | Sign Up</title>
</head>
<body>
	<div class="container">
		<div class="signinup-form">
			<h1>Sign In</h1>
			<div class="input-container">
				<div class="name-container">
					<i class="fas fa-user"></i><input type="text" placeholder="Name">
				</div>
				<div class="email-container">
					<i class="far fa-envelope"></i><input type="text"
						placeholder="Email">
				</div>
				<div class="password-container">
					<i class="fas fa-lock"></i><input type="password"
						placeholder="Password">
				</div>
				<div class="confirm-password-container">
					<i class="fas fa-lock"></i><input type="password"
						placeholder="Confirm passowrd">
				</div>
			</div>
			<button class="signinup-btn">Sign In</button>
			<p class="error"></p>
			<div class="link">
				<a href="#">Sign up for free!</a> <a href="../homeController/">Home</a>
			</div>
		</div>
		<div class="forget-form"></div>
	</div>
</body>
</html>
