<%@page import="com.musicweb.model.Genre"%>
<%@page import="com.musicweb.model.Album"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.musicweb.model.Artist"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<div id="top-artist" class="top">
	<h1>Top 10 Artist</h1>
	<i class="fas fa-chevron-left"></i> <i class="fas fa-chevron-right"></i>
	<%
		int table = ((ArrayList<Artist>) request.getAttribute("topArtists")).size() / 5;
	System.out.println(table);
	%>
	<c:if test="${fn:length(topArtists)%5 > 0}">
		<%
			System.out.println(table);
		table++;
		%>
	</c:if>
	<c:set var="countGenre" value="1"></c:set>
	<c:forEach begin="1" end="<%=table%>" var="countTable">
		<div <c:if test="${countTable == 1}">class="frame active"</c:if>
			<c:if test="${countTable != 1}">class="frame"</c:if>
			data-order="${countTable}">
			<c:forEach varStatus="loop" begin="${countGenre}"
				end="${countGenre+4}" var="i">
				<c:if test="${i <= fn:length(topArtists)}">
					<div class="frame-element">
						<img alt="artist" src="..${topArtists[i-1].img}">
						<p>${topArtists[i-1].name}</p>
						<div class="genres">
							<c:if test="${fn:length(topArtists[i-1].artist_Genres) >= 2}">
								<a href="#">${topArtists[i-1].artist_Genres[0].id.genre.name}</a>,
							<a href="#">${topArtists[i-1].artist_Genres[1].id.genre.name}</a>
							</c:if>
							<c:if test="${fn:length(topArtists[i-1].artist_Genres) < 2}">
								<a href="#">${topArtists[i-1].artist_Genres[0].id.genre.name}</a>
							</c:if>
						</div>
						<div class="more">
							<span> <c:if
									test="${topArtists[i-1].id != artistdiliked[i-1]}">
									<i class="far fa-heart" data-table="artist"
										data-artistid="${topArtists[i-1].id}"></i>
								</c:if> <c:if test="${topArtists[i-1].id == artistdiliked[i-1]}">
									<i class="far fa-heart fas" data-table="artist"
										data-artistid="${topArtists[i-1].id}"></i>
								</c:if>
							</span> <span>${topArtists[i-1].liked}</span> <span
								class="artist-more-button" data-id="${topArtists[i-1].id}">More</span>
						</div>
					</div>
				</c:if>
			</c:forEach>
		</div>
		<c:set var="countGenre" value="${countGenre+5}"></c:set>
	</c:forEach>
</div>

<div id="top-album" class="top">
	<h1>Top 10 Album</h1>
	<i class="fas fa-chevron-left"></i> <i class="fas fa-chevron-right"></i>
	<%
		table = ((ArrayList<Album>) request.getAttribute("topAlbums")).size() / 5;
	System.out.println(table);
	%>
	<c:if test="${fn:length(topAlbums)%5 > 0}">
		<%
			System.out.println(table);
		table++;
		%>
	</c:if>
	<c:set var="countGenre" value="1"></c:set>
	<c:forEach begin="1" end="<%=table%>" var="countTable">
		<div <c:if test="${countTable == 1}">class="frame active"</c:if>
			<c:if test="${countTable != 1}">class="frame"</c:if>
			data-order="${countTable}">
			<c:forEach varStatus="loop" begin="${countGenre}"
				end="${countGenre+4}" var="i">
				<c:if test="${i <= fn:length(topAlbums)}">
					<div class="frame-element">
						<img alt="artist" src="..${topAlbums[i-1].artist.img}">
						<p>${topAlbums[i-1].name}</p>
						<div class="genres">
							<a href="#">${topAlbums[i-1].artist.name}</a>
						</div>
						<div class="more">
							<span> <c:if
									test="${topAlbums[i-1].id != albumidliked[i-1]}">
									<i class="far fa-heart" data-table="album"
										data-albumid="${topAlbums[i-1].id}"></i>
								</c:if> <c:if test="${topAlbums[i-1].id == albumidliked[i-1]}">
									<i class="far fa-heart fas" data-table="album"
										data-albumid="${topAlbums[i-1].id}"></i>
								</c:if>
							</span> <span>${topAlbums[i-1].liked}</span> <span
								class="album-more-button" data-id="${topAlbums[i-1].id}">More</span>
						</div>
					</div>
				</c:if>
			</c:forEach>
		</div>
		<c:set var="countGenre" value="${countGenre+5}"></c:set>
	</c:forEach>
</div>

<div id="top-genre" class="top">
	<h1>Top 10 genre</h1>
	<i class="fas fa-chevron-left"></i> <i class="fas fa-chevron-right"></i>
	<%
		table = ((ArrayList<Genre>) request.getAttribute("topGenres")).size() / 5;
	%>
	<c:if test="${fn:length(topGenres)%5 > 0}">
		<%
			System.out.println(table);
		table++;
		%>
	</c:if>
	<c:set var="countGenre" value="1"></c:set>
	<c:forEach begin="1" end="<%=table%>" var="countTable">
		<div <c:if test="${countTable == 1}">class="frame active"</c:if>
			<c:if test="${countTable != 1}">class="frame"</c:if>
			data-order="${countTable}">
			<c:forEach varStatus="loop" begin="${countGenre}"
				end="${countGenre+4}" var="i">
				<c:if test="${i <= fn:length(topGenres)}">
					<div class="frame-element">
						<!-- <img alt="artist" src="../resources/img/me.jpg"> -->
						<div class="img">
							${fn:substring(topGenres[i-1].name,0, 3)}
						</div>
						<p>${topGenres[i-1].name}</p>
						<div class="genres">
							<a href="#"></a>
						</div>
						<div class="more">
							<span>${topGenres[i-1].listen}</span> <span
								class="genre-more-button" data-id="${topGenres[i-1].id}">More</span>
						</div>
					</div>
				</c:if>
			</c:forEach>
		</div>
		<c:set var="countGenre" value="${countGenre+5}"></c:set>
	</c:forEach>
</div>

<div class="top-song top">
	<h1>
		Top 100 Song (Like) <span></span>
	</h1>
	<div class="songs" data-belong="like">
		<c:forEach items="${topSongs}" var="song" varStatus="loop">
			<div class="song" id="song-${song.id}">
				<div class="img-container">
					<img alt="artist" src="..${song.song_Artists[0].id.artist.img}">
					<i class="fas fa-play img-play"></i>
				</div>
				<input hidden="hidden" type="text" value="..${song.resource}">
				<div style="pointer-events: none;">
					<p style="pointer-events: none;">${song.name}${songidsliked[loop.index]}</p>
					<p style="pointer-events: none;">
						<c:forEach items="${song.song_Artists}" var="artist">
							<span style="pointer-events: none;">${artist.id.artist.name}</span>
						</c:forEach>
					</p>
				</div>
				<div class="song-more-container">
					<i class="fas fa-ellipsis-h song-more"></i>
					<div class="song-more-options">
						<c:if test="${song.id != songidsliked[loop.index]}">
							<i class="far fa-heart" data-table="song"
								data-songid="${song.id}"></i>
						</c:if>
						<c:if test="${song.id == songidsliked[loop.index]}">
							<i class="far fa-heart fas" data-table="song"
								data-songid="${song.id}"></i>
						</c:if>
						<i class="far fa-comment" data-table="song"
							data-songid="${song.id}"></i>
					</div>
				</div>
			</div>
		</c:forEach>
		<button id="more-song-button" value="10" data-table="like">More</button>
	</div>
</div>

<div class="top-song top">
	<h1>
		Top 100 Song (Listen) <span></span>
	</h1>
	<div class="songs" data-belong="listen">
		<c:forEach items="${topSongsListen}" var="song" varStatus="loop">
			<div class="song" id="song-${song.id}">
				<div class="img-container">
					<img alt="artist" src="..${song.song_Artists[0].id.artist.img}">
					<i class="fas fa-play img-play"></i>
				</div>
				<input hidden="hidden" type="text" value="..${song.resource}">
				<div style="pointer-events: none;">
					<p style="pointer-events: none;">${song.name}${songidsliked[loop.index]}</p>
					<p style="pointer-events: none;">
						<c:forEach items="${song.song_Artists}" var="artist">
							<span style="pointer-events: none;">${artist.id.artist.name}</span>
						</c:forEach>
					</p>
				</div>
				<div class="song-more-container">
					<i class="fas fa-ellipsis-h song-more"></i>
					<div class="song-more-options">
						<c:if test="${song.id != songidsliked2[loop.index]}">
							<i class="far fa-heart" data-table="song"
								data-songid="${song.id}"></i>
						</c:if>
						<c:if test="${song.id == songidsliked2[loop.index]}">
							<i class="far fa-heart fas" data-table="song"
								data-songid="${song.id}"></i>
						</c:if>
						<i class="far fa-comment" data-table="song"
							data-songid="${song.id}"></i>
					</div>
				</div>
			</div>
		</c:forEach>
		<button id="more-song-button" value="10" data-table="listen">More</button>
	</div>
</div>

<!-- <script type="application/javascript"
	src="/MusicWebReplace/resources/js/home.js"></script> -->



