<%@ include file="library.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<jsp:include page="static.jsp"></jsp:include>
<link rel="stylesheet" type="text/css"
	href="<c:url value='/resources/css/admin.css'></c:url>">
<script type="application/javascript"
	src="/MusicWebReplace/resources/js/admin1.js"></script>
<title>Admin</title>
</head>
<body>
	<div class="container">
		<%-- <c:if test="${user != null && user.role == 'admin'}"> --%>
			<div class="navbar">
				<ul class="items">
					<li id="genre" class="active" data-tablename="genre">Genre</li>
					<li id="artist" data-tablename="artist">Artist</li>
					<li id="album" data-tablename="album">Album</li>
					<li id="song" data-tablename="song">Song</li>
					<li id="user" data-tablename="user">User</li>
					<li id="logout">Logout</li>
				</ul>
			</div>
			<div class="ajax-container" style="position: relative;"></div>
			<div id="blur"></div>
			<div id="blur2"></div>
		<%-- </c:if> --%>
		<%-- <c:if test="${user.role == 'user' || user == null}">
			<h1
				style="text-align: center; color: red; height: 100vh; display: flex; justify-content: center; align-items: center;">
				You are not allowed to access this site<br />You will be redirected
				to home page after 5s
			</h1>
			<script type="text/javascript">
				setTimeout(function() {
					location.href = "../homeController/";
				}, 5000)
			</script>
		</c:if> --%>
	</div>
	<!-- <script type="text/javascript">
		window.addEventListener("DOMContentLoaded", function() {
			$(".ajax-container").load("../genreController/");
		});
	</script> -->
</body>
</html>