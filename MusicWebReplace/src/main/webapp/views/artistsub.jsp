<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<select class="artist-select">

	<option value="0"
		<c:if test="${genreid <= 0}">selected="selected"</c:if>>All</option>

	<c:forEach items="${genres}" var="genre">
		<option value="${genre.id}"
			<c:if test="${genreid == genre.id}">selected="selected"</c:if>>${genre.name}</option>
	</c:forEach>
</select>
<div class="artists">
	<c:forEach items="${artists}" var="artist" varStatus="loop">
		<div class="frame-element">
			<img alt="artist" src="..${artist.img}">
			<p>${artist.name}</p>
			<div class="genres">
				<c:if test="${fn:length(artist.artist_Genres) >= 2}">
					<a href="#">${artist.artist_Genres[0].id.genre.name}</a>,
							<a href="#">${artist.artist_Genres[1].id.genre.name}</a>
				</c:if>
				<c:if test="${fn:length(artist.artist_Genres) < 2}">
					<a href="#">${artist.artist_Genres[0].id.genre.name}</a>
				</c:if>
			</div>
			<div class="more">
				<span> <c:if test="${artist.id != artistidliked[loop.index]}">
						<i class="far fa-heart" data-table="artist"
							data-artistid="${artist.id}"></i>
					</c:if>
					<c:if test="${artist.id == artistidliked[loop.index]}">
						<i class="far fa-heart fas" data-table="artist"
							data-artistid="${artist.id}"></i>
					</c:if>
				</span> <span>${artist.liked}</span> <span class="artist-more-button"
					data-id="${artist.id}">More</span>
			</div>
		</div>
	</c:forEach>
</div>
<!-- <script type="application/javascript"
	src="/MusicWebReplace/resources/js/home.js"></script> -->



