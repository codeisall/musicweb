<link rel="stylesheet" type="text/css"
	href="/MusicWebReplace/resources/css/all.css">
<script type="application/javascript"
	src="/MusicWebReplace/resources/js/jquery-3.4.1.min.js"></script>

<style type="text/css">
* {
	margin: 0;
	padding: 0;
	font-family: arial;
	box-sizing: border-box;
}

body, html, .container {
	width: 100%;
	height: 100%;
	background-color: #f5f6fa;
}

#blur, #blur2 {
	position: fixed;
	left: 0;
	top: 0;
	z-index: 999;
	width: 100%;
	height: 100%;
	background-color: #2f3640;
	opacity: 0;
	visibility: hidden;
}

#blur2 {
	z-index: 999;
}

#blur.appear, #blur2.appear, #blur2.appear2 {
	opacity: 0.6;
	visibility: visible;
}

#blur2.appear, #blur2.appear2 {
	opacity: 0.98;
}

#blur2.appear2 {
	z-index: 1000;
}
</style>
